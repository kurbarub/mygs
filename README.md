**MyGS (My Gas Station App)**

Klientská část aplikace
Pokud budete postupovat podle pokynů ze zdroje https://medium.com/fluttercommunity/flutter-getting-started-f07df7d4cce2:
1. Stáhněte si Flutter pro váš operační systém https://docs.flutter.dev/getstarted/install
2. Zkontrolujte, zda je Flutter správně nainstalován, zadáním následujícího příkazu na
příkazový řádek: flutter doctor.
3. Použijte jedno z následujících vývojových prostředí: Android Studio nebo VS Code.
4. Spuštění klienta:
. Emulátor - Tento návod použijte k nastavení emulátoru https://docs.flutter.
dev/get-started/install/windows
. Device - Chcete-li spustit klientskou aplikaci na skutečném zařízení Android, postupujte takto.
a. Připojte zařízení Android k počítači nebo notebooku pomocí kabelu USB. b. Aktivujte možnost vývojáře na svém zařízení Android. c. Nainstalujte aplikaci Mirroring
pro zrcadlení vašeho zařízení Android ve vašem počítači
Celý tutoriál - https://ferilukmansyah.medium.com/easy-way-to-setupyour-android-device-to-run-flutter-project-28bddf0fa7f1
. Příkaz - Aplikaci lze spustit následujícím příkazem - flutter run –no-sound-null-safety
Serverová část aplikace
1. Stáhněte si klienta Docker pro pohodlí - https://www.docker.com/products/
docker-desktop/
2. Použijte vývojové prostředí IntelliJ IDEA
3. Nainstalujte plugin Docker: File -> Settings -> Plugins -> Add Docker
4. Otevřete soubor docker-compose.yml v kořenové složce
5. Spusťte proces stavby kontejneru kliknutím na zelenou šipku na levém panelu
6. Poté přejděte na Docker Desktop, spusťte image mygs
7. Provoz na serverové straně aplikace můžete zkontrolovat pomocí již napsaných
skriptů umístěných v kořenové složce requests.
Pro správnou funkci aplikace je nutné nastavit pro odesílání emailů údaje jako
spring.mail.username, spring.mail.password; bing.api.key pro výpočty rozsahu.
Tato data musíte zadat ručně v \src\main\resources\application.properties.
Údaje si můžete vyžádat písemně na email: kurbarub@fel.cvut.cz.
