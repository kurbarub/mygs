package cz.cvut.fel.mygs.assemblers

import cz.cvut.fel.mygs.models.Fuel
import cz.cvut.fel.mygs.models.FuelType
import cz.cvut.fel.mygs.models.RefuelingStand
import cz.cvut.fel.mygs.repositories.FuelRepository
import cz.cvut.fel.mygs.repositories.RefuelingStandRepository
import org.springframework.stereotype.Component

@Component
class RefuelingStandAssembler(
    val fuelAssembler: FuelAssembler,
    val fuelRepository: FuelRepository,
    val refuelingStandRepository: RefuelingStandRepository
) {

    fun toRefuelingStandDto(rs: RefuelingStand, gs_id:Long): RefuelingStandAssembler.RefuelingStandDto {

        return  RefuelingStandAssembler.RefuelingStandDto(
            rs.id,
            rs.standId,
//            fuelAssembler.toListFuelDto(rs.fuels)
            fuelAssembler.toListFuelDto( fuelRepository.findAllByRsContainsRefuelingStand(rs.standId, gs_id))
        )
    }

    fun toListRefuelingStandDto(rsList: List<RefuelingStand>, gs_id:Long): List<RefuelingStandAssembler.RefuelingStandDto>{
        return if(rsList.isNotEmpty()){
            rsList.map { toRefuelingStandDto(it, gs_id) }
        }else{
            listOf()
        }
    }
    data class RefuelingStandDto(
        val id: Long,
        val standId: Int,
        val fuels: List<FuelAssembler.FuelDto>
    )


}

