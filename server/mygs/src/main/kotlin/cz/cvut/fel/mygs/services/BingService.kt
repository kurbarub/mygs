package cz.cvut.fel.mygs.services

import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class BingService(
    @Value("\${bing.api.key}") val bingKey: String,
    val objectMapper: ObjectMapper
) {
    val httpClient = OkHttpClient()
    val travelMode = "driving"

    fun getDistance(userCoord: Coordinates, targetCoord: Coordinates): Double{

        if(userCoord.longitude == 0.0 || userCoord.latitude == 0.0)
            return 0.0
        return getData(userCoord, targetCoord)
    }

    private fun getData(userCoord: Coordinates, targetCoord: Coordinates):Double{

        val postBody = BingBodyData(
            origins = listOf(
                userCoord
            ),
            destinations = listOf(
                targetCoord
            ),
            travelMode = travelMode
        )

        val request = Request.Builder()
            .url("https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?key=$bingKey")
            .header("Content-Type", "application/json")
            .post(
                objectMapper.writeValueAsBytes(
                    postBody
                ).toRequestBody("application/json".toMediaType())
            ).build()

        val response = httpClient.newCall(request).execute()
        val values = objectMapper.readValue(response.body!!.bytes(), BingResponse::class.java)

        return values.resourceSets[0].resources[0].results[0].travelDistance
    }
}

data class Coordinates(
    val latitude: Double,
    val longitude: Double,
) {
    override fun toString(): String {
        return "Coordinates(latitude=$latitude, longitude=$longitude)"
    }
}

data class BingBodyData(
    val origins: List<Coordinates>,
    val destinations: List<Coordinates>,
    val travelMode: String
)

data class BingResponse(
    val resourceSets: List<ResultSet>,
    val statusCode: Int
)

data class ResultSet(
    val resources: List<Resource>
)

data class Resource(
    val results: List<Result>
)

data class Result(
    val travelDistance: Double
)

