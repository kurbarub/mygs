package cz.cvut.fel.mygs.controllers

import cz.cvut.fel.mygs.assemblers.GasStationAssembler
import cz.cvut.fel.mygs.assemblers.OfferAssembler
import cz.cvut.fel.mygs.models.*
import cz.cvut.fel.mygs.repositories.FuelRepository
import cz.cvut.fel.mygs.repositories.GasStationRepository
import cz.cvut.fel.mygs.repositories.RefuelingStandRepository
import cz.cvut.fel.mygs.repositories.UserRepository
import cz.cvut.fel.mygs.services.BingService
import cz.cvut.fel.mygs.services.Coordinates
import cz.cvut.fel.mygs.services.GasStationService
import org.slf4j.LoggerFactory
import org.springframework.data.geo.Distance
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/gas_station")
class GasStationController(
    val gasStationAssembler: GasStationAssembler,
    val gasStationRepository: GasStationRepository,
    val userRepository: UserRepository,
    val gasStationService: GasStationService,
    val fuelRepository: FuelRepository,
    val refuelingStandRepository: RefuelingStandRepository,
    val offerAssembler: OfferAssembler,
    val bingService: BingService
) {

    companion object {
        private val LOG = LoggerFactory.getLogger(GasStationController::class.java)
    }

    /**
     * Get all gas stations by city
     * @param city: location
     * @param fromUser: http request with user principal
     * @param data: user coordinates
     * @return list of gas stations
     */
    @PostMapping("/{city}")
    fun getAllGasStationsShortInfo(@PathVariable city: String, @RequestBody data: Coordinates, fromUser: HttpServletRequest): ResponseEntity<List<GasStationAssembler.ShortGSDto>> {
        LOG.info(
            "REQUEST /gas_station/{city}: Get all gas stations from city - $city"
        )
        println(fromUser.userPrincipal)

        var user:User? = User()

        if(fromUser.userPrincipal == null){
            user = null
        }
        else{
            user = userRepository.findByEmail(fromUser.userPrincipal.name).orElse(null)
        }

        val gs: List<GasStation> = gasStationService.getAllGasStations(city)

        val response: MutableList<GasStationAssembler.ShortGSDto> = mutableListOf()

        for (i in gs){
            response.add(
                gasStationAssembler.toShortGsDto(
                    ShortGasStationInfo(
                        i,
                        user?.favorite?.contains(i) ?: false,
                        bingService.getDistance(
                            Coordinates(data.latitude, data.longitude),
                            Coordinates(i.atitude, i.longtitude)
                        ),
                    )
                )
            )

        }

        LOG.info(
            "RESPONSE /gas_station/{city}: Get all gas stations from city - $city with " +
                    "Found - ${response.size}"
        )

        return ResponseEntity.ok(response)
    }

    /**
     * Get full information of gas stations by id
     * @param gsId: gas station id
     * @param fromUser: http request with user principal
     * @param data: user coordinates
     * @return gas station assembler
     */
    @PostMapping("/info/{gsId}")
    fun getGasStationFullInfo(@PathVariable gsId: Long, @RequestBody data: Coordinates, fromUser: HttpServletRequest): ResponseEntity<GasStationAssembler.FullGSDto>{
        LOG.info(
            "REQUEST /gas_station/{gs_id}: Get full information about the gas station with ID - $gsId"
        )
        val gs = gasStationService.getGasStationFullInfo(gsId)

        var user:User? = User()

        if(fromUser.userPrincipal == null){
            user = null
        }
        else{
            user = userRepository.findByEmail(fromUser.userPrincipal.name).orElse(null)
        }
        val fuels = fuelRepository.findAllByGasStationId(gsId)
        val rs = refuelingStandRepository.findAllByGasStationId(gsId)

        val response = gasStationAssembler.toFullGsDto(
            FullGasStationInfo(
                gs,
                user?.favorite?.contains(gs) ?: false,
                bingService.getDistance(
                    Coordinates(data.latitude, data.longitude),
                    Coordinates(gs.atitude, gs.longtitude)
                ),
                fuels,
                rs
            )
        )

        LOG.info(
            "RESPONSE /gas_station/{gs_id}: Get full information about the gas station with ID - $gsId with" +
                    " response - ${response.toString()}"
        )

        return ResponseEntity.ok(response)

    }

    /**
     * Get list of favorite gas stations
     * @param fromUser: http request with user principal
     * @param data: user coordinates
     * @return list of gas stations
     */
    @PreAuthorize("hasRole('USER')")
    @PostMapping("/favorite")
    fun getFavoriteGasStations(fromUser: HttpServletRequest, @RequestBody data: Coordinates,): ResponseEntity<List<GasStationAssembler.ShortGSDto>>{

        val user = userRepository.findByEmail(fromUser.userPrincipal.name).orElse(null)

        LOG.info(
            "REQUEST /gas_station/favorite: Get a list of all favorite gas stations for a user - ${user.email}"
        )

        val gs = gasStationService.getFavoriteGasStations(user)
        val response: MutableList<GasStationAssembler.ShortGSDto> = mutableListOf()

        for (i in gs){
            response.add(
                gasStationAssembler.toShortGsDto(
                    ShortGasStationInfo(
                        i,
                        true,
                        bingService.getDistance(
                            Coordinates(data.latitude, data.longitude),
                            Coordinates(i.atitude, i.longtitude)
                        ),
                    )
                )
            )
        }

        LOG.info(
            "RESPONSE /gas_station/favorite: Get a list of all favorite gas stations for a user - ${user.email} " +
                    "with the number of elements found - ${response.size}"
        )

        return ResponseEntity.ok(response)

    }

    /**
     * Set to/Remove from list of favorite gas stations
     * @param fromUser: http request with user principal
     * @param gs_id: user id
     * @return list of gas stations
     */
    @PreAuthorize("hasRole('USER')")
    @PostMapping("/favorite/{gs_id}")
    fun changeRelationshipToGasStation(@PathVariable gs_id: Long, fromUser: HttpServletRequest): ResponseEntity<String>{
        val user = userRepository.findByEmail(fromUser.userPrincipal.name).orElse(null)

        LOG.info(
            "REQUEST /gas_station/favorite/{gs_id}: Change the user's attitude - ${user.email}, to the gas station with ID - $gs_id}"
        )

        val gs = gasStationService.changeRelationshipToGasStation(gs_id, user)

        return ResponseEntity.ok("The attitude set to ${user.favorite.contains(gs)}")
    }

    /**
     * Get list of favorite gas stations by properties
     * @param fromUser: http request with user principal
     * @param filterProperties: filter properties
     * @return list of gas stations
     */
    @PostMapping("/filter")
    fun getFilteredGasStationList(@RequestBody filterProperties: FilterGsRequest, fromUser: HttpServletRequest): ResponseEntity<List<GasStationAssembler.ShortGSDto>>{
        var user:User? = User()

        if(fromUser.userPrincipal == null){
            user = null
        }
        else{
            user = userRepository.findByEmail(fromUser.userPrincipal.name).orElse(null)
        }

        LOG.info(
            "REQUEST /gas_station/filter: Get a filtered list of gas stations with parameters - ${filterProperties.toString()}"
        )
        val gs = gasStationService.getFilteredGasStationList(filterProperties)

        val response: MutableList<GasStationAssembler.ShortGSDto> = mutableListOf()

        for (i in gs){
            response.add(
                gasStationAssembler.toShortGsDto(
                    ShortGasStationInfo(
                        i,
                        user?.favorite?.contains(i)?:false,
                        bingService.getDistance(
                            Coordinates(filterProperties.coordinates.latitude, filterProperties.coordinates.longitude),
                            Coordinates(i.atitude, i.longtitude)
                        )
                    )
                )
            )

        }

        LOG.info(
            "RESPONSE /gas_station/filter: Get a filtered list of gas stations with parameters - ${filterProperties.toString()} " +
                    "with the number of elements found - ${response.size}"
        )

        return ResponseEntity.ok(response)
    }

    /**
     * Get list of favorite gas stations by search request
     * @param fromUser: http request with user principal
     * @param req: search request
     * @param data: user coordinates
     * @return list of gas stations
     */
    @PostMapping("/search/{req}")
    fun searchGasStations(@PathVariable req: String, @RequestBody data: Coordinates, fromUser: HttpServletRequest): ResponseEntity<List<GasStationAssembler.ShortGSDto>>{

        var user:User? = User()

        if(fromUser.userPrincipal == null){
            user = null
        }
        else{
            user = userRepository.findByEmail(fromUser.userPrincipal.name).orElse(null)
        }
        LOG.info(
            "REQUEST /gas_station/search/{req): Get a list of gas stations with matching search - $req"
        )
        val gs = gasStationService.getGasStationsByRequestText(req)

        val response: MutableList<GasStationAssembler.ShortGSDto> = mutableListOf()

        for (i in gs){
            response.add(
                gasStationAssembler.toShortGsDto(
                    ShortGasStationInfo(
                        i,
                        user?.favorite?.contains(i)?:false,
                        bingService.getDistance(
                            Coordinates(data.latitude, data.longitude),
                            Coordinates(i.atitude, i.longtitude)
                        )
                    )
                )
            )

        }

        LOG.info(
            "RESPONSE /gas_station/search/{req}: Get a list of gas stations with matching search - $req " +
                    "with the number of elements found - ${response.size}"
        )

        return ResponseEntity.ok(response)
    }

    /**
     * Get 5 random offers of gas stations
     * @return list of offers
     */
    @GetMapping("/offers")
    fun getRandomFiveOffers(): ResponseEntity<List<OfferAssembler.OfferDto>>{

        LOG.info(
            "REQUEST /gas_station/offers: Get a list of 5 random offers from gas stations"
        )
        val offers = gasStationService.getOffers()
        val response = offerAssembler.toListOfferDto(offers)

        LOG.info(
            "RESPONSE /gas_station/offers: Get a list of 5 random offers from gas stations SUCCESS"
        )

        return ResponseEntity.ok(response)

    }


    /**
     * Get nearest gas station
     * @param fromUser: http request with user principal
     * @param data: user coordinates
     * @return gas stations assembler
     */
    @PostMapping("/nearest")
    fun getNearestGasStation( fromUser: HttpServletRequest, @RequestBody data: Coordinates): ResponseEntity<GasStationAssembler.ShortGSDto>{

        var user:User? = User()

        if(fromUser.userPrincipal == null){
            user = null
        }
        else{
            user = userRepository.findByEmail(fromUser.userPrincipal.name).orElse(null)
        }
        LOG.info(
            "REQUEST /gas_station/nearest: Get the nearest gas station to ${data.toString()}"
        )
        val gs = gasStationService.getNearestGS(data)
        val response = gasStationAssembler.toShortGsDto(ShortGasStationInfo(
            gs,
            user?.favorite?.contains(gs)?:false,
            bingService.getDistance(
                Coordinates(data.latitude, data.longitude),
                Coordinates(gs.atitude, gs.longtitude)
            )
        ))

        LOG.info(
            "RESPONSE /gas_station/nearest: Get the nearest gas station with result - ${response.toString()}"
        )

        return ResponseEntity.ok(response)
    }

}

data class ShortGasStationInfo(
    val gs: GasStation,
    val isFavorite: Boolean,
    val distance: Double,
)

data class FilterGsRequest(
    val fuelTypes: List<Int>,
    val paymentMethods: List<Int>,
    val services: List<Int>,
    val city: String,
    val coordinates: Coordinates
) {
    override fun toString(): String {
        return "FilterGsRequest(fuelTypes=$fuelTypes, paymentMethods=$paymentMethods, services=$services, city='$city')"
    }
}

data class FullGasStationInfo(
    val gs: GasStation,
    val isFavorite: Boolean,
    val distance: Double,
    val priceList: List<Fuel>,
    val refuelingStands: List<RefuelingStand>
)

