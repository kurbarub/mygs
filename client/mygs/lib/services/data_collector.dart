import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:MyGS/classes/auto_class.dart';
import 'package:MyGS/classes/card_class.dart';
import 'package:MyGS/classes/feed_back_class.dart';
import 'package:MyGS/classes/gas_station_class.dart';
import 'package:MyGS/classes/order_class.dart';
import 'package:MyGS/classes/question_class.dart';
import 'package:MyGS/utility/variables/cities.dart';
import 'package:MyGS/utility/variables/languages.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../classes/ofer_cless.dart';
import '../classes/share_class.dart';
import '../classes/user_class.dart';

class DataCollector {
  static final DataCollector _instance = DataCollector._internal();
  factory DataCollector() => _instance;

  List<int> fuelTypes = [];
  List<int> payMethods = [];
  List<int> services = [];

  bool inited = false;
  bool? firstStart;
  bool? withoutAuth;

  String? language;
  String? city;

  late List<GasStation> gasStatons;
  late GasStation nearestGS;
  BuildContext? buildContext;
  BuildContext? context;
  SharedPreferences? prefs;


  int? userId;
  String? userName;
  String? userSurname;
  String? userPhone;
  String? userDOB;
  int? userGender;
  String? userEmail;
  String? accessToken;
  bool?  receiveOfferNews;
  bool? receiveOrderInfo;
  bool? callStaff;


  Position? _currentPosition;
  String? _currentAddress;

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  late Dio dio;

  BehaviorSubject<List<GasStation>> obsGS = BehaviorSubject<List<GasStation>>();

  BehaviorSubject<List<GasStation>> obsFavirites = BehaviorSubject<List<GasStation>>();

  BehaviorSubject<List<CardClass>> obsCards =
      BehaviorSubject<List<CardClass>>();

  BehaviorSubject<List<AutoClass>> obsCars = BehaviorSubject<List<AutoClass>>();

  BehaviorSubject<List<OrderClass>> obsReciepts =
      BehaviorSubject<List<OrderClass>>();

  BehaviorSubject<List<OferClass>> obsOfers =
      BehaviorSubject<List<OferClass>>();

  BehaviorSubject<List<ShareClass>> obsShared =
      BehaviorSubject<List<ShareClass>>();

  BehaviorSubject<List<QuestionClass>> obsQuestions =
      BehaviorSubject<List<QuestionClass>>();

  BehaviorSubject<GasStation> obsRefuelGS = BehaviorSubject<GasStation>();

  String apiUrl = 'https://mygs-spring-boot-kotlin-app.herokuapp.com/';

  DataCollector._internal(){

    buildDio();
    initPrefs();
    if (inited != true) {
      inited = true;

      // set up connection
      // buildDio();
      // initPrefs();

      // getSharedPreferences().then((prefs) {
      //   initToken();
      //   initUserId();
      //   initLanguage();
      // });
    }
  }
  
  Future<void> initPrefs() async {
    // logOut();
    prefs = await SharedPreferences.getInstance();
    userId = await int.parse(prefs!.getString('userId') ?? "-1");
    userName = await (prefs!.getString('userName') ?? "");
    userSurname = await (prefs!.getString('userSurname') ?? "");
    userPhone = await (prefs!.getString('userPhone') ?? "");
    userDOB = await (prefs!.getString('userDOB') ?? "");

    userGender = await  int.parse(prefs!.getString('userGender') ?? "0") ;

    userEmail = await (prefs!.getString('email') ?? "");
    accessToken = await (prefs!.getString('token') ?? "");
    receiveOfferNews = await (prefs!.getBool('receiveOfferNews') ?? false);
    receiveOrderInfo = await (prefs!.getBool('receiveOrderInfo') ?? false);
    callStaff = await (prefs!.getBool('callStaff') ?? false);

    withoutAuth = await (prefs!.getBool("withoutAuth") ?? false);

    // init first app start
    if (prefs!.getString("city") == null) {
      await prefs!.setString("city", cities[0]);
      city = cities[0];
    }

    if (prefs!.getString("lan") == null) {
      await prefs!.setString("lan", cities[0]);
      language = languages[0];
    }
    // buildDio();
    // todo - DELETE
    // gasStatons = await getGSAllShort();

    city = await prefs!.getString("city");
    language = await prefs!.getString("lan");

    await _getCurrentLocation();

    if (isLoggedIn()) {

      await getGSAllShort();
      await getOfers();
      await getQuestions();
      
      print("LOGED IN");
      await getGSFavorite();
      await getNearestGS();

      // nearestGS = await getNearestGS();
      // todo
      // GET ALL USER DATA
    }
    if(withoutAuth!){
      await getGSAllShort();
      await getOfers();
      await getQuestions();
      await getNearestGS();
      // todo
      // GET SOME DATA
    }

  }

Future<void> _getCurrentLocation() async{
  await geolocator
      .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
      .then((Position position) {

    _currentPosition = position;
  }).catchError((e) {
    print(e);
  });

  print("${_currentPosition!.latitude}, ${_currentPosition!.longitude}  ");
}


  bool isUnAthorizedVersion(){
    return withoutAuth!;
  }

  Future<void> continueWithoutLogin() async {
    await prefs!.setBool("withoutAuth", true);
  }

  Future<void> callStaffSet() async {
    await prefs!.setBool("callStaff", callStaff);
    callStaff = !callStaff!;
  }

  Future<void> exitFromUnathorizedVersion() async {
    await prefs!.setBool("withoutAuth", false);
  }

  setCity(city) async {
    await prefs!.setString("city", city);
    city = city;
  }

  Future<String> getCity() async {
    return await prefs!.getString("city") ?? "";
  }

  setNeedStaff() async {

    callStaff = callStaff!;
    await prefs!.setBool("callStaff", callStaff);

  }

  Future<bool> getNeedStaff() async {
    return await prefs!.getBool("callStaff") ?? false;
  }

  setLanguage(lan) async {
    await prefs!.setString("lan", lan);
    language = lan;
  }

  Future<String> getLanguage() async {
    return await prefs!.getString("lan") ?? "";
  }

  buildDio() {
    DateTime dateTime = DateTime.now();
    BaseOptions options = BaseOptions(
      baseUrl: apiUrl,
      connectTimeout: 15000000,
      receiveTimeout: 15000000,
      responseType: ResponseType.json,
    );
    dio = Dio(options);
    options.connectTimeout = 15000000;
    // set up authorization
    dio.interceptors.add(
        InterceptorsWrapper(onRequest: (RequestOptions options, handler) async {
      options.headers['Accept-Language'] = language;
      if (isLoggedIn()) {
        options.headers['Authorization'] = 'Bearer $accessToken';
      }
      return handler.next(options);
    }));
    dio.interceptors.add(LogInterceptor(responseBody: true));
  }

  bool isLoggedIn() {
    return accessToken != "";
  }

  Future<void> logOut() async {

    await prefs!.remove("userId");
    await prefs!.remove("userName");
    await prefs!.remove("userSurname");
    await prefs!.remove("userPhone");
    await prefs!.remove("userGender");

    await prefs!.remove("email");
    await prefs!.remove("token");

    await prefs!.remove("receiveOfferNews");
    await prefs!.remove("receiveOrderInfo");
    await prefs!.remove("userDOB");

    userId = -1;
    userName ="";
    userSurname = "";
    userPhone = "";
    userDOB = "";
    userGender = 0;
    userEmail = "";
    accessToken = "";
    receiveOfferNews = false;
    receiveOrderInfo = false;
    // callStaff = false;

    buildDio();
  }

  void setUserData(UserClass user) async{

    await prefs!.setString("userId", user.id.toString());
    await prefs!.setString("userName", user.name);
    await prefs!.setString("userSurname", user.surname);
    await prefs!.setString("userPhone", user.phone);
    await prefs!.setString("userGender", user.gender.toString());

    await prefs!.setString("email", user.email);
    await prefs!.setString("token", user.token);
    await prefs!.setString("userDOB", user.dob);

    await prefs!.setBool("receiveOfferNews", user.receiveOfferNews);
    await prefs!.setBool("receiveOrderInfo", user.receiveOrderInfo);


    userId = user.id;
    userName = user.name;
    userSurname = user.surname;
    userPhone = user.phone;
    userDOB = user.dob;
    userGender = user.gender ;
    userEmail = user.email;
    accessToken = user.token;

    receiveOfferNews = user.receiveOfferNews;
    receiveOrderInfo = user.receiveOrderInfo;

  }

  Future<Registration> createUser(UserClass user) async {
    Map<String, dynamic> postData = user.toRegistrationPostData();

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/reg', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      if (errCode == "USR003") return Registration.alreadyregistered;
      if(errCode == "USR002") return Registration.bademail;
      if(errCode == "USR004") return Registration.badphone;
      if(errCode == "USR005") return Registration.baduserdata;
      if(errCode == "USR006") return Registration.shortpass;

      return Registration.unknown;
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return Registration.networkError;
    }

    if (response.statusCode == 200) {
      buildDio();
      return Registration.success;
    }

    return Registration.unknown;
  }

  Future<VerifyAccount> verifyUserAccount(UserClass user) async {
    Map<String, dynamic> postData = user.toVerifyAccountData();

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/verify_account', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      if(errCode == "USR010") return VerifyAccount.badcode;
      if(errCode == "USR009") return VerifyAccount.alreadyverified;
      if(errCode == "USR007") return VerifyAccount.bademail;

      return VerifyAccount.unknown;
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return VerifyAccount.networkError;
    }

    if (response.statusCode == 200) {
      buildDio();
      
      var user = UserClass.fromJsonLogIn(response.data);
      setUserData(user);

      return VerifyAccount.success;
    }

    return VerifyAccount.unknown;
  }

  Future<LogIn> logIn(UserClass user) async {
    Map<String, dynamic> postData = user.toLogInData();

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/login', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      if(errCode == "USR007" || errCode == "USR006" || errCode == "USR008") return LogIn.baddata;
      if(errCode == "USR011") return LogIn.notyetverified;

      return LogIn.unknown;
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return LogIn.networkError;
    }

    if (response.statusCode == 200) {
      buildDio();
      
      var user = UserClass.fromJsonLogIn(response.data);
      setUserData(user);

      return LogIn.success;
    }

    return LogIn.unknown;
  }

  Future<VerCodeChangePass> sendCodeForChangePass(String email) async {

    // final Map<String, dynamic> postData = new Map<String, dynamic>();
    // postData['email'] = email;

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/$email');
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      if(errCode == "USR007") return VerCodeChangePass.bademail;

      return VerCodeChangePass.unknown;
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return VerCodeChangePass.networkError;
    }

    if (response.statusCode == 200) {
      buildDio();

      return VerCodeChangePass.success;
    }

    return VerCodeChangePass.unknown;
  }

  Future<ResetPassword> resetPassword(UserClass user) async {

    Map<String, dynamic> postData = user.toResestPassword();

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/reset_pas', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      if(errCode == "USR010") return ResetPassword.badcode;
      if(errCode == "USR012") return ResetPassword.pasmismatch;

      return ResetPassword.unknown;
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return ResetPassword.networkError;
    }

    if (response.statusCode == 200) {
      buildDio();
      return ResetPassword.success;
    }

    return ResetPassword.unknown;
  }


  Future<ResetPassword> updatePassword(UserClass user, String currPass) async {


    Map<String, dynamic> postData = new Map<String, dynamic>();
    postData['curPassword'] = currPass;
    postData['newPassword'] = user.password;
    postData['newPasswordAgain'] = user.newPassword;

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/reset_pas_auth', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      if(errCode == "USR012") return ResetPassword.pasmismatch;
      if(errCode == "USR013") return ResetPassword.wrongcurrpass;

      return ResetPassword.unknown;
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return ResetPassword.networkError;
    }

    if (response.statusCode == 200) {
      // buildDio();
      return ResetPassword.success;
    }

    return ResetPassword.unknown;
  }


  Future<String> updatePhone(UserClass user) async {

    Map<String, dynamic> postData = new Map<String, dynamic>();
    postData['newPhone'] = user.phone;

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/update_phone', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      return "";
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return "";
    }

    if (response.statusCode == 200) {
      // buildDio();
      await prefs!.setString("userPhone", user.phone);
      userPhone = user.phone;
      return response.data;
    }

    return "";
  }

  Future<String> sendNewEmailCode(UserClass user) async {

    Map<String, dynamic> postData = new Map<String, dynamic>();
    postData['newEmail'] = user.email;

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/update_email_req', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      return "";
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return "";
    }

    if (response.statusCode == 200) {
      // buildDio();
      // await prefs!.setString("userPhone", user.phone);
      return response.data;
    }

    return "";
  }

  Future<String> updateEmail(UserClass user) async {

    Map<String, dynamic> postData = new Map<String, dynamic>();
    postData['newEmail'] = user.email;
    postData['code'] = user.code;

    Response response;

    // perform action & receive response
    try {
      response = await dio.post('user/update_email', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      return "";
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return "";
    }

    if (response.statusCode == 200) {
      await prefs!.setString("email", user.email);
      userEmail = userEmail;
      // buildDio();
      // await prefs!.setString("userPhone", user.phone);
      return response.data;
    }

    return "";
  }

  Future<List<GasStation>> getGSAllShort() async {
    Map<String, dynamic> postData = {
      'latitude': _currentPosition!.latitude,
      'longitude': _currentPosition!.longitude
    };

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('gas_station/$city', data: postData);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200)  {
      List<GasStation> gs = await (response.data as List<dynamic>)
          .map((e) => GasStation.fromJsonShortInfo(e))
          .toList();

      obsGS.sink.add(gs);
      gasStatons = gs;

      return gs;
    }

    return [];
  }

  Future<FeedBack> getFeedBackGSIfExist(int gsId) async {

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.get('feedback/gs-get/$gsId');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return FeedBack();
    }

    if (response.statusCode == 200) {

      if(response.toString().length == 0) return FeedBack();

      FeedBack fb = FeedBack.fromJson(response.data);

      return fb;
    }

    return FeedBack();
  }

  Future<FeedBack> setFeedBackGS(FeedBack fb, int gsId) async {

    Map<String, dynamic> postData = fb.toJson();

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('feedback/gs/$gsId', data: postData);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return FeedBack();
    }

    if (response.statusCode == 200) {
      FeedBack fb = FeedBack.fromJson(response.data);

      return fb;
    }

    return FeedBack();
  }


  Future<FeedBack> getFeedBackOrderIfExist(int id) async {

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.get('feedback/gorder-get/$id');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return FeedBack();
    }

    if (response.statusCode == 200) {

      if(response.toString().length == 0) return FeedBack();

      FeedBack fb = FeedBack.fromJson(response.data);

      return fb;
    }

    return FeedBack();
  }

  Future<FeedBack> setFeedBackOrder(FeedBack fb, int id) async {

    Map<String, dynamic> postData = fb.toJson();

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('feedback/order/$id', data: postData);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return FeedBack();
    }
    if (response.statusCode == 200) {
      FeedBack fb = FeedBack.fromJson(response.data);

      return fb;
    }

    return FeedBack();
  }

  Future<List<GasStation>> getGSFiltered(List<int> fuelTypes, List<int> payMethods, List<int> services) async {
    Map<String, dynamic> coord = {
      'latitude': _currentPosition!.latitude,
      'longitude': _currentPosition!.longitude
    };

    this.fuelTypes = fuelTypes;
    this.payMethods = payMethods;
    this.services = services;

    Map<String, dynamic> postData = {
      'fuelTypes': fuelTypes,
      'paymentMethods': payMethods,
      'services': services,
      'city': city,
      'coordinates': coord
    };

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('gas_station/filter', data: postData);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<GasStation> gs = (response.data as List<dynamic>)
          .map((e) => GasStation.fromJsonShortInfo(e))
          .toList();

      obsGS.sink.add(gs);
      return gs;
    }

    return [];
  }


  Future<List<OrderClass>> getOrders() async {

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.get('order/');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<OrderClass> ord = (response.data as List<dynamic>)
          .map((e) => OrderClass.fromJson(e))
          .toList();

      obsReciepts.sink.add(ord);
      return ord;
    }

    return [];
  }

  Future<List<GasStation>> getGSByRequest(String req) async {
    Map<String, dynamic> coord = {
      'latitude': _currentPosition!.latitude,
      'longitude': _currentPosition!.longitude
    };

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('gas_station/search/${req}', data: coord);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<GasStation> gs = (response.data as List<dynamic>)
          .map((e) => GasStation.fromJsonShortInfo(e))
          .toList();

      obsGS.sink.add(gs);
      return gs;
    }

    return [];
  }

  Future<List<GasStation>> getGSFavorite() async {

    Map<String, dynamic> coord = {
      'latitude': _currentPosition!.latitude,
      'longitude': _currentPosition!.longitude
    };

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('gas_station/favorite', data: coord);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<GasStation> gs = (response.data as List<dynamic>)
          .map((e) => GasStation.fromJsonShortInfo(e))
          .toList();

      obsFavirites.sink.add(gs);
      return gs;
    }

    return [];
  }
  

  Future<void> changeGSLike(int gsId) async {


    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('gas_station/favorite/$gsId');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return;
    }

    if (response.statusCode == 200) {

      getGSFavorite();
      return;
    }

    return;
  }

  
  Future<void> onOffNotifEmail() async {
    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('user/notif-offer');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return;
    }

    if (response.statusCode == 200) {
      await prefs!.setBool("receiveOfferNews", !receiveOfferNews!);
      receiveOfferNews = !receiveOfferNews!;
      // await prefs!.setBool("receiveOrderInfo", user.receiveOrderInfo);
      return;
    }

    return;
  }

  Future<void> onOffNotifOrder() async {
    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('user/notif-order');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return;
    }

    if (response.statusCode == 200) {
      await prefs!.setBool("receiveOrderInfo", !receiveOrderInfo!);
      receiveOrderInfo = !receiveOrderInfo!;
      // await prefs!.setBool("receiveOrderInfo", user.receiveOrderInfo);
      return;
    }

    return;
  }

  Future<GasStation> getNearestGS() async {

      GasStation neares = gasStatons[0];

      for (GasStation i in gasStatons){
        if(i.distance! < neares.distance!){
          neares = i;
        }
      }
      nearestGS = neares;

      return neares;
  }

  Future<GasStation> getGSFullData(int gsId) async {
    Map<String, dynamic> postData = {
      'latitude': _currentPosition!.latitude,
      'longitude': _currentPosition!.longitude
    };

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('gas_station/info/$gsId', data: postData);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return GasStation();
    }

    if (response.statusCode == 200) {
      GasStation gs =  GasStation.fromJsonFullInfo(response.data);

      return gs;
    }

    return GasStation();
  }

  Future<GasStation> getRefuelGS(int gsId) async {
    Map<String, dynamic> postData = {
      'latitude': _currentPosition!.latitude,
      'longitude': _currentPosition!.longitude
    };

    Response response;
    try {
      // response = await mockRequest('mock/gas_stations.json');
      response = await dio.post('gas_station/info/$gsId', data: postData);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return GasStation();
    }

    if (response.statusCode == 200) {
      GasStation gs =  GasStation.fromJsonFullInfo(response.data);

      obsRefuelGS.sink.add(gs);
      return gs;
    }

    return GasStation();

  }

  Future<List<QuestionClass>> getQuestions() async {
    // Map<String, String> postData = {
    //   'email': email!,
    // };

    Response response;
    try {
      // response = await mockRequest('mock/questions.json');
      response = await dio.get('question/');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<QuestionClass> questions = (response.data as List<dynamic>)
          .map((e) => QuestionClass.fromJson(e))
          .toList();

      obsQuestions.sink.add(questions);
      return questions;
    }

    return [];
  }

  Future<List<OferClass>> getOfers() async {

    Response response;
    try {
      response = await dio.get('gas_station/offers');
    } on DioError catch (error) {
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<OferClass> ofers = (response.data as List<dynamic>)
          .map((e) => OferClass.fromJson(e))
          .toList();

      obsOfers.sink.add(ofers);
      return ofers;
    }

    return [];
  }

  Future<List<ShareClass>> getShared() async {
    // Map<String, String> postData = {
    //   'email': email!,
    // };

    Response response;
    try {
      response = await mockRequest('mock/share.json');
      // response = await dio.post('device/get_devices_house.php', data: postData);
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<ShareClass> shared = (response.data as List<dynamic>)
          .map((e) => ShareClass.fromJson(e))
          .toList();

      obsShared.sink.add(shared);
      return shared;
    }

    return [];
  }

  Future<List<CardClass>> getCards() async {
    // Map<String, String> postData = {
    //   'email': email!,
    // };

    Response response;
    try {
      // response = await mockRequest('mock/cards.json');
      response = await dio.get('card/');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<CardClass> cards = (response.data as List<dynamic>)
          .map((e) => CardClass.fromJson(e))
          .toList();

      obsCards.sink.add(cards);
      return cards;
    }

    return [];
  }

  Future<void> deleteCard(int cardId) async {
    // Map<String, String> postData = {
    //   'email': email!,
    // };

    Response response;
    try {
      // response = await mockRequest('mock/cards.json');
      response = await dio.delete('card/$cardId');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return ;
    }

    if (response.statusCode == 200) {

      return ;
    }

    return;
  }

  Future<NewCard> createCard(String cardNumber, DateTime dateExp, int cvv) async {
    
    Map<dynamic, dynamic> postData = CardClass().toJson(cardNumber, dateExp, cvv);

    Response response ;

    try {
      response = await dio.post('card/', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      if(errCode == "CRD002" || errCode == "CRD001") return NewCard.wrongnumber;
      if(errCode == "CRD004") return NewCard.expire;
      if(errCode == "CRD005") return NewCard.wrongcvv;

      return NewCard.faild;
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return NewCard.faild;
    }

    if (response.statusCode == 200) {
      return NewCard.success;
    }
    return NewCard.faild;
  }

  

  Future<List<AutoClass>> getCars() async {

    Response response;
    try {
      // response = await mockRequest('mock/cars.json');
      response = await dio.get('car/');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return [];
    }

    if (response.statusCode == 200) {
      List<AutoClass> cars = (response.data as List<dynamic>)
          .map((e) => AutoClass.fromJson(e))
          .toList();

      obsCars.sink.add(cars);
      return cars;
    }

    return [];
  }

  Future<NewCard> createCar(AutoClass auto) async {
    
    Map<dynamic, dynamic> postData = auto.toJson();

    Response response ;

    try {
      response = await dio.post('car/', data: postData);
    } on DioError catch (error) {
      print(error.response?.data['errorMessage']);

      String errCode = error.response?.data['errorCode'];
      String errMessage = error.response?.data['errorMessage'];

      print("ERROR FROM THE SERVER: $errCode, $errMessage");

      return NewCard.faild;
    } catch (e) {
      // in case of error, return error
      print('request failed: $e');
      return NewCard.faild;
    }

    if (response.statusCode == 200) {
      return NewCard.success;
    }
    return NewCard.faild;
  }

  Future<void> deleteCar(int carId) async {

    Response response;
    try {
      // response = await mockRequest('mock/cards.json');
      response = await dio.delete('car/$carId');
    } on DioError catch (error) {
      // in case of error, return error
      print(error);
      return ;
    }

    if (response.statusCode == 200) {

      return ;
    }

    return;
  }

  // Future<List<OrderClass>> getReciepts() async {
  //   // Map<String, String> postData = {
  //   //   'email': email!,
  //   // };

  //   Response response;
  //   try {
  //     response = await mockRequest('mock/reciepts.json');
  //     // response = await dio.post('device/get_devices_house.php', data: postData);
  //   } on DioError catch (error) {
  //     // in case of error, return error
  //     print(error);
  //     return [];
  //   }

  //   if (response.statusCode == 200) {
  //     List<OrderClass> res = (response.data as List<dynamic>)
  //         .map((e) => OrderClass.fromJson(e))
  //         .toList();

  //     obsReciepts.sink.add(res);
  //     return res;
  //   }

  //   return [];
  // }

  Future<Response> mockRequest(String url) async {
    await Future.delayed(const Duration(seconds: 1));
    String responseData = await rootBundle.loadString(url);
    RequestOptions? requestOptions = RequestOptions(path: url);
    return Response(
      statusCode: 200,
      data: jsonDecode(responseData),
      requestOptions: requestOptions,
    );
  }
}

enum Registration {
  success,
  alreadyregistered,
  bademail,
  badphone,
  shortpass,
  baduserdata,
  unknown,
  networkError
}

enum NewCard {
  success,
  wrongnumber,
  expire,
  wrongcvv,
  faild
}

enum VerifyAccount {
  success,
  badcode,
  bademail,
  alreadyverified,
  unknown,
  networkError
}

enum LogIn {
  success,
  baddata,
  notyetverified,
  unknown,
  networkError
}

enum VerCodeChangePass {
  success,
  bademail,
  unknown,
  networkError
}

enum ResetPassword {
  success,
  badcode,
  pasmismatch,
  wrongcurrpass,
  unknown,
  networkError
}
