import 'package:MyGS/classes/auto_class.dart';
import 'package:MyGS/classes/card_class.dart';
import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/textfields/access_textfield.dart';
import '../../components/tiles/dialog_tile.dart';
import '../../components/tiles/filter_drop_down_tile.dart';
import '../../components/tiles/gas_station_tile.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';
import 'package:intl/intl.dart';

class CarsPage extends StatefulWidget {
  const CarsPage({super.key});

  @override
  State<CarsPage> createState() => _CarsPageState();
}

class _CarsPageState extends State<CarsPage> {

  final DataCollector dc = DataCollector();


  void _modalBottomSheetMenu() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: _sheet(),
            );
          });
    });
  }

  TextEditingController _controllerNumber = TextEditingController();
  TextEditingController _controllerBrand = TextEditingController();

  late int selectedFuelType;

  @override
  void initState() {
    setState(() {
      selectedFuelType = -1;
    });
    
    super.initState();
    dc.getCars();
  }

  void removeCar(int id)async {
    await dc.deleteCar(id);
    await dc.getCars();
  }

  void addNewCar() async {

    if(_controllerNumber.text.isEmpty || _controllerBrand.text.isEmpty || selectedFuelType == -1)
      return sendMessage("car_error");

    AutoClass car = AutoClass(
      fuelType: selectedFuelType,
      number: _controllerNumber.text.trim(),
      brand: _controllerBrand.text.trim()
    );
    
    NewCard response = await dc.createCar(car);

    switch (response) {
      case NewCard.wrongnumber: return;
      case NewCard.expire: return;
      case NewCard.wrongcvv: return;
      case NewCard.faild: return sendMessage("car_error");
      case NewCard.success: {
        sendMessage("car_success", fromServer: true, isError: false);
          dc.getCars();
          _controllerBrand.text = "";
          _controllerNumber.text = "";
          selectedFuelType = -1;
        }
    }
  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : title) : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
        padding: EdgeInsets.all(font30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            
            StreamBuilder<List<AutoClass>>(
                stream: dc.obsCars,
                builder: (context, snapshot) {
                  List<AutoClass>? cars = snapshot.data;
                  if (cars == null || cars.isEmpty) {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: font400,
                      ),
                      child: Center(
                          child: Text(
                        AppLocalizations.of(context)
                            .translate('not_added_car_yet'),
                        style: styleInscriptions,
                      )),
                    );
                  } else {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: font400,
                      ),
                      child: ListView.builder(
                          itemCount: cars.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _carTile(cars[index]);
                          }),
                    );
                  }
                }),
            RegularButton(
                context: AppLocalizations.of(context).translate('add_new_car'),
                isGreen: true,
                onTap: () {
                  _modalBottomSheetMenu();
                })
          ],
        ));
  }

  Widget _carTile(AutoClass car) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: font8),
        child: DialogTile(
          action: Container(),
          title: "${car.brand} ${car.number}",
          icon: SvgPicture.asset(
                      "assets/icons/car.svg",
                    ),
          
          onTapRemove: () {
            removeCar(car.id!);
          },
        ));
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      title: Text(
        AppLocalizations.of(context).translate('cars'),
        style: styleTitleContent,
      ),
      centerTitle: true,
    );
  }

  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );

  Widget _sheet() {
    return makeDismissible(
      child: Container(
        height: font550,
        decoration: BoxDecoration(
            color: colorWhiteText,
            borderRadius: BorderRadius.vertical(top: Radius.circular(font25))),
        padding: EdgeInsets.symmetric(horizontal: font30, vertical: font49),
        child: ListView(
          // controller: controller,
          // clipBehavior: Clip.none,
          // mainAxisSize: MainAxisSize.min,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context).translate('add_new_car'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font20,
            ),
            SizedBox(
              height: font46,
              child: AuthorizationTextField(
                controller: _controllerNumber,
                text: AppLocalizations.of(context).translate('car_number'),
                tap: () {},
              ),
            ),
            SizedBox(
              height: font16,
            ),
            SizedBox(
              height: font46,
              child: AuthorizationTextField(
                controller: _controllerBrand,
                text: AppLocalizations.of(context).translate('car_brand'),
                tap: () {},
              ),
            ),
            SizedBox(
              height: font16,
            ),
            FilterDropDownTile(
              title: AppLocalizations.of(context).translate('fuel_type'),
              content: _fuelType(),
            ),
            SizedBox(
              height: font32,
            ),
            RegularButton(context: AppLocalizations.of(context).translate('add'), onTap: (){addNewCar(); Navigator.pop(context);}, isGreen: true,)
          ],
        ),
      ),
    );
  }

  Widget _fuelType() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: font6),
        child: Wrap(
          children: [
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: () {
                  setState(() {
                    selectedFuelType = 0;
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  height: font42,
                  width: font66,
                  decoration: BoxDecoration(
                      color: selectedFuelType == 0 ? colorLightGreen : colorLightWhite,
                      borderRadius: BorderRadius.circular(font5)),
                  child: Text(
                    "Disel",
                    textAlign: TextAlign.center,
                    style: styleTitleContent.copyWith(fontSize: font14),
                    softWrap: true,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    selectedFuelType = 1;
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  height: font42,
                  width: font66,
                  decoration: BoxDecoration(
                      color: selectedFuelType == 1 ? colorLightGreen : colorLightWhite,
                      borderRadius: BorderRadius.circular(font5)),
                  child: Text(
                    "Petrol",
                    textAlign: TextAlign.center,
                    style: styleTitleContent.copyWith(fontSize: font14),
                    softWrap: true,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    selectedFuelType = 2;
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  height: font42,
                  width: font66,
                  decoration: BoxDecoration(
                      color: selectedFuelType == 2 ? colorLightGreen : colorLightWhite,
                      borderRadius: BorderRadius.circular(font5)),
                  child: Text(
                    "LPG",
                    textAlign: TextAlign.center,
                    style: styleTitleContent.copyWith(fontSize: font14),
                    softWrap: true,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    selectedFuelType = 3;
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  height: font42,
                  width: font66,
                  decoration: BoxDecoration(
                      color: selectedFuelType == 3 ? colorLightGreen : colorLightWhite,
                      borderRadius: BorderRadius.circular(font5)),
                  child: Text(
                    "CNG",
                    textAlign: TextAlign.center,
                    style: styleTitleContent.copyWith(fontSize: font14),
                    softWrap: true,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}