import 'package:MyGS/pages/menu/settings/account_setting_page.dart';
import 'package:MyGS/pages/menu/settings/change_pas_page.dart';
import 'package:MyGS/pages/menu/settings/email_setting_page.dart';
import 'package:MyGS/pages/menu/settings/language_setting_page.dart';
import 'package:MyGS/pages/menu/settings/phone_setting_page.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../../classes/language_singleton.dart';
import '../../../components/modals/info_bottom_flushbar.dart';
import '../../../components/tiles/regular_tile.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/text_styles.dart';
import '../../access/no_access_page.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String selectedLang = "";

  bool isDisable = false;
  bool emailNotif = false;
  bool electrRec = false;

  DataCollector dc = DataCollector();


  @override
  void initState() {
    super.initState();
    selectedLang = SharedPrefLanguageSingleton().language;
    emailNotif = dc.receiveOfferNews!;
    electrRec = dc.receiveOrderInfo!;
    isDisable = dc.callStaff!;
  }

  List<String> languages = ["English", "Czech", "Russian"];

  void submitPersData() {
     if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
    else
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => AccountSettingPage()));
  }

  void submitChangePassword() {

    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
    else
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ChangePasswordPage()));
  }

  void submitPhone() {
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
    else
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => PhoneSettingPage()));
  }

  void submitEmail() {
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
    else
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => EmailSettingPage()));
  }

  void submitLanguage() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => LanguageSettingPage(
              currentLan: selectedLang,
              languages: languages,
            )));
  }

  void submitRecEmailNotif() async{
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate("access_denied"),
        message: AppLocalizations.of(context).translate("access_denied_desc"),
        isError: true);
    else
      await dc.onOffNotifEmail();
  }

  void submitRecElecNotif() async{
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate("access_denied"),
        message: AppLocalizations.of(context).translate("access_denied_desc"),
        isError: true);
    else
      await dc.onOffNotifOrder();
  }

  void submitCallStaff() async{
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate("access_denied"),
        message: AppLocalizations.of(context).translate("access_denied_desc"),
        isError: true);
    else
      await dc.callStaffSet();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('settings'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppLocalizations.of(context).translate('account'),
                  style: styleTitleMenu,
                ),
                SizedBox(
                  height: font10,
                ),
                RegularTile(
                  onTap: () {submitPersData();},
                  title: "personal_data",
                ),
                SizedBox(
                  height: font6,
                ),
                RegularTile(
                  onTap: () {submitChangePassword();},
                  title: "change_password",
                ),
                SizedBox(
                  height: font28,
                ),
                Text(
                  AppLocalizations.of(context).translate('contact_information'),
                  style: styleTitleMenu,
                ),
                SizedBox(
                  height: font10,
                ),
                RegularTile(
                  onTap: () {submitPhone();},
                  title: "pone",
                ),
                SizedBox(
                  height: font6,
                ),
                RegularTile(
                  onTap: () {submitEmail();},
                  title: "email",
                ),
                SizedBox(
                  height: font28,
                ),

                // Language
                InkWell(
                  onTap: () {
                    submitLanguage();
                  },
                  child: Container(
                    height: font46,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocalizations.of(context).translate("language"),
                          style: styleTitleMenu,
                        ),
                        Row(
                          children: [
                            Text(
                              selectedLang,
                              style: styleTitle2,
                            ),
                            SizedBox(
                              width: font9,
                            ),
                            SvgPicture.asset(
                              "assets/icons/right_simple_arrow.svg",
                              height: font14,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: font28,
                ),

                Text(
                  AppLocalizations.of(context).translate('notifications'),
                  style: styleTitleMenu,
                ),
                SizedBox(
                  height: font10,
                ),

                RegularTile(
                  onTap: () {submitRecEmailNotif();},
                  title: "receive_email_newsletters",
                  isSwitcher: true,
                  switcherValue: emailNotif,
                ),

                SizedBox(
                  height: font6,
                ),
                RegularTile(
                  onTap: () {submitRecElecNotif();},
                  title: "receive_electronic_receipt",
                  isSwitcher: true,
                  switcherValue: electrRec,
                ),

                SizedBox(
                  height: font28,
                ),

                Text(
                  AppLocalizations.of(context).translate('disability'),
                  style: styleTitleMenu,
                ),
                SizedBox(
                  height: font10,
                ),

                RegularTile(
                  onTap: () {submitCallStaff();},
                  title: "always_call_staff",
                  isSwitcher: true,
                  switcherValue: isDisable,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
