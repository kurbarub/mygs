import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../../classes/user_class.dart';
import '../../../components/modals/info_bottom_flushbar.dart';
import '../../../components/textfields/access_textfield.dart';
import '../../../services/data_collector.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/text_styles.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({super.key});

  @override
  State<ChangePasswordPage> createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {

  TextEditingController _currentPasController = TextEditingController();

  TextEditingController _firstNewPasController = TextEditingController();
  TextEditingController _secondNewPasController = TextEditingController();

  DataCollector dc = DataCollector();

  void submitSave()async {

    if (_firstNewPasController.text.trim().isEmpty || _secondNewPasController.text.trim().isEmpty || _currentPasController.text.isEmpty)
        return sendMessage("empty_pass");

    UserClass user = UserClass(
      password: _firstNewPasController.text.trim(),
      newPassword: _secondNewPasController.text.trim()
    );
    
    ResetPassword response = await dc.updatePassword(user, _currentPasController.text.trim());

    switch (response) {
      case ResetPassword.badcode: return;
      case ResetPassword.pasmismatch: return sendMessage("USR012", fromServer: true);
      case ResetPassword.networkError: return sendMessage("network_error", fromServer: true);
      case ResetPassword.unknown: return sendMessage("unknown_error", fromServer: true);
      case ResetPassword.wrongcurrpass: return sendMessage("USR013", fromServer: true);
      case ResetPassword.success: {
        sendMessage("reset_pass_seccess", fromServer: true, isError: false);
        setState(() {
          _currentPasController.text = "";
          _firstNewPasController.text = "";
          _secondNewPasController.text = "";
        });
        }
    }
  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : title) : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('password'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body(){

    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(font30),
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(AppLocalizations.of(context).translate('current_password'), style: styleTitle2,),
                SizedBox(height: font20,),
                AuthorizationTextField(
                  controller: _currentPasController,
                  text: AppLocalizations.of(context).translate('password'),
                  tap: () {},
                  isPassword: true,
                ),
              ],
            ),
            SizedBox(height: font28,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(AppLocalizations.of(context).translate('new_password'), style: styleTitle2,),
                SizedBox(height: font20,),
                AuthorizationTextField(
                  controller: _firstNewPasController,
                  text: AppLocalizations.of(context).translate('create_password'),
                  tap: () {},
                  isPassword: true,
                ),
                SizedBox(height: font16,),
                AuthorizationTextField(
                  controller: _secondNewPasController,
                  text: AppLocalizations.of(context).translate('confirm_password'),
                  tap: () {},
                  isPassword: true,
                ),
              ],
            ),
            SizedBox(height: font32,),
            RegularButton(context: AppLocalizations.of(context).translate('save'), onTap: (){submitSave();}, isGreen: true,)
          ],
        ),
      ),
    );

  }

}