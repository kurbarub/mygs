import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../../components/textfields/access_textfield.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/colors.dart';
import '../../../utility/variables/sizes.dart';
import '../../../utility/variables/text_styles.dart';
import 'package:intl/intl.dart';

class AccountSettingPage extends StatefulWidget {
  const AccountSettingPage({super.key});

  @override
  State<AccountSettingPage> createState() => _AccountSettingPageState();
}

class _AccountSettingPageState extends State<AccountSettingPage> {
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerSurname = TextEditingController();
  DataCollector dc = DataCollector();

  late String name;
  late String surname;

  late String selectedDateView;

  late int selectedGender;

  @override
  void initState() {

    name = dc.userName!;
    surname = dc.userSurname!;

    selectedDateView = DateFormat("dd.MM.yyyy").format(DateTime.parse(dc.userDOB!)).toString();
    selectedGender = dc.userGender!;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('account'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: Column(
        children: [
          AuthorizationTextField(
            controller: _controllerName,
            text: name,
            tap: () {},
            isReadOnly: true,
          ),
          SizedBox(
            height: font16,
          ),
          AuthorizationTextField(
            controller: _controllerSurname,
            text: surname,
            tap: () {},
            isReadOnly: true,
          ),
          SizedBox(
            height: font16,
          ),
          Row(
            children: [
              Expanded(
                flex: 4,
                child: Material(
                  shadowColor: colorBlack,
                  elevation: font4,
                  borderRadius: BorderRadius.circular(font5),
                  child: Container(
                    height: font50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(font5),
                      color: colorTextfieldBackround,
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: font20, vertical: font12),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            selectedDateView.toString(),
                            style: styleInscriptions,
                          ),
                          SvgPicture.asset('assets/icons/bottom_arrow.svg'),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: font18,
              ),
              Text(
                AppLocalizations.of(context).translate('gender'),
                style: styleInscriptions,
              ),
              SizedBox(
                width: font10,
              ),
              Expanded(
                flex: 1,
                child: Material(
                  shadowColor: colorBlack,
                  elevation: font4,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(font5),
                      topLeft: Radius.circular(font5)),
                  child: Container(
                    height: font50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(font5),
                          topLeft: Radius.circular(font5)),
                      color: selectedGender != 0
                          ? colorTextfieldBackround
                          : colorMain.withOpacity(0.5),
                    ),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: font10, vertical: font14),
                        child: SvgPicture.asset('assets/icons/male.svg'),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: font2,
              ),
              Expanded(
                flex: 1,
                child: Material(
                  shadowColor: colorBlack,
                  elevation: font4,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(font5),
                      topRight: Radius.circular(font5)),
                  child: Container(
                    height: font50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(font5),
                          topRight: Radius.circular(font5)),
                      color: selectedGender != 1
                          ? colorTextfieldBackround
                          : colorMain.withOpacity(0.5),
                    ),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: font10, vertical: font12),
                        child: SvgPicture.asset(
                          'assets/icons/female.svg',
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
