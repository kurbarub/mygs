import 'package:MyGS/pages/menu/settings/email_confirm_page.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../../classes/user_class.dart';
import '../../../components/buttons/regular_button.dart';
import '../../../components/modals/info_bottom_flushbar.dart';
import '../../../components/textfields/access_textfield.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/sizes.dart';
import '../../../utility/variables/text_styles.dart';

class EmailSettingPage extends StatefulWidget {
  const EmailSettingPage({super.key});

  @override
  State<EmailSettingPage> createState() => _EmailSettingPageState();
}

class _EmailSettingPageState extends State<EmailSettingPage> {
  TextEditingController _emailController = TextEditingController();
  DataCollector dc = DataCollector();
  late String email;


  @override
  void initState() {
    email = dc.userEmail!;
    super.initState();
  }

  void submitSave()async {
    if (_emailController.text.trim().isEmpty)
        return sendMessage("empty_email");

    UserClass user = UserClass(
      email: _emailController.text.trim(),
    );
    
    String response = await dc.sendNewEmailCode(user);

    if(response != ""){
      
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) =>  EmailConfirmPage(newEmail: user.email!)),
    );
    }
  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : title) : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('email'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppLocalizations.of(context).translate('edit_email'),
            style: styleTitle2,
          ),
          SizedBox(
            height: font20,
          ),
          AuthorizationTextField(
            controller: _emailController,
            text: email,
            tap: () {},
            // isPassword: true,
          ),
          SizedBox(
            height: font32,
          ),
          RegularButton(
            context: AppLocalizations.of(context).translate('save'),
            onTap: () {
              submitSave();
            },
            isGreen: true,
          )
        ],
      ),
    );
  }
}
