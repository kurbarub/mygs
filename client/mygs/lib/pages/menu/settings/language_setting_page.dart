import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../../components/tiles/custom_selector.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/text_styles.dart';

class LanguageSettingPage extends StatefulWidget {
  LanguageSettingPage(
      {super.key, required this.currentLan, required this.languages});

  String currentLan;
  final List<String> languages;

  @override
  State<LanguageSettingPage> createState() => _LanguageSettingPageState();
}

class _LanguageSettingPageState extends State<LanguageSettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('language'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: Column(
        children: [
          CustomListSelector(
            onSelected: (){},
            selectedItem: widget.currentLan,
            items: widget.languages,
            isMaxWidth: true,
            isLanguage: true,
          ),
        ],
      ),
    );
  }
}
