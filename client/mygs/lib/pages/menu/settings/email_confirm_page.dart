import 'package:flutter/material.dart';

import '../../../classes/user_class.dart';
import '../../../components/buttons/regular_button.dart';
import '../../../components/buttons/text_button.dart';
import '../../../components/modals/info_bottom_flushbar.dart';
import '../../../components/textfields/access_textfield.dart';
import '../../../services/data_collector.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/sizes.dart';
import '../../../utility/variables/text_styles.dart';

class EmailConfirmPage extends StatefulWidget {
  const EmailConfirmPage({super.key, required this.newEmail});

  final String newEmail;

  @override
  State<EmailConfirmPage> createState() => _EmailConfirmPageState();
}

class _EmailConfirmPageState extends State<EmailConfirmPage> {
  int timer = 20;

  @override
  Widget build(BuildContext context) {
    
    
    final TextEditingController codeController = TextEditingController();

    String mess = AppLocalizations.of(context).translate('confirmation_code_sent');
    DataCollector dc = DataCollector();


    void submitSave() async{

      UserClass user = UserClass(
        code: int.parse(codeController.text.trim()),
        email: widget.newEmail
      );
    
      String response = await dc.updateEmail(user);

      if(response != ""){
        dc.logOut();
        Navigator.pop(context);
        Navigator.pop(context);
      };

    }

    void submitSendAgain() {}

    return Scaffold(
        // resizeToAvoidBottomInset: false,
        appBar: _appBar(),
        body: Padding(
          padding: EdgeInsets.all(font30),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppLocalizations.of(context).translate('confirmation_code'),
                style: styleTitle2,
              ),
              SizedBox(
                height: font20,
              ),
              Text(mess, style: styleInscriptionsGreen.copyWith(fontSize: font14),),
              SizedBox(
                height: font8,
              ),
              AuthorizationTextField(
                controller: codeController,
                text: AppLocalizations.of(context).translate('code'),
                tap: () {},
              ),
              SizedBox(
                height: font8,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '$timer s',
                        style:
                            styleInscriptionsGreen.copyWith(fontSize: font14),
                      ),
                      ButtonText(
                          text: AppLocalizations.of(context)
                              .translate('send_again'),
                          onTap: () {
                            submitSendAgain();
                          })
                    ],
                  )
                ],
              ),
              SizedBox(
                height: font28,
              ),
              RegularButton(
                context: AppLocalizations.of(context).translate('save'),
                onTap: () {
                  submitSave();
                },
                isGreen: true,
              ),
            ],
          ),
        ));
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      title: Text(
        AppLocalizations.of(context).translate('email'),
        style: styleTitleContent,
      ),
      centerTitle: true,
    );
  }
}
