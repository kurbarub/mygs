import 'package:MyGS/services/data_collector.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../../classes/user_class.dart';
import '../../../components/buttons/regular_button.dart';
import '../../../components/modals/info_bottom_flushbar.dart';
import '../../../components/textfields/access_textfield.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/text_styles.dart';

class PhoneSettingPage extends StatefulWidget {
  const PhoneSettingPage({super.key});

  @override
  State<PhoneSettingPage> createState() => _PhoneSettingPageState();
}

class _PhoneSettingPageState extends State<PhoneSettingPage> {

  TextEditingController _phoneController = TextEditingController();

  DataCollector dc = DataCollector();
  late String phoneNumber;

  @override
  void initState() {

    phoneNumber = dc.userPhone!;
    super.initState();
  }

  void submitSave()async{
    
    if (_phoneController.text.trim().isEmpty)
        return sendMessage("empty_phone");

    UserClass user = UserClass(
      phone: _phoneController.text.trim()
    );
    
    String response = await dc.updatePhone(user);

    if(response != ""){
      sendMessage("reset_phone_success", fromServer: true, isError: false);
      setState(() {
        phoneNumber = _phoneController.text.trim();
      });
    }

  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : title) : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('pone'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppLocalizations.of(context).translate('edit_phone_number'),
            style: styleTitle2,
          ),
          SizedBox(
            height: font20,
          ),
          AuthorizationTextField(
            controller: _phoneController,
            text: phoneNumber,
            tap: () {},
            // isPassword: true,
          ),
          SizedBox(height: font32,),
          RegularButton(context: AppLocalizations.of(context).translate('save'), onTap: (){submitSave();}, isGreen: true,)
        ],
      ),
    );
  }
}
