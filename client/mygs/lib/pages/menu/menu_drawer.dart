import 'package:MyGS/classes/city_singleton.dart';
import 'package:MyGS/components/dividers/regular_divider.dart';
import 'package:MyGS/components/tiles/custom_selector.dart';
import 'package:MyGS/pages/home/questions_page.dart';
import 'package:MyGS/pages/menu/payments_page.dart';
import 'package:MyGS/pages/menu/settings/settings_page.dart';
import 'package:MyGS/pages/menu/share/share_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:restart_app/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../components/buttons/text_button.dart';
import '../../components/modals/info_bottom_flushbar.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/cities.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import '../access/splash_page.dart';
import 'about_app_page.dart';
import 'cars_page.dart';

class MenuDrawer extends StatefulWidget {
  const MenuDrawer({Key? key}) : super(key: key);

  @override
  State<MenuDrawer> createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {

  DataCollector dc = DataCollector();

  String selectedCity = "";

  late CustomListSelector cs;

  void submitSettings() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const SettingsPage()));
  }

  void submitPayments() {
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate("access_denied"),
        message: AppLocalizations.of(context).translate("access_denied_desc"),
        isError: true);
    else
      Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const PaymentsPage()));
  }

  void submitCars() {
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate("access_denied"),
        message: AppLocalizations.of(context).translate("access_denied_desc"),
        isError: true);
    else
      Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const CarsPage()));
  }

  // void submitShare() {
  //   Navigator.of(context)
  //       .push(MaterialPageRoute(builder: (context) => const SharePage()));
  // }

  void submitAboutApp() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const AboutAppPage()));
  }

  void updateCity()async{

    // dc = new DataCollector();
    // Navigator.pop(context);

    String newCity = await dc.getCity();

    setState(()  {
      selectedCity = newCity;
      cs = CustomListSelector(
        onSelected: updateCity,
        selectedItem: newCity,
        items: cities,
        isCity: true,
      );
    });
    print( dc.city!);

    Navigator.of(context)
      .pushReplacement(MaterialPageRoute(builder: (context) => const SplashPage()));
  }

  void submitHelp() {
    Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => const QuestionsPage()));
  }

  void submitLogOut()async{
    
    if(dc.isUnAthorizedVersion()){
      await dc.exitFromUnathorizedVersion();
    }else{
      await dc.logOut();
    }

    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => const SplashPage()));

  }

  void closeDrawer() {
    Navigator.pop(context);
  }

  @override
  void initState() {
    selectedCity = dc.city!;
    cs = CustomListSelector(
      onSelected: updateCity,
      selectedItem: selectedCity,
      items: cities,
      isCity: true,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: font270,
      backgroundColor: colorMenuPanel,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: font30),
        child: ListView(
          children: [
            Padding(
              padding: EdgeInsets.only(top: font70, bottom: font28),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: InkWell(
                      onTap: () {
                        closeDrawer();
                      },
                      child: SvgPicture.asset(
                        'assets/icons/cross.svg',
                      ),
                    ),
                  ),
                  SizedBox(
                    height: font77,
                  ),
                  Align(alignment: Alignment.bottomCenter, child: cs),
                ],
              ),
            ),
            RegularDivider(height: font2),
            SizedBox(
              height: font32,
            ),
            InkWell(
              onTap: () {
                submitSettings();
              },
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    "assets/images/menu/settings.png",
                    width: font50,
                  ),
                  SizedBox(
                    width: font12,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: font12),
                    child: Text(
                      AppLocalizations.of(context).translate('settings'),
                      style: styleTitle2,
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: font16,
            ),
            InkWell(
              onTap: () {
                submitPayments();
              },
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    "assets/images/menu/payments.png",
                    width: font50,
                  ),
                  SizedBox(
                    width: font12,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: font12),
                    child: Text(
                      AppLocalizations.of(context).translate('payments'),
                      style: styleTitle2,
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: font16,
            ),
            InkWell(
              onTap: () {
                submitCars();
              },
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    "assets/images/menu/cars.png",
                    width: font50,
                  ),
                  SizedBox(
                    width: font12,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: font12),
                    child: Text(
                      AppLocalizations.of(context).translate('cars'),
                      style: styleTitle2,
                    ),
                  )
                ],
              ),
            ),
            // SizedBox(
            //   height: font16,
            // ),
            // InkWell(
            //   onTap: () {
            //     submitShare();
            //   },
            //   child: Row(
            //     // mainAxisAlignment: MainAxisAlignment.start,
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       Image.asset(
            //         "assets/images/menu/share.png",
            //         width: font50,
            //       ),
            //       SizedBox(
            //         width: font12,
            //       ),
            //       Padding(
            //         padding: EdgeInsets.only(top: font12),
            //         child: Text(
            //           AppLocalizations.of(context).translate('share'),
            //           style: styleTitle2,
            //         ),
            //       )
            //     ],
            //   ),
            // ),
            SizedBox(
              height: font32,
            ),
            RegularDivider(height: font2),
            SizedBox(
              height: font32,
            ),
            ButtonText(
              onTap: () {
                submitAboutApp();
              },
              text: AppLocalizations.of(context).translate('about_app'),
            ),
            SizedBox(
              height: font8,
            ),
            ButtonText(
              onTap: () {
                submitHelp();
              },
              text: AppLocalizations.of(context).translate('help'),
            ),
            SizedBox(
              height: font8,
            ),
            ButtonText(
              onTap: () {
                submitLogOut();
              },
              text: dc.isUnAthorizedVersion()?AppLocalizations.of(context).translate('sign_in'): AppLocalizations.of(context).translate('logout'),
            ),
          ],
        ),
      ),
    );
  }
}
