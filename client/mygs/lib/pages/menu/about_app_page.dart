import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class AboutAppPage extends StatefulWidget {
  const AboutAppPage({super.key});

  @override
  State<AboutAppPage> createState() => _AboutAppPageState();
}

class _AboutAppPageState extends State<AboutAppPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
        padding: EdgeInsets.all(font30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                      child: Image.asset("assets/images/logo.png"),
                      width: font200,
                      height: font80,
                    ),
              ],
            ),

            Text(
              AppLocalizations.of(context).translate('about_app'),
              style: styleTitleContent,
            ),
            SizedBox(height: font10,),
            Text(AppLocalizations.of(context).translate('about_app_d'),),
            SizedBox(height: font10,),
            Text("GitLab:", style: styleTitleContent,),
            SizedBox(height: font10,),
            InkWell(
              child: new Text('https://gitlab.fel.cvut.cz/kurbarub/mygs'),
              onTap: () => launch('https://gitlab.fel.cvut.cz/kurbarub/mygs')
            ),
            SizedBox(height: font10,),
            Text("Developed by:", style: styleTitleContent,),
            SizedBox(height: font10,),
            Text("Ruben Kurbanov"),
            
          ],
        ));
  }

  PreferredSizeWidget _appBar(){
    return AppBar(
      title: Text(
        AppLocalizations.of(context).translate('about_app'),
        style: styleTitleContent,
      ),
      centerTitle: true,
    );

  }
}