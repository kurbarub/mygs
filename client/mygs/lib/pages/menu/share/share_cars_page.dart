import 'dart:math';

import 'package:MyGS/classes/auto_class.dart';
import 'package:MyGS/classes/card_class.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../../components/buttons/regular_button.dart';
import '../../../services/data_collector.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/colors.dart';
import '../../../utility/variables/sizes.dart';
import '../../../utility/variables/text_styles.dart';

class ShareCarsBottomSheet extends StatefulWidget {
  ShareCarsBottomSheet({super.key, required this.shared});

  List<AutoClass> shared;

  @override
  State<ShareCarsBottomSheet> createState() =>
      _ShareCarsBottomSheetState();
}

class _ShareCarsBottomSheetState extends State<ShareCarsBottomSheet> {
  final DataCollector dc = DataCollector();

  late List<AutoClass> newShared;

  @override
  void initState() {
    super.initState();
    dc.getCars();
    newShared = List<AutoClass>.from(widget.shared);
  }

  void submitSave() {
    // widget.shared = newShared;

    Navigator.pop(context);
  }

  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );

  void selectCar(AutoClass car) {
    if (newShared.any((element) => element.id == car.id)) {
      setState(() {
        newShared.removeWhere((element) => car.id == element.id);
      });
    } else {
      setState(() {
        newShared.add(car);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return makeDismissible(
      child: Container(
        height: font600,
        decoration: BoxDecoration(
            color: colorWhiteText,
            borderRadius: BorderRadius.vertical(top: Radius.circular(font25))),
        padding: EdgeInsets.symmetric(horizontal: font30, vertical: font49),
        child: ListView(
          // controller: controller,
          clipBehavior: Clip.none,
          // mainAxisSize: MainAxisSize.min,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context).translate('select_cars'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font24,
            ),
            SizedBox(
              height: font400,
              child: StreamBuilder<List<AutoClass>>(
                stream: dc.obsCars,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<AutoClass> cars = snapshot.data!;

                    if (cars.isEmpty) {
                      return Text("No");
                    } else {
                      return ListView.builder(
                          itemCount: cars.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Column(
                              children: [
                                SizedBox(
                                  height: font20,
                                ),
                                InkWell(
                                  onTap: () {
                                    selectCar(cars[index]);
                                  },
                                  child: Container(
                                    height: font42,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: colorTextfieldBackround,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SizedBox(width: font20,),
                                        Container(
                                            height: font25,
                                            width: font25,
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: font2,
                                                    color: colorBlack),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        font5)),
                                            alignment: Alignment.center,
                                            child: newShared.any((e) =>
                                                    e.id == cars[index].id)
                                                ? SvgPicture.asset(
                                                    "assets/icons/check_mark.svg",
                                                  )
                                                : Container()),
                                        SizedBox(width: font50,),
                                        Row(
                                          children: [
                                            SvgPicture.asset(
                                                    "assets/icons/car.svg",
                                                  ),
                                            SizedBox(
                                              width: font12,
                                            ),
                                            Text(
                                                "${cars[index].number}")
                                          ],
                                        ),
                                        Text('')
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          });
                    }
                  } else {
                    return Container();
                  }
                },
              ),
            ),
            RegularButton(
              context: AppLocalizations.of(context).translate('save'),
              onTap: () {
                submitSave();
              },
              isGreen: true,
            )
          ],
        ),
      ),
    );
  }
}
