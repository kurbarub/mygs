import 'package:MyGS/classes/share_class.dart';
import 'package:MyGS/pages/menu/share/share_cars_page.dart';
import 'package:MyGS/pages/menu/share/share_payment_page.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../../classes/auto_class.dart';
import '../../../classes/card_class.dart';
import '../../../components/buttons/regular_button.dart';
import '../../../components/buttons/text_button.dart';
import '../../../components/dividers/widget_divider.dart';
import '../../../components/textfields/access_textfield.dart';
import '../../../components/tiles/dialog_tile.dart';
import '../../../services/data_collector.dart';
import '../../../utility/translation/app_localization.dart';
import '../../../utility/variables/colors.dart';
import '../../../utility/variables/text_styles.dart';

class SharePage extends StatefulWidget {
  const SharePage({super.key});

  @override
  State<SharePage> createState() => _SharePageState();
}

class _SharePageState extends State<SharePage> {
  final DataCollector dc = DataCollector();

  void _modalBottomSheet(ShareClass share) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: _sheet(share),
            );
          });
    });
  }

  @override
  void initState() {
    super.initState();
    dc.getShared();
    dc.getCards();
  }

  void removeInvUser(String user) {
    print("remove");
  }

  void submitShareData(ShareClass share) {
    _modalBottomSheet(share);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('share'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
        padding: EdgeInsets.all(font30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            StreamBuilder<List<ShareClass>>(
                stream: dc.obsShared,
                builder: (context, snapshot) {
                  List<ShareClass>? share = snapshot.data;
                  if (share == null || share.isEmpty) {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: font400,
                      ),
                      child: Center(
                          child: Text(
                        AppLocalizations.of(context)
                            .translate('not_shared_yet'),
                        style: styleInscriptions,
                      )),
                    );
                  } else {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: font400,
                      ),
                      child: ListView.builder(
                          itemCount: share.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _invUserTile(share[index]);
                          }),
                    );
                  }
                }),
            RegularButton(
                context: AppLocalizations.of(context).translate('share_data'),
                isGreen: true,
                onTap: () {
                  submitShareData(ShareClass());
                })
          ],
        ));
  }

  Widget _invUserTile(ShareClass share) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: font8),
        child: InkWell(
          onTap: () {
            submitShareData(share);
          },
          child: DialogTile(
            action: Row(
              children: [
                SizedBox(
                  width: font20,
                ),
                Padding(
                  padding: EdgeInsets.only(right: font12),
                  child: SvgPicture.asset(
                    "assets/icons/more_action.svg",
                    width: font2,
                    height: font20,
                  ),
                ),
              ],
            ),
            title: "${share.invitedUser}",
            icon: SvgPicture.asset(
              "assets/icons/user.svg",
            ),
            // isShared: car.isShared!,
            onTapRemove: () {
              removeInvUser(share.invitedUser!);
            },
          ),
        ));
  }

  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );

  TextEditingController _emailController = TextEditingController();

  void submitAddPayments(List<CardClass>? sharedPayments) {
    
    // Navigator.pop(context);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: SharePaymentBottomSheet(
                shared: sharedPayments ?? [],
              ),
            );
          });
    });
  }

  void submitAddCars(List<AutoClass>? sharedCars) {
    
    // Navigator.pop(context);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: ShareCarsBottomSheet(
                shared: sharedCars ?? [],
              ),
            );
          });
    });
  }


  Widget _sheet(ShareClass share) {
    void submitClearPayments() {}

    void submitClearCars() {}

    return makeDismissible(
      child: Container(
        height: font600,
        decoration: BoxDecoration(
            color: colorWhiteText,
            borderRadius: BorderRadius.vertical(top: Radius.circular(font25))),
        padding: EdgeInsets.symmetric(horizontal: font30, vertical: font49),
        child: ListView(
          // controller: controller,
          clipBehavior: Clip.none,
          // mainAxisSize: MainAxisSize.min,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context).translate('share_data'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font24,
            ),
            SizedBox(
              height: font50,
              child: AuthorizationTextField(
                controller: _emailController,
                isReadOnly: share.invitedUser != null,
                text: share.invitedUser == null
                    ? AppLocalizations.of(context).translate('email')
                    : share.invitedUser!,
                tap: () {},
              ),
            ),
            SizedBox(
              height: font20,
            ),
            Row(
              children: [
                Expanded(
                  child: Material(
                    elevation: font4,
                    shadowColor: colorBlack,
                    borderRadius: BorderRadius.circular(font5),
                    child: Container(
                      height: font280,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(font5),
                          color: colorTextfieldBackround),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: font13, horizontal: font10),
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context).translate('payment'),
                              style: styleTitle2,
                            ),
                            SizedBox(
                              height: font20,
                            ),
                            share.sharedCards == null
                                ? Container(
                                    height: font123,
                                    alignment: Alignment.center,
                                    // color: colorBlack,
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate('choose_payments'),
                                      style: styleTitle2,
                                    ),
                                  )
                                : Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: font10),
                                    // color: colorBlack,
                                    height: font123,
                                    child: ListView.builder(
                                        itemCount: share.sharedCards!.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          // return _invUserTile(share[index]);
                                          return _cardTile(
                                              share.sharedCards![index]);
                                        }),
                                  ),
                            SizedBox(
                              height: font12,
                            ),
                            WidgetDivider(
                              height: font2,
                              child: InkWell(
                                onTap: () {
                                  submitAddPayments(share.sharedCards);
                                },
                                child: Image.asset(
                                  "assets/images/add_new.png",
                                  width: font50,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                ButtonText(
                                  text: AppLocalizations.of(context)
                                      .translate('clear'),
                                  onTap: () {
                                    submitClearPayments();
                                  },
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: font23,
                ),
                Expanded(
                  child: Material(
                    elevation: font4,
                    shadowColor: colorBlack,
                    borderRadius: BorderRadius.circular(font5),
                    child: Container(
                      height: font280,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(font5),
                          color: colorTextfieldBackround),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: font13, horizontal: font10),
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context).translate('cars'),
                              style: styleTitle2,
                            ),
                            SizedBox(
                              height: font20,
                            ),
                            share.sharedCars == null
                                ? Container(
                                    height: font123,
                                    alignment: Alignment.center,
                                    // color: colorBlack,
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate('choose_cars'),
                                      style: styleTitle2,
                                    ),
                                  )
                                : Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: font10),
                                    // color: colorBlack,
                                    height: font123,
                                    child: ListView.builder(
                                        itemCount: share.sharedCars!.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          // return _invUserTile(share[index]);
                                          return _carTile(
                                              share.sharedCars![index]);
                                        }),
                                  ),
                            SizedBox(
                              height: font12,
                            ),
                            WidgetDivider(
                              height: font2,
                              child: InkWell(
                                onTap: () {
                                  submitAddCars(share.sharedCars);
                                },
                                child: Image.asset(
                                  "assets/images/add_new.png",
                                  width: font50,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                ButtonText(
                                  text: AppLocalizations.of(context)
                                      .translate('clear'),
                                  onTap: () {
                                    submitClearCars();
                                  },
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: font40,
            ),
            RegularButton(
              context: AppLocalizations.of(context).translate('share'),
              onTap: () {},
              isGreen: true,
            )
          ],
        ),
      ),
    );
  }

  Widget _cardTile(CardClass card) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: font8),
        child: SizedBox(
          height: font25,
          child: DialogTile(
            action: Container(),
            isTrash: false,
            title: "... ${card.lastFourNumber}",
            icon: Padding(
              padding: EdgeInsets.only(bottom: font2),
              // child: card.paymentSytem == "visa"
              //     ? SvgPicture.asset(
              //         "assets/icons/visa.svg",
              //       )
              //     : SvgPicture.asset(
              //         "assets/icons/master_card.svg",
              //       ),
            ),
            // isShared: car.isShared!,
            onTapRemove: () {},
          ),
        ));
  }

  Widget _carTile(AutoClass car) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: font8),
        child: SizedBox(
          height: font25,
          child: DialogTile(
            action: Container(),
            isTrash: false,
            title: "${car.number}",
            icon: Padding(
                padding: EdgeInsets.only(bottom: font2),
                child: SvgPicture.asset(
                  "assets/icons/car.svg",
                )),
            // isShared: car.isShared!,
            onTapRemove: () {},
          ),
        ));
  }
}
