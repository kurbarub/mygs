import 'package:MyGS/classes/card_class.dart';
import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/textfields/access_textfield.dart';
import '../../components/tiles/dialog_tile.dart';
import '../../components/tiles/gas_station_tile.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';
import 'package:intl/intl.dart';

class PaymentsPage extends StatefulWidget {
  const PaymentsPage({super.key});

  @override
  State<PaymentsPage> createState() => _PaymentsPageState();
}

class _PaymentsPageState extends State<PaymentsPage> {
  final DataCollector dc = DataCollector();

  
  TextEditingController _controllerNumber = TextEditingController();
  TextEditingController _controllerDate = TextEditingController();
  TextEditingController _controllerCVV = TextEditingController();

  void _modalBottomSheetMenu() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: _sheet(),
            );
          });
    });
  }

  @override
  void initState() {
    super.initState();
    dc.getCards();
  }

  void removeCard(int id) async{
    await dc.deleteCard(id);
    await dc.getCards();
  }

  void addNewCard() async{

    String s = _controllerDate.text;

    int idx = s.indexOf("/");
    DateTime exp = DateTime.parse("${s.substring(idx+1).trim()}-${s.substring(0,idx).trim()}-01 00:00:00.000");

    // List parts = [s.substring(0,idx).trim(), s.substring(idx+1).trim()];

    NewCard response = await dc.createCard(_controllerNumber.text.trim(), exp, int.parse(_controllerCVV.text));

    switch (response) {
      case NewCard.wrongnumber: return sendMessage("CRD002", fromServer: true);
      case NewCard.expire: return sendMessage("CRD004", fromServer: true);
      case NewCard.wrongcvv: return sendMessage("CRD005", fromServer: true);
      case NewCard.faild: return sendMessage("CRD007", fromServer: true);
      case NewCard.success: {
        sendMessage("card_success", fromServer: true, isError: false);
          dc.getCards();
        }
    }
    
  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : title) : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
        padding: EdgeInsets.all(font30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context).translate('paymen_method'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font28,
            ),
            StreamBuilder<List<CardClass>>(
                stream: dc.obsCards,
                builder: (context, snapshot) {
                  List<CardClass>? cards = snapshot.data;
                  if (cards == null || cards.isEmpty) {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: font400,
                      ),
                      child: Center(
                          child: Text(
                        AppLocalizations.of(context)
                            .translate('not_added_card_yet'),
                        style: styleInscriptions,
                      )),
                    );
                  } else {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: font400,
                      ),
                      child: ListView.builder(
                          itemCount: cards.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _cardTile(cards[index]);
                          }),
                    );
                  }
                }),
            RegularButton(
                context: AppLocalizations.of(context).translate('add_new_card'),
                isGreen: true,
                onTap: () {
                  _modalBottomSheetMenu();
                })
          ],
        ));
  }

  Widget _cardTile(CardClass card) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: font8),
        child: DialogTile(
          action: Container(),
          title: "... ${card.lastFourNumber}",
          icon: card.bin == 4
              ? SvgPicture.asset(
                  "assets/icons/visa.svg",
                )
              : card.bin == 5
                  ? SvgPicture.asset(
                      "assets/icons/master_card.svg",
                    )
                  : Text(''),
          onTapRemove: () {
            removeCard(card.id!);
          },
        ));
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      title: Text(
        AppLocalizations.of(context).translate('payments'),
        style: styleTitleContent,
      ),
      centerTitle: true,
    );
  }

  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );

  Widget _sheet() {
    return makeDismissible(
      child: Container(
        decoration: BoxDecoration(
            color: colorWhiteText,
            borderRadius: BorderRadius.vertical(top: Radius.circular(font25))),
        padding: EdgeInsets.symmetric(horizontal: font30, vertical: font49),
        child: Column(
          // controller: controller,
          // clipBehavior: Clip.none,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context).translate('add_new_card'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font20,
            ),
            SizedBox(
              height: font46,
              child: AuthorizationTextField(
                controller: _controllerNumber,
                text: AppLocalizations.of(context).translate('card_number'),
                tap: () {},
              ),
            ),
            SizedBox(
              height: font16,
            ),
            Row(
              children: [
                SizedBox(
                  width: font115,
                  height: font46,
                  child: SizedBox(
                    child: AuthorizationTextField(
                      controller: _controllerDate,
                      text: 'mm/yyyy',
                      tap: () {},
                      isDateMask: true,
                    ),
                  ),
                ),
                SizedBox(
                  width: font18,
                ),
                SizedBox(
                  width: font71,
                  height: font46,
                  child: AuthorizationTextField(
                    controller: _controllerCVV,
                    text: 'Cvv',
                    tap: () {},
                    isCvvMask: true,
                    // isMask: true,
                  ),
                ),
                
              ],
            ),
            SizedBox(
              height: font32,
            ),
            RegularButton(context: AppLocalizations.of(context).translate('add'), onTap: (){addNewCard(); Navigator.pop(context);}, isGreen: true,)
          ],
        ),
      ),
    );
  }
}


