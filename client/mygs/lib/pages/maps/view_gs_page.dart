import 'package:MyGS/pages/maps/filter_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../classes/gas_station_class.dart';
import '../../components/modals/dialog_modal.dart';
import '../../components/modals/gas_station_bottom_sheet.dart';
import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/tiles/gas_station_tile.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class ViewGSPage extends StatefulWidget {
  const ViewGSPage({super.key});

  @override
  State<ViewGSPage> createState() => _ViewGSPageState();
}

class _ViewGSPageState extends State<ViewGSPage> {
final DataCollector dc = DataCollector();

  @override
  void initState() {
    super.initState();
    // dc.getGS();
  }


  void submitFilter(){
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => FilterPage()));
  }

  Future modalRemoveFromFavorite(int gsid) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return DialogModal(
              action: () async{
                await dc.changeGSLike(gsid);
                infoFlashbar(
                  context: context,
                  title: AppLocalizations.of(context).translate('rem_to_fav'),
                  message: AppLocalizations.of(context).translate('rem_to_fav_disc'),
                  isInfo: true);
              },
              actionTitle: "Remove",
              title: AppLocalizations.of(context)
                  .translate('remove_from_favorites'),
              discription:
                  AppLocalizations.of(context).translate('remove_fav_comment'),
              cont: context);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context).translate('gas_stations'),
            style: styleTitleContent,
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: font10),
              child: Center(
                child: IconButton(onPressed: (){submitFilter();}, 
                icon: SvgPicture.asset(
                      'assets/icons/filter.svg',
                      width: font23,
                    ),
                ),
              ),
            )
          ],
        ),
        body: Padding(
            padding: EdgeInsets.only(top: font22),
            child: StreamBuilder<List<GasStation>>(
                stream: dc.obsGS,
                builder: (context, snapshot) {
                  List<GasStation>? gs = snapshot.data;
                  if (gs == null || gs.isEmpty) {
                    return Container();
                  } else {
                    return ListView.builder(
                        itemCount: gs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return _gsTile(gs[index]);
                        });
                  }
                })));
  }

  Widget _gsTile(GasStation gs) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: font8, horizontal: font30),
      child: GasStationTile(
        onTap: () async{
          GasStation g = await dc.getGSFullData(gs.id!);

          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => GasStationSheet(
                    gs: g,
        
          )));
        },
        gs: gs
      ),
    );
  }
}