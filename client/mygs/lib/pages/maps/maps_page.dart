import 'package:MyGS/classes/gas_station_class.dart';
import 'package:MyGS/pages/maps/search_page.dart';
import 'package:MyGS/pages/maps/view_gs_page.dart';
import 'package:MyGS/utility/variables/cities.dart';
import 'package:MyGS/utility/variables/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../components/buttons/regular_button.dart';
import '../../components/modals/dialog_modal.dart';
import '../../components/modals/gas_station_bottom_sheet.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../access/no_access_page.dart';
import 'favorite_page.dart';
import 'filter_page.dart';

class MapsPage extends StatefulWidget {
  const MapsPage({super.key});

  @override
  State<MapsPage> createState() => _MapsPageState();
}

class _MapsPageState extends State<MapsPage> {
  DataCollector dc = DataCollector();

  var coordinate;
  final Map<String, Marker> _markers = {};

  Future<void> _onMapCreated(GoogleMapController controller) async {
    final gasStations = dc.gasStatons;
    setState(() {
      _markers.clear();
      for (final gs in gasStations) {
        final marker = Marker(
          onTap: () {
            print(gs.firmName);
          },
          markerId: MarkerId(gs.id.toString()),
          position: LatLng(gs.latitude!, gs.longtitude!),
          infoWindow: InfoWindow(
              title: gs.firmName,
              snippet:
                  "${gs.adress}",
              onTap: () async {
                GasStation g = await dc.getGSFullData(gs.id!);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => GasStationSheet(
                          gs: g,
               
                        )));
              }),
        );
        _markers[gs.id.toString()] = marker;
      }
    });
  }

  @override
  void initState() {
    coordinate = coordinates[dc.city!];
    super.initState();
  }

  void submitSearch() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const SearchPage()));
  }

  void submitFilter() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const ViewGSPage()));
  }

  void submitFavorite() {
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
    else
      Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const FavoriteGSPage()));
  }

  void submitNearestGS() async{

    GasStation gs = await dc.getGSFullData(dc.nearestGS.id!);
    // dc.nearestGS
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => GasStationSheet(
              gs: gs,
            )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(0),
          child: AppBar(
            // Here we create one to set status bar color
            backgroundColor: Colors
                .black, // Set any color of status bar you want; or it defaults to your theme's primary color
          )),

      // backgroundColor: Colors.red,
      body: _body(),
    );
  }

  Widget _body() {
    return (Stack(
      children: [
        _map(),
        _topMenu(),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: EdgeInsets.only(bottom: font61),
            child: SizedBox(
              width: font140,
              height: font38,
              child: RegularButton(
                isGreen: true,
                context: AppLocalizations.of(context).translate('nearest_GS'),
                onTap: () {
                  submitNearestGS();
                },
              ),
            ),
          ),
        )
      ],
    ));
  }

  Widget _map() {
    late GoogleMapController mapController;

    final LatLng _center = LatLng(coordinate[0], coordinate[1]);

    return GoogleMap(
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 10.0,
      ),
      markers: _markers.values.toSet(),
    );
  }

  Widget _topMenu() {
    return Padding(
      padding: EdgeInsets.only(top: font60),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          InkWell(
            onTap: () {
              submitSearch();
            },
            child: Container(
              alignment: Alignment.center,
              height: font38,
              width: font55,
              decoration: BoxDecoration(
                  color: colorBlack.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(font5)),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: font16, vertical: font8),
                child: SvgPicture.asset(
                  'assets/icons/search.svg',
                  width: font23,
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              submitFilter();
            },
            child: Container(
              alignment: Alignment.center,
              height: font38,
              width: font55,
              decoration: BoxDecoration(
                  color: colorBlack.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(font5)),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: font16, vertical: font8),
                child: SvgPicture.asset(
                  'assets/icons/filter.svg',
                  width: font23,
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              submitFavorite();
            },
            child: Container(
              alignment: Alignment.center,
              height: font38,
              width: font55,
              decoration: BoxDecoration(
                  color: colorTranspRed,
                  borderRadius: BorderRadius.circular(font5)),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(horizontal: font16, vertical: font8),
                child: SvgPicture.asset(
                  'assets/icons/unfilled_heart.svg',
                  width: font23,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
