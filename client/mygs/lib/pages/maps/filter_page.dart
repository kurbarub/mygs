import 'package:MyGS/components/buttons/text_button.dart';
import 'package:MyGS/components/tiles/filter_drop_down_tile.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';

class FilterPage extends StatefulWidget {
  const FilterPage({super.key});

  @override
  State<FilterPage> createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {

  DataCollector dc = DataCollector();
  bool isSelected = false;

  List<int> fuelTypes =  [];
  List<int> payMethods = [];
  List<int> services = [];

    @override
  void initState() {
    
    fuelTypes = dc.fuelTypes;
    payMethods = dc.payMethods;
    services = dc.services;
    
    super.initState();
  }

  void submitAply() async{
    await dc.getGSFiltered(fuelTypes,payMethods,services);
    Navigator.pop(context);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context).translate('filter'),
          style: styleTitleContent,
        ),
        actions: [
          Center(
            child: Padding(
              padding: EdgeInsets.only(right: font30),
              child: ButtonText(
                text: AppLocalizations.of(context).translate('aply'),
                onTap: (){submitAply();}),
            ),
          )
        ],
      
      ),
      body: Padding(
        padding: EdgeInsets.only(top: font30),
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: font30),
      child: SingleChildScrollView(
        child: Column(
          children: [
            FilterDropDownTile(
                title: AppLocalizations.of(context).translate('fuel_type'),
                content: _fuelType()),
            SizedBox(height: font16,),
            FilterDropDownTile(
                title: AppLocalizations.of(context).translate('paymen_method'),
                content: _paymentMethod()),
            SizedBox(height: font16,),
            FilterDropDownTile(
                title: AppLocalizations.of(context).translate('services'),
                content: _services()),
            // SizedBox(height: font16,),
            // FilterDropDownTile(
            //     title: AppLocalizations.of(context).translate('offers'),
            //     content: _offers()),
          ],
        ),
      ),
    );
  }

  Widget _fuelType() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: font6),
        child: Wrap(
          children: [
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: () {
                  setState(() {
                    if(fuelTypes.contains(0)) fuelTypes.remove(0);
                    else fuelTypes.add(0);
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  height: font42,
                  width: font66,
                  decoration: BoxDecoration(
                      color: fuelTypes.contains(0) ? colorLightGreen : colorLightWhite,
                      borderRadius: BorderRadius.circular(font5)),
                  child: Text(
                    "Disel",
                    textAlign: TextAlign.center,
                    style: styleTitleContent.copyWith(fontSize: font14),
                    softWrap: true,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(fuelTypes.contains(1)) fuelTypes.remove(1);
                    else fuelTypes.add(1);
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  height: font42,
                  width: font66,
                  decoration: BoxDecoration(
                      color: fuelTypes.contains(1) ? colorLightGreen : colorLightWhite,
                      borderRadius: BorderRadius.circular(font5)),
                  child: Text(
                    "Petrol",
                    textAlign: TextAlign.center,
                    style: styleTitleContent.copyWith(fontSize: font14),
                    softWrap: true,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(fuelTypes.contains(2)) fuelTypes.remove(2);
                    else fuelTypes.add(2);
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  height: font42,
                  width: font66,
                  decoration: BoxDecoration(
                      color: fuelTypes.contains(2) ? colorLightGreen : colorLightWhite,
                      borderRadius: BorderRadius.circular(font5)),
                  child: Text(
                    "LPG",
                    textAlign: TextAlign.center,
                    style: styleTitleContent.copyWith(fontSize: font14),
                    softWrap: true,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(fuelTypes.contains(3)) fuelTypes.remove(3);
                    else fuelTypes.add(3);
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  height: font42,
                  width: font66,
                  decoration: BoxDecoration(
                      color: fuelTypes.contains(3) ? colorLightGreen : colorLightWhite,
                      borderRadius: BorderRadius.circular(font5)),
                  child: Text(
                    "CNG",
                    textAlign: TextAlign.center,
                    style: styleTitleContent.copyWith(fontSize: font14),
                    softWrap: true,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _paymentMethod() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: font6),
        child: Wrap(
          children: [

            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(payMethods.contains(0)) payMethods.remove(0);
                    else payMethods.add(0);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
              
                    decoration: BoxDecoration(
                        color: payMethods.contains(0) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "Cashless",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
           
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(payMethods.contains(1)) payMethods.remove(1);
                    else payMethods.add(1);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: payMethods.contains(1) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "Cash",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(payMethods.contains(2)) payMethods.remove(2);
                    else payMethods.add(2);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: payMethods.contains(2) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "Contactless",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            
          ],
        ),
      ),
    );
  }

    Widget _services() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: font6),
        child: Wrap(
          children: [

            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(services.contains(0)) services.remove(0);
                    else services.add(0);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: services.contains(0) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "WIFI",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
           
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(services.contains(1)) services.remove(1);
                    else services.add(1);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: services.contains(1) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "Carwashing",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(services.contains(2)) services.remove(2);
                    else services.add(2);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: services.contains(2) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "Vacuum",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: font12),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(services.contains(3)) services.remove(3);
                    else services.add(3);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: services.contains(3) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "ATM",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(services.contains(4)) services.remove(4);
                    else services.add(4);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: services.contains(4) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "Market",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(services.contains(5)) services.remove(5);
                    else services.add(5);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: services.contains(5) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "Handicapped",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(services.contains(6)) services.remove(6);
                    else services.add(6);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: services.contains(6) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "Parking",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: font12, right: font16),
              child: InkWell(
                onTap: (){
                  setState(() {
                    if(services.contains(7)) services.remove(7);
                    else services.add(7);
                  });
                },
                child: ConstrainedBox(
                  constraints: BoxConstraints(),
                  child: DecoratedBox(
                    // alignment: Alignment.center,
                    // height: font22,
                    decoration: BoxDecoration(
                        color: services.contains(7) ? colorLightGreen : colorLightWhite,
                        borderRadius: BorderRadius.circular(font5)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                      child: Text(
                        "WC",
                        textAlign: TextAlign.center,
                        style: styleTitleContent.copyWith(fontSize: font14),
                        softWrap: true,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            
          ],
        ),
      ),
    );
  }

      Widget _offers() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: font13),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: EdgeInsets.only(bottom: font24),
            child: Row(
              children: [
                Column(
               
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: font190,
                      height: font95,
                      color: colorModalWindow,
                    ),
                    SizedBox(height: font8,),
                    Text("Title", style: styleTitle2,)
                  ],
                ),
                SizedBox(width: font12,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: font190,
                      height: font95,
                      color: colorModalWindow,
                    ),
                    SizedBox(height: font8,),
                    Text("Title", style: styleTitle2,)
                  ],
                ),
                SizedBox(width: font12,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: font190,
                      height: font95,
                      color: colorModalWindow,
                    ),
                    SizedBox(height: font8,),
                    Text("Title", style: styleTitle2,)
                  ],
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}
