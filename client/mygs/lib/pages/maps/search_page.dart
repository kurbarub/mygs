import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/components/modals/dialog_modal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../classes/gas_station_class.dart';
import '../../components/buttons/text_button.dart';
import '../../components/modals/gas_station_bottom_sheet.dart';
import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/tiles/gas_station_tile.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';

import '../../utility/variables/text_styles.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final DataCollector dc = DataCollector();


  

  @override
  void initState() {
    super.initState();
    // dc.getGS();
  }

  TextEditingController tc = new TextEditingController();

  Future<void> text()async{
    await dc.getGSByRequest(tc.text);
  }

  Future modalRemoveFromFavorite(int gsid) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return DialogModal(
              action: () async{
                await dc.changeGSLike(gsid);
                infoFlashbar(
                  context: context,
                  title: AppLocalizations.of(context).translate('rem_to_fav'),
                  message: AppLocalizations.of(context).translate('rem_to_fav_disc'),
                  isInfo: true);
              },
              actionTitle: "Remove",
              title: AppLocalizations.of(context)
                  .translate('remove_from_favorites'),
              discription:
                  AppLocalizations.of(context).translate('remove_fav_comment'),
              cont: context);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
                  actions: [
              Center(
                child: Padding(
                  padding: EdgeInsets.only(right: font30),
                  child: ButtonText(
                    text: AppLocalizations.of(context).translate('aply'),
                    onTap: (){text();}),
                ),
              )
            ],
            title: Container(
          width: font260,
          height: font40,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: colorTextfieldBackround,
              borderRadius: BorderRadius.circular(font5)),
          child: Center(
            child: TextField(
              cursorColor: colorBlack,
              style: styleTitle3,
              cursorWidth: font1,
              controller: tc,
              decoration: InputDecoration(
                  isDense: true,
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(font9),
                    child: SvgPicture.asset(
                      'assets/icons/search_tf.svg',
                    ),
                  ),
                  prefixIconConstraints: BoxConstraints(
                    minWidth: font15,
                    minHeight: font15,
                  ),
                  suffixIconConstraints: BoxConstraints(
                    minWidth: font15,
                    minHeight: font15,
                  ),
                  suffixIcon: IconButton(
                    color: colorBlack,
                    icon: const Icon(Icons.clear),
                    onPressed: () {
                      tc.clear();
                    },
                  ),
                  hintText: 'Search...',
                  border: InputBorder.none),
            ),
          ),
        )
        
        ),
        body: Padding(
            padding: EdgeInsets.only(top: font22),
            child: StreamBuilder<List<GasStation>>(
                stream: dc.obsGS,
                builder: (context, snapshot) {
                  List<GasStation>? gs = snapshot.data;
                  if (gs == null || gs.isEmpty) {
                    return Container();
                  } else {
                    return ListView.builder(
                        itemCount: gs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return _gsTile(gs[index]);
                        });
                  }
                })));
  }

  Widget _gsTile(GasStation gs) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: font8, horizontal: font30),
      child: GasStationTile(
        onTap: () async{
          
          GasStation g = await dc.getGSFullData(gs.id!);

          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => GasStationSheet(
                    gs: g
                    
          )));
        },
        gs: gs,
      ),
    );
  }
}
