import 'package:MyGS/classes/gas_station_class.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../components/modals/dialog_modal.dart';
import '../../components/modals/gas_station_bottom_sheet.dart';
import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/tiles/gas_station_tile.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class FavoriteGSPage extends StatefulWidget {
  const FavoriteGSPage({super.key});

  @override
  State<FavoriteGSPage> createState() => _FavoriteGSPageState();
}

class _FavoriteGSPageState extends State<FavoriteGSPage> {
  final DataCollector dc = DataCollector();

  @override
  void initState() {
    super.initState();
    // dc.getGS();
  }

  void submitGS(GasStation gs) async{
    GasStation g = await dc.getGSFullData(gs.id!);
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => GasStationSheet(
              gs: g,
            )));
  }

  // Future modalRemoveFromFavorite(int gsid) {
  //   return showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return DialogModal(
  //             action: () async{
  //               await dc.changeGSLike(gsid);
  //               infoFlashbar(
  //                 context: context,
  //                 title: AppLocalizations.of(context).translate('rem_to_fav'),
  //                 message: AppLocalizations.of(context).translate('rem_to_fav_disc'),
  //                 isInfo: true);
  //             },
  //             actionTitle: "Remove",
  //             title: AppLocalizations.of(context)
  //                 .translate('remove_from_favorites'),
  //             discription:
  //                 AppLocalizations.of(context).translate('remove_fav_comment'),
  //             cont: context);
  //       });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context).translate('favorite_gas_stations'),
            style: styleTitleContent,
          ),
        ),
        body: Padding(
            padding: EdgeInsets.only(top: font22),
            child: StreamBuilder<List<GasStation>>(
                stream: dc.obsFavirites,
                builder: (context, snapshot) {
                  List<GasStation>? gs = snapshot.data;
                  if (gs == null || gs.isEmpty) {
                    return Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: font30),
                          child: Text(
                            
                                              AppLocalizations.of(context).translate('no_gs'),
                                              style: styleInscriptions,
                                              textAlign: TextAlign.center,
                                            ),
                        ));
                  } else {
                    return ListView.builder(
                        itemCount: gs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return _gsTile(gs[index]);
                        });
                  }
                })));
  }

  Widget _gsTile(GasStation gs) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: font8, horizontal: font30),
      child: GasStationTile(
        onTap: () {
          submitGS(gs);
        },
        gs: gs,
      ),
    );
  }
}
