import 'package:MyGS/classes/feed_back_class.dart';
import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/components/textfields/feedback_textarea.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../components/modals/info_bottom_flushbar.dart';
import '../../mixins/star_rating.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/text_styles.dart';

class FeedBackPage extends StatefulWidget {
  const FeedBackPage({super.key, required this.fb, required this.itemId, this.isGs = true,});

  
  final FeedBack fb;
  final int itemId;
  final bool isGs;
  @override
  State<FeedBackPage> createState() => _FeedBackPageState();
}

class _FeedBackPageState extends State<FeedBackPage> {

  DataCollector dc = DataCollector();
  
  TextEditingController tc = TextEditingController();

  void submitSend() {

    if(widget.itemId!=0){
      FeedBack f = FeedBack(
        content: tc.text.trim(),
        title: widget.isGs ? "GS id = {${widget.itemId}} FB": "Order id = {${widget.itemId}} FB",
        rating: widget.fb.rating
      );

      dc.setFeedBackGS(f, widget.itemId);

      infoFlashbar(
          context: context,
          title: AppLocalizations.of(context).translate('new_fb'),
          message: "",
          isInfo: true);
      
    }

    Future.delayed(const Duration(seconds: 3), () {
      Navigator.pop(context);
    });
    
  }

  @override
  void initState() {
    
    tc.text = widget.fb.content!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('feedback'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.only(top: font60, left: font30, right: font30),
      child: SingleChildScrollView(
        child: Column(
          children: [
            FeedBackTextArea(
              controller: tc,
              hint: AppLocalizations.of(context).translate('write'),
            ),
            SizedBox(
              height: font20,
            ),
            StarRating(
              rating: widget.fb.rating!,
              onRatingChanged: (rating) => setState(() => widget.fb.rating = rating),
              color: Colors.black,
            ),
            SizedBox(
              height: font20,
            ),
            RegularButton(
              context: AppLocalizations.of(context).translate('send'),
              onTap: () {
                submitSend();
              },
              isGreen: true,
            )
          ],
        ),
      ),
    );
  }
}
