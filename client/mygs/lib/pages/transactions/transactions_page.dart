import 'package:MyGS/classes/order_class.dart';
import 'package:MyGS/pages/transactions/filter_drawer.dart';
import 'package:MyGS/pages/transactions/full_order_info_page.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';

class TransactionsPage extends StatefulWidget {
  const TransactionsPage({super.key});

  @override
  State<TransactionsPage> createState() => _TransactionsPageState();
}

class _TransactionsPageState extends State<TransactionsPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();



  void moreInfo(OrderClass order) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => FullOrderInfoPge(order: order,)));
  }

  final DataCollector dc = DataCollector();

  @override
  void initState() {
    super.initState();
    dc.getOrders();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
      endDrawer: const FilterDrawer(),
      key: _scaffoldKey,
      resizeToAvoidBottomInset : false,
      );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _appBar(),
          SizedBox(
            height: font50,
          ),
          StreamBuilder<List<OrderClass>>(
              stream: dc.obsReciepts,
              builder: (context, snapshot) {
                List<OrderClass>? reciepts = snapshot.data;
                if (reciepts == null || reciepts.isEmpty) {
                  return ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: font400,
                    ),
                    child: Center(
                        child: Text(
                      AppLocalizations.of(context).translate('no_transactions'),
                      style: styleInscriptions,
                    )),
                  );
                } else {
                  return ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: font550,
                    ),
                    child: ListView.builder(
                        itemCount: reciepts.length,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            child: resieptTile(reciepts[index]),
                            onTap: () {
                              moreInfo(reciepts[index]);
                            },
                          );
                        }),
                  );
                }
              }),
        ],
      ),
    );
  }

  Widget resieptTile(OrderClass order) {
    return Padding(
      padding: EdgeInsets.only(bottom: font16),
      child: Container(
        height: font85,
        width: double.infinity,
        decoration: BoxDecoration(
            color: colorTextfieldBackround,
            borderRadius: BorderRadius.circular(font5)),
        child: Padding(
          padding: EdgeInsets.all(font12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Material(
                        elevation: font4,
                        shadowColor: colorBlack,
                        borderRadius: BorderRadius.circular(font5),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: colorLightWhite,
                              borderRadius: BorderRadius.circular(font5)),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: font8, vertical: font6),
                            child: Text(
                              order.refueledCarNumber!,
                              style: styleTitle2,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: font14,
                      ),
                      Text(
                        DateFormat("dd.MM.yyyy")
                            .format(order.created!)
                            .toString(),
                        style: styleTitle3,
                      )
                    ],
                  ),
                  // SizedBox(height: font8,),
                  Text(
                    order.gasStationAddress!,
                    style: styleTitle3,
                  ),
                ],
              ),
              Text(
                "${order.totalAmount.toString()} Kc",
                style:
                    styleFuelStandNumber.copyWith(fontWeight: FontWeight.w600),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _appBar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text(''),
        Text(
          AppLocalizations.of(context).translate('transactions'),
          style: styleTitleContent,
        ),
        InkWell(
          onTap: () {
            _scaffoldKey.currentState!.openEndDrawer();
          },
          child: SvgPicture.asset(
            "assets/icons/filter.svg",
            height: font23,
          ),
        )
      ],
    );
  }
}
