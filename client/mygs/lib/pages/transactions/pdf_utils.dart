import 'dart:io';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

import '../../classes/order_class.dart';


class PdfUtil {

  final pdf = pw.Document();

  writeOnPdf(OrderClass order) {
    pdf.addPage(pw.MultiPage(
      pageFormat: PdfPageFormat.a4,
      margin: pw.EdgeInsets.all(32),
      build: (pw.Context context) {
        return <pw.Widget>[
          pw.Header(
              level: 0,
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  children: <pw.Widget>[
                    pw.Text('My GasStation', textScaleFactor: 2),
                  ])),
          pw.Header(level: 1, text: 'Order# ${order.id}'),

          pw.Paragraph(
              text:
                  'Order# ${order.id}'),
          pw.Paragraph(
              text:
                  'Created: ${order.created}'),
          pw.Paragraph(
              text:
                  'Gas Station: ${order.gasStationName}; ${order.gasStationAddress}'),
          pw.Paragraph(
              text:
                  'Litters count: ${order.countLitters}'),
          pw.Paragraph(
              text:
                  'Total Amount: ${order.totalAmount}'),
           pw.Paragraph(
              text:
                  'Fuel name: ${order.fuelName}'),
          pw.Paragraph(
              text:
                  'Refueild car: ${order.refueledCarNumber}'), 
          
        ];
      },
    ));
  }

  
Future savePdf() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    String documentPath = documentDirectory.path;

    String id = DateTime.now().toString();

    File file = File("$documentPath/$id.pdf");

    file.writeAsBytesSync(await pdf.save());

    final sharePdf = await file.readAsBytes();

    await Share.file(
      'PDF Document',
      'reciept.pdf',
      sharePdf.buffer.asUint8List(),
      '*/*',
    );
    
  }
}
