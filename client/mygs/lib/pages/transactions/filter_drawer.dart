import 'package:MyGS/classes/card_class.dart';
import 'package:MyGS/components/dividers/regular_divider.dart';
import 'package:MyGS/components/tiles/custom_selector.dart';
import 'package:MyGS/pages/menu/payments_page.dart';
import 'package:MyGS/pages/menu/settings/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

import '../../classes/auto_class.dart';
import '../../components/buttons/text_button.dart';
import '../../components/textfields/access_textfield.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class FilterDrawer extends StatefulWidget {
  const FilterDrawer({Key? key}) : super(key: key);

  @override
  State<FilterDrawer> createState() => _FilterDrawerState();
}

class _FilterDrawerState extends State<FilterDrawer> {
  final DataCollector dc = DataCollector();

  final TextEditingController _fromPriceController = TextEditingController();
  final TextEditingController _toPriceController = TextEditingController();

  DateTime selectedDate = DateTime.now();
  String selectedGender = '';
  String selectedDateView = '';

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1940),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        selectedDateView = DateFormat("dd.MM.yyyy").format(picked).toString();
      });
    }
  }

  void closeDrawer() {
    Navigator.pop(context);
  }

  void submitReset() {}
  void submitAply() {}

  @override
  void initState() {
    super.initState();
    dc.getCars();
    dc.getCards();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: font270,
      backgroundColor: colorMenuPanel,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: font30),
        child: ListView(
          children: [
            Padding(
              padding: EdgeInsets.only(top: font70, bottom: font75),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtonText(
                          text: AppLocalizations.of(context).translate('apply'),
                          isGreen: true,
                          onTap: () {
                            submitAply();
                          }),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ButtonText(
                          text: AppLocalizations.of(context).translate('reset'),
                          onTap: () {
                            submitReset();
                          }),
                      SizedBox(
                        width: font16,
                      ),
                      InkWell(
                        onTap: () {
                          closeDrawer();
                        },
                        child: SvgPicture.asset(
                          'assets/icons/cross.svg',
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            RegularDivider(height: font2),
            SizedBox(
              height: font32,
            ),
            Text(
              AppLocalizations.of(context).translate('date'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font12,
            ),
            Material(
              shadowColor: colorBlack,
              elevation: font4,
              borderRadius: BorderRadius.circular(font5),
              child: InkWell(
                onTap: () {
                  _selectDate(context);
                },
                child: Container(
                  height: font50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(font5),
                    color: colorTextfieldBackround,
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: font20, vertical: font12),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          selectedDateView == ''
                              ? 'dd.mm.yyyy'
                              : selectedDateView.toString(),
                          style: styleInscriptions,
                        ),
                        SvgPicture.asset('assets/icons/bottom_arrow.svg'),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: font20,
            ),
            Text(
              AppLocalizations.of(context).translate('price'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font12,
            ),
            Row(
              children: [
                SizedBox(
                  width: font85,
                  child: AuthorizationTextField(
                    controller: _fromPriceController,
                    text: AppLocalizations.of(context).translate('from'),
                    tap: () {},
                  ),
                ),
                SizedBox(
                  width: font20,
                ),
                SizedBox(
                  width: font85,
                  child: AuthorizationTextField(
                    controller: _toPriceController,
                    text: AppLocalizations.of(context).translate('to'),
                    tap: () {},
                  ),
                ),
              ],
            ),
            SizedBox(
              height: font20,
            ),
            Text(
              AppLocalizations.of(context).translate('car'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font12,
            ),
            StreamBuilder<List<AutoClass>>(
                stream: dc.obsCars,
                builder: (context, snapshot) {
                  List<AutoClass>? cars = snapshot.data;
                  if (cars == null || cars.isEmpty) {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: font80,
                      ),
                      child: Text(
                        AppLocalizations.of(context)
                            .translate('not_added_car_yet'),
                        style: styleInscriptions,
                      ),
                    );
                  } else {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: font80,
                      ),
                      child: GridView.builder(
                          gridDelegate:
                              new SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2, childAspectRatio: 2.5),
                          itemCount: cars.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _tile(cars[index].number!);
                          }),
                    );
                  }
                }),
            SizedBox(
              height: font20,
            ),
            Text(
              AppLocalizations.of(context).translate('paymen_method'),
              style: styleTitle2,
            ),
            SizedBox(
              height: font12,
            ),
            StreamBuilder<List<CardClass>>(
                stream: dc.obsCards,
                builder: (context, snapshot) {
                  List<CardClass>? cards = snapshot.data;
                  if (cards == null || cards.isEmpty) {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: font80,
                      ),
                      child: Text(
                        AppLocalizations.of(context)
                            .translate('not_added_card_yet'),
                        style: styleInscriptions,
                      ),
                    );
                  } else {
                    return ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: font80,
                      ),
                      child: GridView.builder(
                          gridDelegate:
                              new SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2, childAspectRatio: 2.5),
                          itemCount: cards.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _tile("... ${cards[index].lastFourNumber!}");
                          }),
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }

  Widget _tile(String number) {
    return Padding(
      padding: EdgeInsets.only(right: font6, bottom: font8),
      child: Material(
        elevation: font4,
        shadowColor: colorBlack,
        borderRadius: BorderRadius.circular(font5),
        child: Container(
          alignment: Alignment.center,
          height: font34,
          decoration: BoxDecoration(
              color: colorLightWhite,
              borderRadius: BorderRadius.circular(font5)),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
            child: Text(
              number,
              style: styleTitle2,
              maxLines: 1,
            ),
          ),
        ),
      ),
    );
  }
}
