import 'dart:io';

import 'package:MyGS/classes/order_class.dart';
import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/pages/transactions/pdf_utils.dart';
import 'package:MyGS/pages/transactions/pdf_viewer_page.dart';
import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import '../../classes/feed_back_class.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/text_styles.dart';
import '../maps/feed_back_page.dart';

class FullOrderInfoPge extends StatefulWidget {
  const FullOrderInfoPge({super.key, required this.order});

  final OrderClass order;

  @override
  State<FullOrderInfoPge> createState() => _FullOrderInfoPgeState();
}

class _FullOrderInfoPgeState extends State<FullOrderInfoPge> {

  PdfUtil pu = PdfUtil();

  Future<void> submitDownload() async {
    pu.writeOnPdf(widget.order);
    await pu.savePdf();
  }

  void submitShare() async{
    pu.writeOnPdf(widget.order);
    await pu.savePdf();
  }

  void submitContactSupport() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => FeedBackPage(fb: FeedBack(), itemId:0)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('receipt'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                AppLocalizations.of(context).translate('date'),
                style: styleBold,
              ),
              Text(
                " : ${DateFormat("dd.MM.yyyy").format(widget.order.created!).toString()}",
                style: styleTitleContent,
              )
            ],
          ),
          SizedBox(
            height: font12,
          ),
          Row(
            children: [
              Text(
                AppLocalizations.of(context).translate('time'),
                style: styleBold,
              ),
              Text(
                " : ${DateFormat("hh:mm").format(widget.order.created!).toString()}",
                style: styleTitleContent,
              ),
            ],
          ),
          SizedBox(
            height: font12,
          ),
          Row(
            children: [
              Text(
                AppLocalizations.of(context).translate('car'),
                style: styleBold,
              ),
              Text(
                " : ${widget.order.refueledCarNumber}",
                style: styleTitleContent,
              ),
            ],
          ),
          SizedBox(
            height: font12,
          ),
          Row(
            children: [
              Text(
                AppLocalizations.of(context).translate('total'),
                style: styleBold,
              ),
              Text(
                " : ${widget.order.totalAmount} Kc",
                style: styleTitleContent,
              ),
            ],
          ),
          SizedBox(
            height: font12,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppLocalizations.of(context).translate('gas_station'),
                style: styleBold,
              ),
              Expanded(
                  child: Text(
                " : ${widget.order.gasStationAddress}",
                style: styleTitleContent,
              )),
            ],
          ),
          SizedBox(
            height: font12,
          ),
          Row(
            children: [
              Text(
                AppLocalizations.of(context).translate('fuel'),
                style: styleBold,
              ),
              Text(
                " : ${widget.order.fuelName}",
                style: styleTitleContent,
              ),
            ],
          ),
          SizedBox(
            height: font12,
          ),
          Row(
            children: [
              Text(
                AppLocalizations.of(context).translate('amount'),
                style: styleBold,
              ),
              Text(
                " : ${widget.order.countLitters!.toInt()} ${AppLocalizations.of(context).translate('liters')}",
                style: styleTitleContent,
              ),
            ],
          ),
          SizedBox(
            height: font40,
          ),
          Row(
            children: [
              Expanded(
                child: RegularButton(
                    context: AppLocalizations.of(context).translate('download'),
                    isGreen: true,
                    onTap: () {
                      submitDownload();
                    }),
              ),
              SizedBox(width: font18),
              Expanded(
                child: Material(
                  elevation: font4,
                  shadowColor: colorBlack,
                  borderRadius: BorderRadius.circular(font5),
                  child: RegularButton(
                      context: AppLocalizations.of(context).translate('share'),
                      onTap: () {
                        submitShare();
                      }),
                ),
              )
            ],
          ),
          SizedBox(
            height: font24,
          ),
          Material(
            elevation: font4,
            shadowColor: colorBlack,
            borderRadius: BorderRadius.circular(font5),
            child: RegularButton(
                context:
                    AppLocalizations.of(context).translate('contact_support'),
                onTap: () {
                  submitContactSupport();
                }),
          ),
        ],
      ),
    );
  }
}
