import 'dart:convert';
import 'dart:io';

import 'package:MyGS/classes/ofer_cless.dart';
import 'package:MyGS/pages/access/no_access_page.dart';
import 'package:MyGS/pages/home/ofer_page.dart';
import 'package:MyGS/pages/home/questions_page.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../classes/gas_station_class.dart';
import '../../components/modals/dialog_modal.dart';
import '../../components/modals/gas_station_bottom_sheet.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';
import '../maps/favorite_page.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final DataCollector dc = DataCollector();

    void submitAkce(OferClass ofer) {

      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => OferPage(ofer: ofer,)));
    }

  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  Widget _body() {
    void submitFavorite() {

      if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
        Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
      else
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => const FavoriteGSPage()));
    }


    void submitNearesGS() async{
          GasStation g = await dc.getGSFullData(dc.nearestGS.id!);

          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => GasStationSheet(
                    gs: g,
               
          )));
    }

    void submitQuestions() {

      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const QuestionsPage()));
    }

    void submitSupport() {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
    }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: font30, vertical: font50),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  child: Image.asset("assets/images/logo.png"),
                  width: font158,
                  height: font40,
                ),
                InkWell(
                    onTap: () {
                      submitFavorite();
                    },
                    child: SvgPicture.asset('assets/icons/unfilled_heart.svg')),
              ],
            ),
            SizedBox(
              height: font79,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context).translate('akce'),
                    style: styleTitleContent,
                  ),
                  SizedBox(
                    height: font14,
                  ),
                  StreamBuilder<List<OferClass>>(
                    stream: dc.obsOfers,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        List<OferClass> ofers = snapshot.data!;

                        if (ofers.isEmpty) {
                          return waitForDataOfers();
                        } else {
                          return Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Expanded(
                                child: SizedBox(
                                  height: font150,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                      itemCount: ofers.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return _oferItem(ofers[index]);
                                      }),
                                ),
                              ),
                            ],
                          );
                        }
                      } else {
                        return waitForDataOfers();
                      }
                    },
                  ),
                  SizedBox(
                    height: font40,
                  ),
                  InkWell(
                    onTap: () {
                      submitNearesGS();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: colorMain,
                          borderRadius: BorderRadius.circular(font5)),
                      child: Padding(
                        padding: EdgeInsets.all(font10),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  dc.nearestGS.adress!,
                                  style: styleTitle1,
                                ),
                                Text(
                                  "(${dc.nearestGS.firmName})",
                                  style: styleTitle1.copyWith(
                                      fontWeight: FontWeight.w600),
                                )
                              ],
                            ),
                            SizedBox(
                              height: font10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                  decoration: BoxDecoration(
                                    color: colorWhiteText,
                                    borderRadius: BorderRadius.circular(font15),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: font2, horizontal: font8),
                                    child: Text(
                                     "${dc.nearestGS.distance.toString()} km",
                                      style: styleTitle1.copyWith(
                                          color: colorBlack),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: font12,
                                ),
                                Text(
                                  dc.nearestGS.getWorkingHours(),
                                  style: styleTitle1,
                                ),
                                  ],
                                ),
                                
                                SvgPicture.asset(
                                  'assets/icons/right_round_arrow.svg',
                                  height: font25,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: font40,
                  ),
                  InkWell(
                    onTap: () {
                      submitQuestions();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          vertical: font10, horizontal: font25),
                      // width: font148,
                      height: font140,
                      decoration: BoxDecoration(
                          color: colorModalWindow,
                          borderRadius: BorderRadius.circular(font5)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            child: Image.asset("assets/images/question_img.png"),
                            width: font158,
                            height: font50,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('questions'),
                              style: styleTitleContent,
                              //  textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _oferItem(OferClass ofer) {
    return Row(
      children: [
        InkWell(
            onTap: () {
              submitAkce(ofer);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: font190,
                  height: font95,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(ofer.imgUrl!),
                      fit: BoxFit.cover,
                    ),
                      color: colorModalWindow,
                      borderRadius: BorderRadius.circular(font5)),
                ),
                SizedBox(
                  height: font8,
                ),
                SizedBox(width: font190, child: Text(ofer.title!, style: styleTitle2,)),
              ],
            ),
          ),
          SizedBox(
            width: font12,
          ),
      ],
    );
  }

  Widget waitForDataOfers() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          InkWell(
            // onTap: () {
            //   submitAkce(1);
            // },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: font190,
                  height: font95,
                  decoration: BoxDecoration(
                      color: colorModalWindow,
                      borderRadius: BorderRadius.circular(font5)),
                ),
                SizedBox(
                  height: font8,
                ),
                Text("Title"),
              ],
            ),
          ),
          SizedBox(
            width: font12,
          ),
          InkWell(
            // onTap: () {
            //   submitAkce(1);
            // },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: font190,
                  height: font95,
                  decoration: BoxDecoration(
                      color: colorModalWindow,
                      borderRadius: BorderRadius.circular(font5)),
                ),
                SizedBox(
                  height: font8,
                ),
                Text("Title"),
              ],
            ),
          ),
          SizedBox(
            width: font12,
          ),
          InkWell(
            // onTap: () {
            //   submitAkce(1);
            // },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: font190,
                  height: font95,
                  decoration: BoxDecoration(
                      color: colorModalWindow,
                      borderRadius: BorderRadius.circular(font5)),
                ),
                SizedBox(
                  height: font8,
                ),
                Text("Title"),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
