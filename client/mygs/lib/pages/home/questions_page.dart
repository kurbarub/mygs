import 'package:MyGS/classes/question_class.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';

class QuestionsPage extends StatefulWidget {
  const QuestionsPage({super.key});

  @override
  State<QuestionsPage> createState() => _QuestionsPageState();
}

class _QuestionsPageState extends State<QuestionsPage> {
  final DataCollector dc = DataCollector();

  @override
  void initState() {
    super.initState();
    // dc.getQuestions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: Container(
        child: StreamBuilder<List<QuestionClass>>(
          
          stream: dc.obsQuestions,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<QuestionClass> questions = snapshot.data!;
      
              if (questions.isEmpty) {
                return Container();
              } else {
                return ListView.builder(
                    clipBehavior: Clip.none,
                    itemCount: questions.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _questionTile(questions[index], index+1);
                    });
              }
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }

  Widget _questionTile(QuestionClass question, int index) {
    return Padding(
      padding: EdgeInsets.only(bottom: font18),
      child: Material(
        elevation: font4,
        shadowColor: colorBlack,
        borderRadius: BorderRadius.circular(font5),
        child: Container(
          decoration: BoxDecoration(
            color: colorWhiteText,
            borderRadius: BorderRadius.circular(font5)
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: font14, horizontal: font20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("$index. ${question.question}?", style: styleTitle2.copyWith(fontSize: font18),),
                SizedBox(height: font4,),
                Text("${question.answer}", style: styleTitle2.copyWith(fontWeight: FontWeight.w300),),
              ],
            ),
          ),
        ),
      ),
    );
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      title: Text(
        AppLocalizations.of(context).translate('questions'),
        style: styleTitleContent,
      ),
      backgroundColor: Colors.white,
      centerTitle: true,
    );
  }
}
