import 'package:MyGS/classes/ofer_cless.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';

class OferPage extends StatefulWidget {
  const OferPage({super.key, required this.ofer});

  final OferClass ofer;

  @override
  State<OferPage> createState() => _OferPageState();
}

class _OferPageState extends State<OferPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('akce'),
          // widget.ofer.title!,
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.all(font30),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: font330,
              width: double.infinity,
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(widget.ofer.imgUrl!),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.circular(font5)),
            ),
      
            SizedBox(height: font23,),
            Text(widget.ofer.title!, style: styleTitleContent.copyWith(fontWeight: FontWeight.bold),),
            SizedBox(height: font12,),
            Text(widget.ofer.content!, style: styleTitle2,),
            
          ],
        ),
      ),
    );
  }
}
