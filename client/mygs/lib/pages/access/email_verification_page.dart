import 'package:MyGS/classes/user_class.dart';
import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/components/modals/info_bottom_flushbar.dart';
import 'package:MyGS/pages/access/reset_password_page.dart';
import 'package:MyGS/pages/access/splash_page.dart';
import 'package:MyGS/pages/access/welcome_page.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/material.dart';


import '../../components/buttons/text_button.dart';
import '../../components/textfields/access_textfield.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class EmailVerification extends StatefulWidget {
  const EmailVerification({super.key, required this.email, this.password = null});

  final String email;
  final String? password;

  @override
  State<EmailVerification> createState() => _EmailVerificationState();
}

class _EmailVerificationState extends State<EmailVerification> {
  int timer = 20;
  DataCollector dc = DataCollector();
  final TextEditingController codeController = TextEditingController();

  void submitSendAgain() {
    // Navigator.of(context)
    //     .push(MaterialPageRoute(builder: (context) => const LoginPage()));
  }

  Future<void> submitFinish() async {

    if(widget.password != null){
      if(codeController.text == '') return;

      var user = UserClass(
        email: widget.email,
        password: widget.password,
        code:  int.parse(codeController.text.trim())
      );

      VerifyAccount response = await dc.verifyUserAccount(user);

      switch (response) {
        case VerifyAccount.alreadyverified: return sendMessage("USR009", fromServer: true);
        case VerifyAccount.bademail: return sendMessage("USR007", fromServer: true);
        case VerifyAccount.badcode: return sendMessage("USR010", fromServer: true);
        case VerifyAccount.networkError: return sendMessage("network_error", fromServer: true);
        case VerifyAccount.unknown: return sendMessage("unknown_error", fromServer: true);
        case VerifyAccount.success: {
          sendMessage("verify_success", fromServer: true, isError: false);

          Future.delayed(const Duration(seconds: 3), () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => const SplashPage()));
          });

          }
      }
    }else{

      if(codeController.text == '') return;

      var user = UserClass(
        email: widget.email,
        code:  int.parse(codeController.text.trim())
      );

      Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => ResetPasswordPage(code: user.code!, email: user.email!,)));


    }

  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : title) : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }

  @override
  Widget build(BuildContext context) {
    

    return Scaffold(
        // resizeToAvoidBottomInset: false,
        appBar: _appBar(),
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: font30, vertical: font50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context)
                        .translate('email_varification'),
                    style: styleTitleContent,
                  ),
                  SizedBox(
                    height: font20,
                  ),
                  AuthorizationTextField(
                    controller: codeController,
                    text: AppLocalizations.of(context).translate('code'),
                    tap: () {},
                  ),
                  SizedBox(
                    height: font8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '$timer s',
                            style: styleInscriptionsGreen.copyWith(
                                fontSize: font14),
                          ),
                          ButtonText(
                              text: AppLocalizations.of(context)
                                  .translate('send_again'),
                              onTap: () {
                                submitSendAgain();
                              })
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: font28,
                  ),
                  RegularButton(
                    context: AppLocalizations.of(context).translate('finish'),
                    onTap: () {
                      submitFinish();
                    },
                    isGreen: true,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}
