import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/pages/access/login_page.dart';
import 'package:MyGS/pages/access/reg_page.dart';
import 'package:MyGS/pages/access/splash_page.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../components/buttons/text_button.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}



class _WelcomePageState extends State<WelcomePage> {
  // get colorBlack => null;
  DataCollector dc = DataCollector();

  void submitSignIn() {
    
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const LoginPage()));
  }

  void submitSignUp() {

    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const RegistrationPage()));
  }

  void withoutLogin() async{
    await dc.continueWithoutLogin();
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const SplashPage()));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: font30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(top: font116),
                child: Column(
                  children: [
                    Image.asset("assets/images/logo.png"),
                    SizedBox(height: font8,),
                    Text(
                      AppLocalizations.of(context).translate('main_description'),
                      style: styleTitle1.copyWith(color: colorBlack),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:  EdgeInsets.only(bottom: font54),
                child: Column(
                  
                  children: [
                    RegularButton(onTap: (() => {submitSignIn()}), context: "Sign In", isGreen: true,),
                    SizedBox(height: font16,),
                    RegularButton(onTap: (() => {submitSignUp()}), context: "Sign Up",),
                    SizedBox(height: font16,),
                    ButtonText(text: "Continue without login", onTap: (){withoutLogin();},)

                  ],
                ),
              )
            ],
            ),
        )
      );
  }
}
