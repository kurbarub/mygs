import 'dart:math';

import 'package:MyGS/pages/access/welcome_page.dart';
import 'package:MyGS/pages/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import '../tabs.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  late DataCollector dc;

  @override
  void initState() {
    _loadSplashData();
    super.initState();
  }
  
  _loadSplashData() async {
    dc = DataCollector();
    await dc.initPrefs();
    

    if(dc.isLoggedIn())
      toTabs();
    else {

      if(dc.isUnAthorizedVersion()){
        toTabs();
      }else{
        toWelcomePage();
      }
      
    }

      
  }

  void toWelcomePage() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => const WelcomePage()));
  }

  void toTabs() {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => const Tabs()));
  }

  @override
  Widget build(BuildContext context) {
    getWidth(context);

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: colorMain,
      body: Center(
        
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: font30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            
            children: [
              Image.asset("assets/images/logo.png"),
              SizedBox(height: font8),
              Text(
                AppLocalizations.of(context).translate('main_description'),
                style: styleTitle1.copyWith(color: colorBlack),
                
              ),
              
            ],
          ),
        ),
      )
    );
  }
}

