import 'package:MyGS/classes/user_class.dart';
import 'package:MyGS/pages/access/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../components/buttons/regular_button.dart';
import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/textfields/access_textfield.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class ResetPasswordPage extends StatefulWidget {
  const ResetPasswordPage({super.key, required this.email, required this.code});

  final String email;
  final int code;

  @override
  State<ResetPasswordPage> createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {

  TextEditingController _firstNewPasController = TextEditingController();
  TextEditingController _secondNewPasController = TextEditingController();
  DataCollector dc = DataCollector();


  void submitSave() async{


    if (_firstNewPasController.text.trim().isEmpty || _secondNewPasController.text.trim().isEmpty)
        return sendMessage("empty_pass");

    UserClass user = UserClass(
      email: widget.email,
      code: widget.code,
      password: _firstNewPasController.text.trim(),
      newPassword: _secondNewPasController.text.trim()
    );
    
    ResetPassword response = await dc.resetPassword(user);

    switch (response) {
      case ResetPassword.badcode: return sendMessage("USR010", fromServer: true);
      case ResetPassword.pasmismatch: return sendMessage("USR012", fromServer: true);
      case ResetPassword.networkError: return sendMessage("network_error", fromServer: true);
      case ResetPassword.unknown: return sendMessage("unknown_error", fromServer: true);
      case ResetPassword.wrongcurrpass: break;
      case ResetPassword.success: {
        sendMessage("reset_pass_seccess", fromServer: true, isError: false);
        Future.delayed(const Duration(seconds: 3), () {
          Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => LoginPage()));
        });
        }
    }

  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : title) : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context).translate('password'),
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: _body(),
    );
  }

    Widget _body(){

    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(font30),
        child: Column(
          children: [

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(AppLocalizations.of(context).translate('new_password'), style: styleTitle2,),
                SizedBox(height: font20,),
                AuthorizationTextField(
                  controller: _firstNewPasController,
                  text: AppLocalizations.of(context).translate('create_password'),
                  tap: () {},
                  isPassword: true,
                ),
                SizedBox(height: font16,),
                AuthorizationTextField(
                  controller: _secondNewPasController,
                  text: AppLocalizations.of(context).translate('confirm_password'),
                  tap: () {},
                  isPassword: true,
                ),
              ],
            ),
            SizedBox(height: font32,),
            RegularButton(context: AppLocalizations.of(context).translate('save'), onTap: (){submitSave();}, isGreen: true,)
          ],
        ),
      ),
    );

  }

}