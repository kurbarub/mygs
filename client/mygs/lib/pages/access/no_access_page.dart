import 'package:MyGS/pages/access/splash_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../components/buttons/regular_button.dart';
import '../../components/buttons/text_button.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class NoAccessPage extends StatefulWidget {
  const NoAccessPage({super.key});

  @override
  State<NoAccessPage> createState() => _NoAccessPageState();
}

class _NoAccessPageState extends State<NoAccessPage> {
  @override
  Widget build(BuildContext context) {

    DataCollector dc = DataCollector();

    void submitLigIn() async{
      await dc.exitFromUnathorizedVersion();

      Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => const SplashPage()));
    }
    
    return Scaffold(
        // resizeToAvoidBottomInset: false,
       
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: font30, vertical: font50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context)
                        .translate('access_denied'),
                    // style: styleTitleContent,
                    style: styleBold,
                  ),
                  SizedBox(
                    height: font8,
                  ),
                  Text(
                    AppLocalizations.of(context)
                        .translate('access_denied_desc'),
                    style: styleTitleContent,
              
                  ),
                  SizedBox(
                    height: font20,
                  ),
           

                  SizedBox(
                    height: font28,
                  ),
                  RegularButton(
                    context: AppLocalizations.of(context).translate('sign_in'),
                    onTap: () {
                      submitLigIn();
                    },
                    isGreen: true,
                  ),
                  SizedBox(
                    height: font8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ButtonText(
                        text: AppLocalizations.of(context)
                            .translate('back'),
                            isGreen: true,
                            onTap: (){
                              Navigator.pop(context);
                            },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ));
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}
