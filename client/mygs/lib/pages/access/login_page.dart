import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/pages/access/forgot_password_page.dart';
import 'package:MyGS/pages/access/reg_page.dart';
import 'package:MyGS/pages/access/splash_page.dart';
import 'package:MyGS/pages/home/home_page.dart';
import 'package:MyGS/pages/tabs.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../classes/user_class.dart';
import '../../components/buttons/google_button.dart';
import '../../components/buttons/text_button.dart';
import '../../components/dividers/text_divider.dart';
import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/textfields/access_textfield.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import 'email_verification_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  DataCollector dc = DataCollector();



  void submitSignIn() async{

    if (emailController.text.isEmpty) return sendMessage("empty_email");
    if (passwordController.text.trim().isEmpty) return sendMessage("empty_pass");

    UserClass user = UserClass(
        email: emailController.text.trim(),
        password: passwordController.text.trim(),
    );

    LogIn response = await dc.logIn(user);

    switch (response) {
      case LogIn.baddata: return sendMessage("USR008", fromServer: true);
      case LogIn.notyetverified: {
        Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => EmailVerification(email: user.email!, password: user.password!,)));
        return ;
      };
      case LogIn.networkError: return sendMessage("network_error", fromServer: true);
      case LogIn.unknown: return sendMessage("unknown_error", fromServer: true);
      case LogIn.success: {
          Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => const Tabs()));
        }
    }
  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : "reg") : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }

  void submitForgotPas() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const ForgotPasswordPage()));
  }

  void submitWithoutLogIn() async{
    await dc.continueWithoutLogin();
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const SplashPage()));
  }

  void submitSignUp() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const RegistrationPage()));
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        // resizeToAvoidBottomInset: false,
        appBar: _appBar(),
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: font30, vertical: font50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context).translate('login_to_account'),
                    style: styleTitleContent,
                  ),
                  SizedBox(
                    height: font20,
                  ),
                  AuthorizationTextField(
                    controller: emailController,
                    text: "Email",
                    tap: () {},
                  ),
                  SizedBox(
                    height: font16,
                  ),
                  AuthorizationTextField(
                    isPassword: true,
                    controller: passwordController,
                    text: AppLocalizations.of(context).translate('password'),
                    tap: () {},
                  ),
                  SizedBox(
                    height: font28,
                  ),
                  RegularButton(
                    context: AppLocalizations.of(context).translate('sign_in'),
                    onTap: () {
                      submitSignIn();
                    },
                    isGreen: true,
                  ),
                  SizedBox(
                    height: font12,
                  ),
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ButtonText(
                        onTap: () {submitForgotPas();},
                        text:
                            '${AppLocalizations.of(context).translate('forgot_password')}?',
                        isGreen: true,
                      )
                    ],
                  ),
                  SizedBox(
                    height: font43,
                  ),
                  TextDivider(
                    text: AppLocalizations.of(context).translate('or'),
                  ),
                  SizedBox(
                    height: font43,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GoogleButton(
                        context:
                            AppLocalizations.of(context).translate('continue_without_login'),
                        onTap: () {
                          submitWithoutLogIn();
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: font20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${AppLocalizations.of(context).translate('dont_have_an_account')}? ',
                        style: styleInscriptions,
                      ),
                      ButtonText(
                        text: AppLocalizations.of(context).translate('sign_up'),
                        onTap: () {
                          submitSignUp();
                        },
                        isGreen: true,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}
