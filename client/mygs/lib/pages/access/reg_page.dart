import 'package:MyGS/classes/user_class.dart';
import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/textfields/access_textfield.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import 'email_verification_page.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({super.key});

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  DateTime selectedDate = DateTime.parse("2000-06-26");
  DataCollector dc = DataCollector();
  String selectedGender = '0';
  String selectedDateView = '';

  final TextEditingController nameController = TextEditingController();
  final TextEditingController surnameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();

  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordAgainController = TextEditingController();

  Future<void> submitNextStep() async {
    if (nameController.text.isEmpty) return sendMessage("empty_name");
    if (surnameController.text.isEmpty) return sendMessage("empty_surname");
    if (emailController.text.isEmpty) return sendMessage("empty_email");
    if (phoneController.text.isEmpty) return sendMessage("empty_phone");
    if (passwordController.text.trim().isEmpty ||
        passwordAgainController.text.trim().isEmpty)
      return sendMessage("empty_pass");
    if (passwordController.text.trim() != passwordAgainController.text.trim())
      return sendMessage("mis_pass");

    if (selectedDateView == '') return sendMessage("empty_dob");

    UserClass user = UserClass(
        name: nameController.text.trim(),
        surname: surnameController.text.trim(),
        email: emailController.text.trim(),
        phone: phoneController.text.trim(),
        password: passwordController.text.trim(),
        dob: selectedDate.toIso8601String(),
        gender: int.parse(selectedGender));

    Registration response = await dc.createUser(user);

    switch (response) {
      case Registration.alreadyregistered: return sendMessage("USR003", fromServer: true);
      case Registration.bademail: return sendMessage("USR002", fromServer: true);
      case Registration.badphone: return sendMessage("USR004", fromServer: true);
      case Registration.baduserdata: return sendMessage("USR005", fromServer: true);
      case Registration.shortpass: return sendMessage("USR006", fromServer: true);
      case Registration.networkError: return sendMessage("network_error", fromServer: true);
      case Registration.unknown: return sendMessage("unknown_error", fromServer: true);
      case Registration.success: {
        sendMessage("reg_success", fromServer: true, isError: false);
          Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => EmailVerification(email: user.email!, password: user.password!,)));
        }
    }
  }

  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : "reg") : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1940),
      lastDate: DateTime(2020),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        selectedDateView = DateFormat("dd.MM.yyyy").format(picked).toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // resizeToAvoidBottomInset: false,
        appBar: _appBar(),
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: font30, vertical: font50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context)
                        .translate('create_your_account'),
                    style: styleTitleContent,
                  ),
                  SizedBox(
                    height: font20,
                  ),
                  AuthorizationTextField(
                    controller: nameController,
                    text: AppLocalizations.of(context).translate('name'),
                    tap: () {},
                  ),
                  SizedBox(
                    height: font16,
                  ),
                  AuthorizationTextField(
                    controller: surnameController,
                    text: AppLocalizations.of(context).translate('surname'),
                    tap: () {},
                  ),
                  SizedBox(
                    height: font16,
                  ),
                  AuthorizationTextField(
                    controller: emailController,
                    text: "Email",
                    tap: () {},
                  ),
                  SizedBox(
                    height: font16,
                  ),
                  AuthorizationTextField(
                    controller: phoneController,
                    text:
                        AppLocalizations.of(context).translate('phone_number'),
                    tap: () {},
                  ),
                  SizedBox(
                    height: font16,
                  ),
                  AuthorizationTextField(
                    controller: passwordController,
                    isPassword: true,
                    text: AppLocalizations.of(context).translate('password'),
                    tap: () {},
                  ),
                  SizedBox(
                    height: font16,
                  ),
                  AuthorizationTextField(
                    controller: passwordAgainController,
                    isPassword: true,
                    text:
                        AppLocalizations.of(context).translate('con_password'),
                    tap: () {},
                  ),
                  SizedBox(
                    height: font16,
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Material(
                          shadowColor: colorBlack,
                          elevation: font4,
                          borderRadius: BorderRadius.circular(font5),
                          child: InkWell(
                            onTap: () {
                              _selectDate(context);
                            },
                            child: Container(
                              height: font50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(font5),
                                color: colorTextfieldBackround,
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: font20, vertical: font12),
                                child: Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      selectedDateView == ''
                                          ? AppLocalizations.of(context)
                                              .translate('dob')
                                          : selectedDateView.toString(),
                                      style: styleInscriptions,
                                    ),
                                    SvgPicture.asset(
                                        'assets/icons/bottom_arrow.svg'),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: font18,
                      ),
                      Text(
                        AppLocalizations.of(context).translate('gender'),
                        style: styleInscriptions,
                      ),
                      SizedBox(
                        width: font10,
                      ),
                      Expanded(
                        flex: 1,
                        child: Material(
                          shadowColor: colorBlack,
                          elevation: font4,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(font5),
                              topLeft: Radius.circular(font5)),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                selectedGender = '0';
                              });
                            },
                            child: Container(
                              height: font50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(font5),
                                    topLeft: Radius.circular(font5)),
                                color: selectedGender != '0'
                                    ? colorTextfieldBackround
                                    : colorMain.withOpacity(0.5),
                              ),
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: font10, vertical: font14),
                                  child:
                                      SvgPicture.asset('assets/icons/male.svg'),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: font2,
                      ),
                      Expanded(
                        flex: 1,
                        child: Material(
                          shadowColor: colorBlack,
                          elevation: font4,
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(font5),
                              topRight: Radius.circular(font5)),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                selectedGender = '1';
                              });
                            },
                            child: Container(
                              height: font50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(font5),
                                    topRight: Radius.circular(font5)),
                                color: selectedGender != '1'
                                    ? colorTextfieldBackround
                                    : colorMain.withOpacity(0.5),
                              ),
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: font10, vertical: font12),
                                  child: SvgPicture.asset(
                                    'assets/icons/female.svg',
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: font28,
                  ),
                  RegularButton(
                    context:
                        AppLocalizations.of(context).translate('next_step'),
                    onTap: () {
                      submitNextStep();
                    },
                    isGreen: true,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}
