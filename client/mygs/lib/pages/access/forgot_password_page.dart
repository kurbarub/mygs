import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../components/buttons/regular_button.dart';
import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/textfields/access_textfield.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import 'email_verification_page.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({super.key});

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  DataCollector dc = DataCollector();
  final TextEditingController emailController = TextEditingController();

  void submitSend() async{

    if (emailController.text.isEmpty) return sendMessage("empty_email");

    VerCodeChangePass response = await dc.sendCodeForChangePass(emailController.text.trim());

    switch (response) {
      case VerCodeChangePass.bademail: return sendMessage("USR007", fromServer: true);
      case VerCodeChangePass.networkError: return sendMessage("network_error", fromServer: true);
      case VerCodeChangePass.unknown: return sendMessage("unknown_error", fromServer: true);
      case VerCodeChangePass.success: {
          Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => EmailVerification(email: emailController.text.trim())));
        }
    }
    
  }
  
  void sendMessage(String title, {bool fromServer = false, bool isError = true}) {
    String t = fromServer ? (isError ? "server_error" : "reg") : title;
    String m = fromServer ? (isError? title : '${title}_disc') : '${title}_disc';

    infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(t),
        message: AppLocalizations.of(context).translate(m),
        isSuccess: !isError,
        isError: isError);
  }



  @override
  Widget build(BuildContext context) {
    

    return Scaffold(
        // resizeToAvoidBottomInset: false,
        appBar: _appBar(),
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: font30, vertical: font50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppLocalizations.of(context)
                        .translate('for_pass_des'),
                    style: styleTitleContent,
                  ),
                  SizedBox(
                    height: font20,
                  ),
                  AuthorizationTextField(
                    controller: emailController,
                    text: AppLocalizations.of(context).translate('email'),
                    tap: () {},
                  ),
                  SizedBox(
                    height: font28,
                  ),
                  RegularButton(
                    context: AppLocalizations.of(context).translate('forg_pass'),
                    onTap: () {
                      submitSend();
                    },
                    isGreen: true,
                  ),
                ],
              ),
            ),
          ),
        ));
  }


    PreferredSizeWidget _appBar() {
    return AppBar(
      title:  Text(AppLocalizations.of(context).translate('forg_pass'),),
      centerTitle: true,
      leading: IconButton(
        icon: const Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}