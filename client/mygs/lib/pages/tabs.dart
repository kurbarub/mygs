import 'package:MyGS/pages/home/home_page.dart';
import 'package:MyGS/pages/menu/menu_drawer.dart';
import 'package:MyGS/pages/refueling/refueling_page.dart';
import 'package:MyGS/pages/transactions/transactions_page.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../components/modals/info_bottom_flushbar.dart';
import '../utility/translation/app_localization.dart';
import '../utility/variables/text_styles.dart';
import 'maps/maps_page.dart';

class Tabs extends StatefulWidget {
  const Tabs({super.key});

  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  late int _selectedIndex;

  DataCollector dc = DataCollector();

  static const List<Widget> _pages = [
    HomePage(),
    MapsPage(),
    RefuelingPage(),
    TransactionsPage(),
    MenuDrawer(),
    
  ];

  static const List<String> _pagesLabel = [
    "home",
    "maps",
    "refueling",
    "transactions",
    "menu"
  ];

  @override
  void initState() {
    _selectedIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(child: _pages[_selectedIndex]),
      bottomNavigationBar: _bottomNavBar(),
      endDrawer: const MenuDrawer(),
      floatingActionButton: _fab(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      resizeToAvoidBottomInset: false,
    );
  }

  void _onItemTapped(int index) {


    if((index == 2 || index == 3) && ( !dc.isLoggedIn() || dc.isUnAthorizedVersion())){
      infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate("access_denied"),
        message: AppLocalizations.of(context).translate("access_denied_desc"),
         isError: true);
    }else{
      setState(() {
        _selectedIndex = index;
      });
    }

  }

  Widget _fab() {
    return SizedBox(
      width: font80,
      height: font80,
      child: FittedBox(
        child: FloatingActionButton(
          elevation: 0,
          child:
              SvgPicture.asset('assets/icons/fuel_panel.svg', height: font40),
          backgroundColor: colorLightGreen,
          onPressed: () {
            _onItemTapped(2);
          },
        ),
      ),
    );
  }

  Widget _bottomNavBar() {
    return BottomAppBar(
      child: Container(
        decoration: BoxDecoration(color: colorMenuPanel),
        height: font94,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                MaterialButton(
                  minWidth: font40,
                  onPressed: () {
                    _onItemTapped(0);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        'assets/icons/home_panel.svg',
                        width: font23,
                      ),
                      Text(
                          AppLocalizations.of(context)
                              .translate(_pagesLabel[0]),
                          style: styleTitleContent.copyWith(fontSize: font14))
                    ],
                  ),
                ),
                MaterialButton(
                  minWidth: font40,
                  onPressed: () {
                    _onItemTapped(1);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/icons/maps_panel.svg',
                          width: font23),
                      Text(
                          AppLocalizations.of(context)
                              .translate(_pagesLabel[1]),
                          style: styleTitleContent.copyWith(fontSize: font14))
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                MaterialButton(
                  minWidth: font40,
                  onPressed: () {
                    _onItemTapped(3);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/icons/transaktions_panel.svg',
                          width: font23),
                      Text(
                          AppLocalizations.of(context)
                              .translate(_pagesLabel[3]),
                          style: styleTitleContent.copyWith(fontSize: font14))
                    ],
                  ),
                ),
                MaterialButton(
                  minWidth: font40,
                  onPressed: () {
                    // _onItemTapped(4);
                    _scaffoldKey.currentState!.openEndDrawer();
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/icons/menu_panel.svg',
                          width: font23),
                      Text(
                        AppLocalizations.of(context).translate(_pagesLabel[4]),
                        style: styleTitleContent.copyWith(fontSize: font14),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );

    // return BottomNavigationBar(
    //   items: [
    //     BottomNavigationBarItem(
    //       label: AppLocalizations.of(context).translate(_pagesLabel[0]) ,
    //       icon: SvgPicture.asset('assets/icons/home_panel.svg')
    //     ),
    //     BottomNavigationBarItem(
    //       label: AppLocalizations.of(context).translate(_pagesLabel[1]) ,
    //       icon: SvgPicture.asset('assets/icons/maps_panel.svg')
    //     ),
    //     BottomNavigationBarItem(
    //       label: AppLocalizations.of(context).translate(_pagesLabel[2]) ,
    //       icon: SvgPicture.asset('assets/icons/home_panel.svg')
    //     ),
    //     BottomNavigationBarItem(
    //       label: AppLocalizations.of(context).translate(_pagesLabel[3]) ,
    //       icon: SvgPicture.asset('assets/icons/transaktions_panel.svg')
    //     ),
    //     BottomNavigationBarItem(
    //       label: AppLocalizations.of(context).translate(_pagesLabel[4]) ,
    //       icon: SvgPicture.asset('assets/icons/menu_panel.svg')
    //     ),
    //   ],
    //   currentIndex: _selectedIndex,
    //   selectedItemColor: colorMain,
    //   unselectedItemColor: colorBlack,
    //   backgroundColor: colorMenuPanel,
    //   onTap: _onItemTapped,
    // );
  }
}
