import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/text_styles.dart';

class InstructionPage extends StatefulWidget {
  const InstructionPage({super.key});

  @override
  State<InstructionPage> createState() => _InstructionPageState();
}

class _InstructionPageState extends State<InstructionPage> {
  List<String> instructionPath = [
    "1.png",
    "2.png",
    "3.png",
    "4.png",
    "5.png"
  ];

  int selectedImg = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context).translate('instruction'),
            style: styleTitleContent,
          ),
        ),
        body: _body());
  }

  Widget _body() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: font100, horizontal: font30),
      child: Column(
        children: [
          SizedBox(
            height: font400,
            child: Swiper(
              itemBuilder: (BuildContext context, int index) {

                return Image.asset(
                  "assets/images/instruction/${instructionPath[index]}",
                  fit: BoxFit.fill,
                );
              },
              index: selectedImg,
              itemCount: instructionPath.length,
              viewportFraction: 0.8,
              scale: 0.8,
              onIndexChanged: (val) {
                setState(() {
                  selectedImg = val;
                });
              },
              // control: new SwiperControl(),
            ),
          ),
          SizedBox(
            height: font30,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: font40),
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.baseline,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.generate(instructionPath.length, (index) {
                    return Container(
                        height: font20,
                        width: font20,
                        decoration: BoxDecoration(
                            color: selectedImg ==index? colorMain: colorUnderlineBorder. withOpacity(0.5),
                            borderRadius: BorderRadius.circular(font50)),
                      );}
          
            )),
          )
        ],
      ),
    );
  }
}
