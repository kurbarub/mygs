import 'package:MyGS/classes/auto_class.dart';
import 'package:MyGS/classes/card_class.dart';
import 'package:MyGS/classes/fuel_class.dart';
import 'package:MyGS/classes/gas_station_class.dart';
import 'package:MyGS/classes/refueling_stand_class.dart';
import 'package:MyGS/pages/refueling/instruction_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../components/buttons/regular_button.dart';
import '../../components/dividers/regular_divider.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class ProcessBottomSheet extends StatefulWidget {
  const ProcessBottomSheet({
    super.key,
    required this.isHandicapped,
    required this.handicapedChangeValue,
    required this.selectedFuel,
    required this.fuelChangeValue,
    required this.litters,
    required this.littersChangeValue,
    required this.price,
    required this.priceChangeValue,
    required this.selectedAuto,
    required this.autoChangeValue,
    required this.selectedCard,
    required this.cardChangeValue,
    required this.dispanserNumber,
    required this.gs, required this.pay,
  });

  final bool isHandicapped;
  final ValueChanged handicapedChangeValue;

  final FuelClass? selectedFuel;
  final ValueChanged fuelChangeValue;

  final double litters;
  final ValueChanged littersChangeValue;

  final double price;
  final ValueChanged priceChangeValue;

  final AutoClass? selectedAuto;
  final ValueChanged autoChangeValue;

  final CardClass? selectedCard;
  final ValueChanged cardChangeValue;

  final int dispanserNumber;
  final GasStation gs;

  final VoidCallback pay;
  // final List<FuelClass> fuels;
  // final List<AutoClass> cars;
  // final List<CardClass> cards;

  @override
  State<ProcessBottomSheet> createState() => _ProcessBottomSheetState();
}

class _ProcessBottomSheetState extends State<ProcessBottomSheet> {
  final DataCollector dc = DataCollector();
  late bool isHandicaped;
  FuelClass? fuel;
  AutoClass? auto;
  CardClass? card;
  late double price;
  late double litters;

  List<AutoClass>? cars;

  List<FuelClass>? fuelsGS;

  @override
  void initState() {
    isHandicaped = widget.isHandicapped;
    fuel = widget.selectedFuel;
    auto = widget.selectedAuto;
    card = widget.selectedCard;
    price = widget.price;
    litters = widget.litters;

    List<RefuelingStand> r = widget.gs.refuelingStands!;

    dc.getCards();
    dc.getCars();

    fuelsGS = [];


    for(RefuelingStand i in r){
      if(i.standId == widget.dispanserNumber){
        fuelsGS = i.fuels;
      } 
    }
    super.initState();
  }

  void submitQuestion() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const InstructionPage()));
  }

  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );
  @override
  Widget build(BuildContext context) {
    return makeDismissible(
        child: DraggableScrollableSheet(
            initialChildSize: 0.8,
            minChildSize: 0,
            builder: (_, controller) => Container(
                  child: Container(
                    decoration: BoxDecoration(
                        color: colorModalWindow,
                        borderRadius: BorderRadius.vertical(
                            top: Radius.circular(font25))),
                    child: Padding(
                      padding: EdgeInsets.all(font30),
                      child: ListView(
                        controller: controller,
                        clipBehavior: Clip.none,
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    isHandicaped = !isHandicaped;
                                    widget.handicapedChangeValue(isHandicaped);
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: isHandicaped
                                          ? colorMain.withOpacity(0.5)
                                          : Colors.transparent,
                                      borderRadius:
                                          BorderRadius.circular(font5)),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: font5, horizontal: font8),
                                    child: SvgPicture.asset(
                                      'assets/icons/disabled_person.svg',
                                      height: font30,
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                "${widget.dispanserNumber.toString()} ${AppLocalizations.of(context).translate('dispenser')}",
                                style: styleTitleContent,
                              ),
                              InkWell(
                                onTap: () {
                                  submitQuestion();
                                },
                                child: Image.asset(
                                  "assets/images/question.png",
                                  width: font40,
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: font14,
                          ),
                          RegularDivider(
                            height: font2,
                          ),
                          SizedBox(
                            height: font27,
                          ),
                          Wrap(
                              alignment: WrapAlignment.start,
                              children: List.generate(fuelsGS!.length, (index) {
                                return _priceBox(fuelsGS![index]);
                              })),
                          SizedBox(
                            height: font20,
                          ),
                          Text(
                            AppLocalizations.of(context)
                                .translate('fuel_quantiy'),
                            style: styleTitle2,
                          ),
                          SizedBox(
                            height: font16,
                          ),
                          _slider(),
                          SizedBox(
                            height: font16,
                          ),
                          Row(
                            // mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    AppLocalizations.of(context)
                                        .translate('auto'),
                                    style: styleTitle2,
                                  ),
                                  SizedBox(
                                    height: font12,
                                  ),
                                  autoSelector()
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    AppLocalizations.of(context)
                                        .translate('paymen_method'),
                                    style: styleTitle2,
                                  ),
                                  SizedBox(
                                    height: font12,
                                  ),
                                  cardSelector()
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: font32,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "${price.toStringAsFixed(2)} Kc",
                                style: styleTitle2.copyWith(fontSize: font32),
                              )
                            ],
                          ),
                          SizedBox(
                            height: font26,
                          ),
                          RegularButton(
                            context:
                                AppLocalizations.of(context).translate('pay'),
                            onTap: widget.pay,
                            isGreen: true,
                          )
                        ],
                      ),
                    ),
                  ),
                )));
  }

  Widget autoSelector() {
    return StreamBuilder<List<AutoClass>>(
        stream: dc.obsCars,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<AutoClass> cars = snapshot.data!;
            return ConstrainedBox(
              constraints: BoxConstraints(maxHeight: font80, maxWidth: font170),
              child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 2),
                  itemCount: cars.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      child: _carTile(cars[index]),
                      onTap: () {
                        setState(() {
                          auto = cars[index];
                          widget.autoChangeValue(auto);
                        });
                      },
                    );
                  }),
            );
          } else {
            return Container();
          }
        });
  }

  Widget cardSelector() {
    return StreamBuilder<List<CardClass>>(
        stream: dc.obsCards,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<CardClass> cards = snapshot.data!;
            return ConstrainedBox(
              constraints: BoxConstraints(maxHeight: font80, maxWidth: font170),
              child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 2),
                  itemCount: cards.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      child: _cardTile(cards[index]),
                      onTap: () {
                        setState(() {
                          card = cards[index];
                          widget.cardChangeValue(card);
                        });
                      },
                    );
                  }),
            );
          } else {
            return Container();
          }
        });
  }

  Widget _carTile(AutoClass car) {
    return Padding(
      padding: EdgeInsets.only(right: font6, bottom: font8),
      child: Material(
        elevation: font4,
        shadowColor: colorBlack,
        borderRadius: BorderRadius.circular(font5),
        child: Container(
          alignment: Alignment.center,
          height: font34,
          decoration: BoxDecoration(
              color: (auto != null && auto!.id == car.id)
                  ? colorLightGreen
                  : colorLightWhite,
              borderRadius: BorderRadius.circular(font5)),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
            child: Text(
              car.number.toString(),
              style: styleTitle2,
              maxLines: 1,
            ),
          ),
        ),
      ),
    );
  }

  Widget _cardTile(CardClass card) {
    return Padding(
      padding: EdgeInsets.only(right: font6, bottom: font8),
      child: Material(
        elevation: font4,
        shadowColor: colorBlack,
        borderRadius: BorderRadius.circular(font5),
        child: Container(
          alignment: Alignment.center,
          height: font34,
          decoration: BoxDecoration(
              color: (this.card != null && this.card!.id == card.id)
                  ? colorLightGreen
                  : colorLightWhite,
              borderRadius: BorderRadius.circular(font5)),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: font6, horizontal: font8),
            child: Text(
              "... ${card.lastFourNumber}",
              style: styleTitle2,
              maxLines: 1,
            ),
          ),
        ),
      ),
    );
  }

  Widget _priceBox(FuelClass fuel) {
    return Padding(
      padding: EdgeInsets.only(bottom: font12, right: font16),
      child: Material(
        elevation: font4,
        shadowColor: colorBlack,
        borderRadius: BorderRadius.circular(font5),
        child: InkWell(
          onTap: () {
            setState(() {
              this.fuel = fuel;
              widget.fuelChangeValue(fuel);
              price = 0.0;
              litters = 0.0;
            });
          },
          child: Container(
            width: font79,
            decoration: BoxDecoration(
                color: (this.fuel != null && this.fuel!.id == fuel.id)
                    ? colorLightGreen
                    : colorTextfieldBackround,
                borderRadius: BorderRadius.circular(font5)),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: font10, vertical: font6),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    fuel.name!,
                    style: styleTitle3,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: font4,
                  ),
                  Text(
                    "${fuel.price} Kc",
                    style: styleFuelPrice,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _slider() {
    return SliderTheme(
      data: SliderThemeData(
          trackHeight: font100,
          trackShape: RectangularSliderTrackShape(),
          thumbShape: SliderComponentShape.noOverlay,
          overlayShape: SliderComponentShape.noOverlay,
          valueIndicatorShape: SliderComponentShape.noOverlay,
          activeTrackColor: colorMain,
          inactiveTrackColor: colorMain.withOpacity(0.2),
          activeTickMarkColor: Colors.transparent,
          inactiveTickMarkColor: Colors.transparent),
      child: Stack(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Positioned(
              left: font30,
              top: font90,
              child: Text(
                "${price.toStringAsFixed(2)} Kc",
                style: styleTitle2,
              )),
          RotatedBox(
            quarterTurns: 3,
            child: Slider(
              value: litters,
              min: 0,
              max: 75,
              divisions: 15,
              onChanged: (value) {
                setState(() {
                  if (fuel != null) {
                    litters = value;
                    widget.littersChangeValue(value);
                    price = value * fuel!.price!;
                    widget.priceChangeValue(price);
                  }
                });
              },
            ),
          ),
          Positioned(
              right: font30,
              top: font90,
              child: Text(
                "${litters.toStringAsFixed(2)} l",
                style: styleTitle2,
              )),
        ],
      ),
    );
  }
}
