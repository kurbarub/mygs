import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/pages/home/home_page.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../classes/feed_back_class.dart';
import '../../classes/fuel_class.dart';
import '../../components/dividers/regular_divider.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import '../access/no_access_page.dart';
import '../maps/feed_back_page.dart';

class GratitudeBottomSheet extends StatefulWidget {
  const GratitudeBottomSheet({super.key});
  

  @override
  State<GratitudeBottomSheet> createState() => _GratitudeBottomSheetState();
}

class _GratitudeBottomSheetState extends State<GratitudeBottomSheet> {
  
  DataCollector dc = DataCollector();
  
  void submitMainPage() {
    Navigator.pop(context);
  }
  void submitFeedback() async{
   
   

    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => FeedBackPage(fb: FeedBack(), itemId:0)));
  }

  

  @override
  Widget build(BuildContext context) {
    return Container(
      height: font700,
      decoration: BoxDecoration(
          color: colorModalWindow,
          borderRadius: BorderRadius.vertical(top: Radius.circular(font25))),
      child: Padding(
        padding: EdgeInsets.all(font30),
        child: Column(
          // controller: controller,
          // clipBehavior: Clip.none,
          // crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: font100,
            ),
            Text(
              "${AppLocalizations.of(context).translate('ready')}!",
              style: styleBold.copyWith(fontSize: font24),
            ),
            SizedBox(
              height: font8,
            ),
            Text(
              "${AppLocalizations.of(context).translate('thank_for_purchase')}!",
              style: styleTitleContent,
            ),
            SizedBox(
              height: font32,
            ),
            Material(
              elevation: font4,
              shadowColor: colorBlack,
              borderRadius: BorderRadius.circular(font5),
              child: Container(
                decoration: BoxDecoration(
                    color: colorWhiteText,
                    borderRadius: BorderRadius.circular(font5)),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: font21, horizontal: font18),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/icons/warning.svg',
                        height: font45,
                      ),
                      SizedBox(
                        width: font12,
                      ),
                      Expanded(
                          child: Text(
                        AppLocalizations.of(context)
                            .translate('warning_after_refuel'),
                        style: styleTitle2,
                      ))
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: font200,
            ),
            Row(
              children: [
                Expanded(
                  child: RegularButton(
                      isGreen: true,
                      context:
                          AppLocalizations.of(context).translate('main_page'),
                      onTap: () {
                        submitMainPage();
                      }),
                ),
                SizedBox(
                  width: font18,
                ),
                Expanded(
                  child: RegularButton(
                   
                      context:
                          AppLocalizations.of(context).translate('feedback'),
                      onTap: () {
                        submitFeedback();
                      }),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
