import 'package:MyGS/classes/auto_class.dart';
import 'package:MyGS/classes/card_class.dart';
import 'package:MyGS/classes/gas_station_class.dart';
import 'package:MyGS/pages/refueling/gratitude_bottom_sheet.dart';
import 'package:MyGS/pages/refueling/process_bottom_sheet.dart';
import 'package:MyGS/pages/refueling/waiting_refueling_bottom_sheet.dart';
import 'package:MyGS/utility/variables/sizes.dart';

// import 'package:barcode_scan_fix/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_svg/svg.dart';

import '../../classes/fuel_class.dart';
import '../../components/modals/info_bottom_flushbar.dart';
import '../../components/textfields/single_border_textfield.dart';
import '../../services/data_collector.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';

class RefuelingPage extends StatefulWidget {
  const RefuelingPage({super.key});

  @override
  State<RefuelingPage> createState() => _RefuelingPageState();
}

class _RefuelingPageState extends State<RefuelingPage> {
  TextEditingController _codeController = TextEditingController();

  final DataCollector dc = DataCollector();

  var barcode;

  late bool isHandicaped = false;
  FuelClass? fuel;
  AutoClass? auto;
  CardClass? card;
  double price = 0.0;
  double litters = 0.0;
  int dispanser = -1;
  int gsid = -1;

  String _scanBarcode = 'Unknown';

  @override
  void initState() {

    isHandicaped = dc.callStaff!;
    super.initState();
  }

  Future<void> scan() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });

    if(_scanBarcode.length == 7){

      String s = _scanBarcode;

      int idx = s.indexOf("-");

      int gsId = int.parse(s.substring(0,idx).trim()) ;
      int rsId = int.parse(s.substring(idx+1).trim());


      print("----RS: ${gsId}-----RS: ${rsId}");

      GasStation gs =  await dc.getRefuelGS(gsId);

      if(gs.id == null) {
        infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate('gs_not_found'),
        message: AppLocalizations.of(context).translate('gs_not_found_desc'),
        isError: true);
        return;
      }else if(gs.refuelingStands!.where((element) => element.standId ==rsId).isEmpty){
        infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate('gs_not_found'),
        message: AppLocalizations.of(context).translate('gs_not_found_desc'),
        isError: true);
        return;
      }

      dispanser = rsId;
      gsid = gsId;
      _modalBottomSheetProcess(rsId);
    } 

  }

  void submitHelp() {
    _modalBottomSheetQuestion();
  }

  void start() async{

    // print("START");

    if(_codeController.text.length == 7){

      String s = _codeController.text;

      int idx = s.indexOf("-");

      int gsId = int.parse(s.substring(0,idx).trim()) ;
      int rsId = int.parse(s.substring(idx+1).trim());


      print("----RS: ${gsId}-----RS: ${rsId}");

      GasStation gs =  await dc.getRefuelGS(gsId);

      if(gs.id == null) {
        infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate('gs_not_found'),
        message: AppLocalizations.of(context).translate('gs_not_found_desc'),
        isError: true);
        return;
      }else if(gs.refuelingStands!.where((element) => element.standId ==rsId).isEmpty){
        infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate('gs_not_found'),
        message: AppLocalizations.of(context).translate('gs_not_found_desc'),
        isError: true);
        return;
      }
      dispanser = rsId;
      gsid = gsId;
      _modalBottomSheetProcess(rsId);
    } 

  }


  void submitPay() {

    if(fuel == null){
        mess('select_fuel');
        return;
    }
    if(litters == 0.0){
        mess('no_ltters');
        return;
    }
    if(card == null){
        mess('no_card');
        return;
    }
    if(auto == null){
        mess('no_car');
        return;
    }
    if(auto!.fuelType != fuel!.getFuelTypeIndex()){
        infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate('wrong_fuel'),
        message: AppLocalizations.of(context).translate('wrong_fuel_des') + auto!.getFuelTypeString(),
        isError: true);
        return;
    }

    print("START REFUILING");

    Navigator.pop(context);
    _modalBottomSheetRefuel(dispanser, gsid, auto!.id!, card!.id! );
  }

  void mess(String s){
      infoFlashbar(
        context: context,
        title: AppLocalizations.of(context).translate(s),
        message: "",
        isError: true);
  }

  void _modalBottomSheetQuestion() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: _sheet(),
            );
          });
    });
  }

  void _modalBottomSheetRefuel(int dispanser, int gsId, int carId, int cardId,) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: false,
          isDismissible: false,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: WaitingRefuelingBottomSheet(
                done: (){
                  
                  _modalBottomSheetGratitude();
                },
                dispanserNumber: dispanser,
                fuel: fuel!,
                isHandicaped: isHandicaped,
                price: price,
                litters: litters,
                gsId: gsId,
                carId: carId,
                cardId: cardId,
              ),
            );
          });
    });
  }

  void _modalBottomSheetGratitude() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: GratitudeBottomSheet());
          });
    });
  }

  void _modalBottomSheetProcess(int dispanser) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.black.withOpacity(0.5),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return StreamBuilder<GasStation>(
              stream: dc.obsRefuelGS,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  GasStation gs = snapshot.data!;

                  if (gs == null || gs == GasStation()) {
                    return Container();
                  } else {
                    return Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: ProcessBottomSheet(
                          pay: submitPay,
                          dispanserNumber: dispanser,
                          gs: gs,
                          isHandicapped: isHandicaped,
                          handicapedChangeValue: (value) {
                            isHandicaped = value;
                          },
                          selectedFuel: fuel,
                          fuelChangeValue: (value) {
                            fuel = value;
                          },
                          litters: litters,
                          littersChangeValue: (value) {
                            litters = value;
                          },
                          price: price,
                          priceChangeValue: (value) {
                            price = value;
                          },
                          selectedAuto: auto,
                          autoChangeValue: (value) {
                            auto = value;
                          },
                          selectedCard: card,
                          cardChangeValue: (value) {
                            card = value;
                          },
                        ));
                  }
                } else {
                  return Container();
                }
              },
            );
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(),
        body: _body());
  }

  Widget _body() {
    return Center(
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: font30),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            // crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                AppLocalizations.of(context)
                    .translate('enter_dispenser_number'),
                style: styleTitleContent,
              ),
              SizedBox(
                height: font12,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: font156,
                    child: SingleBorderTextfield(
                      start: (){start();},
                      controller: _codeController,
                      text: "000-000",
                      tap: () {},
                      isMask: true,
                    ),
                  ),
                  InkWell(
                    splashColor: Colors.transparent,
                    focusColor: Colors.transparent,
                    hoverColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    child: Image.asset(
                      "assets/images/question.png",
                      width: font40,
                    ),
                    onTap: () {
                      submitHelp();
                    },
                  ),
                ],
              ),
              SizedBox(
                height: font97,
              ),
              InkWell(
                splashColor: Colors.transparent,
                focusColor: Colors.transparent,
                hoverColor: Colors.transparent,
                highlightColor: Colors.transparent,
                child: Column(
                  children: [
                    Image.asset(
                      "assets/images/qr_code.png",
                      width: font70,
                    ),
                    SizedBox(
                      height: font12,
                    ),
                    Text(
                      AppLocalizations.of(context).translate('scan'),
                      style: styleTitle2.copyWith(fontSize: font24),
                    )
                  ],
                ),
                onTap: () {
                  scan();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );

  Widget _sheet() {
    return makeDismissible(
      child: Container(
        // height: font400,
        decoration: BoxDecoration(
            color: colorModalWindow,
            borderRadius: BorderRadius.vertical(top: Radius.circular(font25))),
        padding: EdgeInsets.symmetric(horizontal: font30, vertical: font49),
        child: Container(
          height: font480,
          width: font330,
          color: colorFakeImg,
          child: Image.asset(
                  "assets/images/instr.png",
                  fit: BoxFit.cover,
                )
        ),
      ),
    );
  }
}
