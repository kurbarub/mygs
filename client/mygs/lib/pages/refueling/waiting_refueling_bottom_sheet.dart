import 'dart:convert';

import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../../classes/fuel_class.dart';
import '../../components/dividers/regular_divider.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class WaitingRefuelingBottomSheet extends StatefulWidget {
   WaitingRefuelingBottomSheet(
      {super.key,
      required this.fuel,
      required this.isHandicaped,
      required this.price,
      required this.litters,
      required this.dispanserNumber, required this.gsId, required this.carId, required this.cardId, required this.done});

  final bool isHandicaped;
  final FuelClass fuel;
   double price;
   double litters;
  final int dispanserNumber;
  final int gsId;
  final int carId;
  final int cardId;
  final VoidCallback done;

  @override
  State<WaitingRefuelingBottomSheet> createState() =>
      _WaitingRefuelingBottomSheetState();
}

class _WaitingRefuelingBottomSheetState
    extends State<WaitingRefuelingBottomSheet> {

      DataCollector dc = DataCollector();
  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );
      
  late WebSocketChannel channel;

  late String  message = "Refueling progress";

  @override
  void initState() {

    channel = WebSocketChannel.connect(Uri.parse('wss://mygs-spring-boot-kotlin-app.herokuapp.com/refueling')); 

    Map messageMap = {
      'user_id': dc.userId,
      'count_litters': widget.litters,
      'total_amount': widget.price,
      'refueledCar_id': widget.carId,
      'refueledCard_id': widget.cardId,
      'gasStation_id': widget.gsId,
      'fuel_id': widget.fuel.id,
      'is_staff_need': widget.isHandicaped,
    };

    channel.stream.listen(
      (data) {
        setUpdateData(jsonDecode(data));
      },
      onError: (error) => print(error),
      onDone: (){
      Navigator.pop(context);
      widget.done();}
    );

    channel.sink.add(jsonEncode(messageMap));

    super.initState();
  }

  setUpdateData(s){

    setState(() {
      widget.price = double.parse(s['filled_price'].toString());
      widget.litters = double.parse(s['filled_liters'].toString());
      message = s['state'];
    });
  }

  // onDone(){
  //   Navigator.pop(context);
  //   _modalBottomSheetGratitude();
  // }

  @override
  void dispose() {
    channel.sink.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return 
        Container(
          height: font700,
          decoration: BoxDecoration(
              image: const DecorationImage(
                image: AssetImage("assets/images/refuel_back.png"),
                fit: BoxFit.fitWidth,
                alignment: Alignment.bottomCenter,
              ),
              color: colorModalWindow,
              borderRadius: BorderRadius.vertical(
                  top: Radius.circular(font25))),
          child: Padding(
            padding: EdgeInsets.all(font30),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: widget.isHandicaped
                              ? colorMain.withOpacity(0.5)
                              : Colors.transparent,
                          borderRadius: BorderRadius.circular(font5)),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: font5, horizontal: font8),
                        child: SvgPicture.asset(
                          'assets/icons/disabled_person.svg',
                          height: font30,
                        ),
                      ),
                    ),
                    
                    Text(
                      "${widget.dispanserNumber.toString()} ${AppLocalizations.of(context).translate('dispenser')}",
                      style: styleTitleContent,
                    ),
                    Text(""),
                    // InkWell(
                  ],
                ),
                SizedBox(
                  height: font14,
                ),
                RegularDivider(
                  height: font2,
                ),
                SizedBox(
                  height: font16,
                ),
                Text(
                  message,
                  style: styleTitleContent,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: font16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Material(
                      elevation: font4,
                      shadowColor: colorBlack,
                      // color: Colors.transparent,
                      borderRadius: BorderRadius.circular(font5),
                      child: Container(
                        width: font62,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: colorWhiteText, width: font3),
                            borderRadius:
                                BorderRadius.circular(font5),
                            color: colorModalWindow),
                        child: Padding(
                          padding:
                              EdgeInsets.symmetric(vertical: font6),
                          child: Text(
                            widget.fuel.name!,
                            style: styleTitle3,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: font26,
                    ),
                    Column(
                      children: [
                        Material(
                            elevation: font4,
                            shadowColor: colorBlack,
                            borderRadius: BorderRadius.circular(font5),
                            child: Container(
                              width: font100,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(font5),
                                  color: colorWhiteText),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: font6),
                                child: Text(
                                  "${widget.price.toStringAsFixed(2)} Kc",
                                  style: styleTitle2,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        SizedBox(
                          height: font18,
                        ),
                        Material(
                          elevation: font4,
                          shadowColor: colorBlack,
                          borderRadius: BorderRadius.circular(font5),
                          child: Container(
                            width: font100,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(font5),
                                color: colorWhiteText),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: font6),
                              child: Text(
                                "${widget.litters.toStringAsFixed(2)} ${AppLocalizations.of(context).translate('liters')}",
                                style: styleTitle2,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                       
                      ],
                    )
                  ],
                ),
                SizedBox(height: font50,),
                CircularProgressIndicator(
                  color: colorLightGreen,
                  // value: font5,
                  strokeWidth: font5,
                ),
                SizedBox(height: font100,),
                 widget.isHandicaped
                            ? Text(AppLocalizations.of(context)
                                .translate('please_wait_attendant'), style: styleTitle2.copyWith(color: colorWhiteText),)
                            : Text('')
              ],
            ),
          ),
        );
  }
}
