// ignore_for_file: unnecessary_null_comparison

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:shared_preferences/shared_preferences.dart';
// import 'package:truth/services/data_collector.dart';

class AppLanguage extends ChangeNotifier {
  // final DataCollector dc = DataCollector();
  // Locale _appLocale = Locale('cs');
  Locale _appLocale = const Locale('en', '');

  Locale get appLocal => _appLocale;
  dynamic fetchLocale() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString('language_code') == null) {
      String? langCode;
      WidgetsFlutterBinding.ensureInitialized();
      while (ui.window.locale == null) {
        await Future.delayed(const Duration(milliseconds: 100));
      }
      if (ui.window.locale != null) {
        langCode = ui.window.locale.languageCode;
        print('>>> langCode: $langCode');
      }
      // if (langCode == 'ar') {
      //   _appLocale = const Locale('ar');
      //   dc.setLanguage('ar');
      // }
      // else  
      {
        _appLocale = const Locale('en');
        // dc.setLanguage('en');
      } 
      return null;
    }
    
    _appLocale = Locale(prefs.getString('language_code')!, '');
    return null;
  }

  Future<void> changeLanguage(Locale type) async {
    var prefs = await SharedPreferences.getInstance();
    if (_appLocale == type) {
      return;
    }
    if (type == const Locale('en')) {
      // dc.setLanguage('en');
      _appLocale = const Locale('en');
      await prefs.setString('language_code', 'en');
      await prefs.setString('countryCode', '');
    } 
    // else {
    //   dc.setLanguage('ar');
    //   _appLocale = const Locale('ar');
    //   await prefs.setString('language_code', 'ar');
    //   await prefs.setString('countryCode', '');
    // }
    notifyListeners();
  }
}
