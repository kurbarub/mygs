
List<String> languages = ["English", "Czech", "Russian"];

var languagesCode = {
  'English'   : 'en',
  'Czech'  : 'cz',
  'Russian'    : 'ru'
}; 