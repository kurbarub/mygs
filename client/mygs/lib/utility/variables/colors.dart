import 'package:flutter/material.dart';

/// Primary Colors

Color colorBlack = const Color(0xff000000);
Color colorMain = Color(0xff14471E);
Color colorWhiteText = const Color(0xffFFFFFF);
Color colorMenuPanel = const Color.fromRGBO(200, 210, 209, 1);
Color colorTextfieldBackround = const Color.fromRGBO(242, 242, 242, 1);
Color colorModalWindow = const Color.fromRGBO(217, 217, 217, 1);

/// Accent Colors

Color colorGrayTextFields = const Color(0xff333333);
Color colorLightGreen = const Color(0xff68904D);
Color colorWarning = const Color(0xffEE9B01);
Color colorInfo = const Color(0xffC8D2D1);
Color colorError = const Color(0xffCC0000);
Color colorTranspBlack = const Color.fromRGBO(0, 0, 0, 0.5);
Color colorTranspRed = Color.fromRGBO(255, 0, 0, 0.5);
Color colorTranspGreen = const Color.fromRGBO(20, 71, 30, 0.5);
Color colorLightWhite = Color(0xffFCFCFC);
Color colorUnderlineBorder = Color(0xff656565);

Color colorFakeImg = Color(0xff696969);

/// Utility Colors

/// Gradient Colors

Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}
