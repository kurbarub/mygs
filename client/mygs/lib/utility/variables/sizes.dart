import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

double width = 0;
double screenWidth = 0;
double realWidth = 0;
double height = 0;
double screenHeight = 0;
double preferredWidth = 0.0;

bool isTablet = false;
bool dark = false;

bool wideScreen = false;

// double getWidth([BuildContext context]) {
//   if (context != null) {
//     width = MediaQuery.of(context).size.width;
//     height = MediaQuery.of(context).size.height;
//     if(width >= 720) {
//       isTablet = true;
//     }
//     if(width > 500) {
//       width = 500;
//     }
//     var brightness = MediaQuery.of(context).platformBrightness;
//     if (brightness == Brightness.dark) {
//       dark = true;
//     } else {
//       dark = false;
//     }
//     getHeight(context);
//     getRealWidth(context);
//   }
//   print('width: $width');
//   return width;
// }

double getWidth(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    if(width > 420) {
      preferredWidth = width;
      wideScreen = true;
    } 
    if (width <= 420) {
      preferredWidth = 414;
      wideScreen = false;
    }
    getHeight(context);
    getRealWidth(context);

  return width;
} 

double getRealWidth(BuildContext context) {
    realWidth = MediaQuery.of(context).size.width;
    screenWidth = width;
  return realWidth;
}

double getHeight(BuildContext context) {
    //width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    screenHeight = height;
  return height;
}

// Custom sizes
double appBarHeight = font56;

// Font sizes
double font1 = width * (1 / preferredWidth);
double font2 = width * (2 / preferredWidth);
double font3 = width * (3 / preferredWidth);
double font4 = width * (4 / preferredWidth);
double font5 = width * (5 / preferredWidth);
double font6 = width * (6 / preferredWidth);
double font7 = width * (7 / preferredWidth);
double font8 = width * (8 / preferredWidth);
double font9 = width * (9 / preferredWidth);
double font10 = width * (10 / preferredWidth);
double font11 = width * (11 / preferredWidth);
double font12 = width * (12 / preferredWidth);
double font13 = width * (13 / preferredWidth);
double font14 = width * (14 / preferredWidth);
double font15 = width * (15 / preferredWidth);
double font16 = width * (16 / preferredWidth);
double font17 = width * (17 / preferredWidth);
double font18 = width * (18 / preferredWidth);
double font19 = width * (19 / preferredWidth);
double font20 = width * (20 / preferredWidth);
double font21 = width * (21 / preferredWidth);
double font22 = width * (22 / preferredWidth);
double font23 = width * (23 / preferredWidth);
double font24 = width * (24 / preferredWidth);
double font25 = width * (25 / preferredWidth);
double font26 = width * (26 / preferredWidth);
double font27 = width * (27 / preferredWidth);
double font28 = width * (28 / preferredWidth);
double font29 = width * (29 / preferredWidth);
double font30 = width * (30 / preferredWidth);
double font31 = width * (31 / preferredWidth);
double font32 = width * (32 / preferredWidth);
double font33 = width * (33 / preferredWidth);
double font34 = width * (34 / preferredWidth);
double font35 = width * (35 / preferredWidth);
double font36 = width * (36 / preferredWidth);
double font37 = width * (37 / preferredWidth);
double font38 = width * (38 / preferredWidth);
double font39 = width * (39 / preferredWidth);
double font40 = width * (40 / preferredWidth);
double font41 = width * (41 / preferredWidth);
double font42 = width * (42 / preferredWidth);
double font43 = width * (43 / preferredWidth);
double font44 = width * (44 / preferredWidth);
double font45 = width * (45 / preferredWidth);
double font46 = width * (46 / preferredWidth);
double font47 = width * (47 / preferredWidth);
double font48 = width * (48 / preferredWidth);
double font49 = width * (49 / preferredWidth);
double font50 = width * (50 / preferredWidth);
double font51 = width * (51 / preferredWidth);
double font52 = width * (52 / preferredWidth);
double font53 = width * (53 / preferredWidth);
double font54 = width * (54 / preferredWidth);
double font55 = width * (55 / preferredWidth);
double font56 = width * (56 / preferredWidth);
double font57 = width * (57 / preferredWidth);
double font58 = width * (58 / preferredWidth);
double font59 = width * (59 / preferredWidth);
double font60 = width * (60 / preferredWidth);
double font61 = width * (61 / preferredWidth);
double font62 = width * (62 / preferredWidth);
double font63 = width * (63 / preferredWidth);
double font64 = width * (64 / preferredWidth);
double font65 = width * (65 / preferredWidth);
double font66 = width * (66 / preferredWidth);
double font68 = width * (68 / preferredWidth);
double font69 = width * (69 / preferredWidth);
double font70 = width * (70 / preferredWidth);
double font71 = width * (71 / preferredWidth);
double font72 = width * (72 / preferredWidth);
double font73 = width * (73 / preferredWidth);
double font74 = width * (74 / preferredWidth);
double font75 = width * (75 / preferredWidth);
double font76 = width * (76 / preferredWidth);
double font77 = width * (77 / preferredWidth);
double font78 = width * (78 / preferredWidth);
double font79 = width * (79 / preferredWidth);
double font80 = width * (80 / preferredWidth);
double font81 = width * (81 / preferredWidth);
double font82 = width * (82 / preferredWidth);
double font83 = width * (83 / preferredWidth);
double font84 = width * (84 / preferredWidth);
double font85 = width * (85 / preferredWidth);
double font86 = width * (86 / preferredWidth);
double font87 = width * (87 / preferredWidth);
double font88 = width * (88 / preferredWidth);
double font89 = width * (89 / preferredWidth);
double font90 = width * (90 / preferredWidth);
double font91 = width * (91 / preferredWidth);
double font92 = width * (92 / preferredWidth);
double font93 = width * (93 / preferredWidth);
double font94 = width * (94 / preferredWidth);
double font95 = width * (95 / preferredWidth);
double font96 = width * (96 / preferredWidth);
double font97 = width * (97 / preferredWidth);
double font98 = width * (98 / preferredWidth);
double font99 = width * (99 / preferredWidth);
double font100 = width * (100 / preferredWidth);
/// up to 100
double font102 = width * (102 / preferredWidth);
double font104 = width * (104 / preferredWidth);
double font105 = width * (105 / preferredWidth);
double font106 = width * (106 / preferredWidth);
double font108 = width * (108 / preferredWidth);
double font110 = width * (110 / preferredWidth);
double font115 = width * (115 / preferredWidth);
double font114 = width * (114 / preferredWidth);
double font116 = width * (116 / preferredWidth);
double font120 = width * (120 / preferredWidth);
double font121 = width * (121 / preferredWidth);
double font122 = width * (122 / preferredWidth);
double font123 = width * (123 / preferredWidth);
double font124 = width * (124 / preferredWidth);
double font125 = width * (125 / preferredWidth);
double font126 = width * (126 / preferredWidth);
double font127 = width * (127 / preferredWidth);
double font128 = width * (128 / preferredWidth);
double font129 = width * (129 / preferredWidth);
double font130 = width * (130 / preferredWidth);
double font135 = width * (135 / preferredWidth);
double font137 = width * (137 / preferredWidth);
double font140 = width * (140 / preferredWidth);
double font144 = width * (144 / preferredWidth);
double font145 = width * (145 / preferredWidth);
double font146 = width * (146 / preferredWidth);
double font147 = width * (147 / preferredWidth);
double font148 = width * (148 / preferredWidth);
double font149 = width * (149 / preferredWidth);
double font150 = width * (150 / preferredWidth);
double font156 = width * (156 / preferredWidth);
double font158 = width * (158 / preferredWidth);
double font160 = width * (160 / preferredWidth);
double font162 = width * (162 / preferredWidth);
double font166 = width * (166 / preferredWidth);
double font170 = width * (170 / preferredWidth);
double font171 = width * (171 / preferredWidth);
double font172 = width * (172 / preferredWidth);
double font173 = width * (173 / preferredWidth);
double font174 = width * (174 / preferredWidth);
double font175 = width * (175 / preferredWidth);
double font176 = width * (176 / preferredWidth);
double font177 = width * (177 / preferredWidth);
double font178 = width * (178 / preferredWidth);
double font179 = width * (179 / preferredWidth);
double font180 = width * (180 / preferredWidth);
double font185 = width * (185 / preferredWidth);
double font190 = width * (190 / preferredWidth);
double font191 = width * (191 / preferredWidth);
double font192 = width * (192 / preferredWidth);
double font193 = width * (193 / preferredWidth);
double font194 = width * (194 / preferredWidth);
double font195 = width * (195 / preferredWidth);
double font196 = width * (196 / preferredWidth);
double font197 = width * (197 / preferredWidth);
double font198 = width * (198 / preferredWidth);
double font199 = width * (199 / preferredWidth);
double font200 = width * (200 / preferredWidth);
double font203 = width * (203 / preferredWidth);
double font210 = width * (210 / preferredWidth);
double font211 = width * (211 / preferredWidth);
double font212 = width * (212 / preferredWidth);
double font213 = width * (213 / preferredWidth);
double font214 = width * (214 / preferredWidth);
double font215 = width * (215 / preferredWidth);
double font216 = width * (216 / preferredWidth);
double font217 = width * (217 / preferredWidth);
double font218 = width * (218 / preferredWidth);
double font219 = width * (219 / preferredWidth);
double font220 = width * (220 / preferredWidth);
double font225 = width * (225 / preferredWidth);
double font230 = width * (230 / preferredWidth);
double font240 = width * (240 / preferredWidth);
double font248 = width * (248 / preferredWidth);
double font250 = width * (250 / preferredWidth);
double font260 = width * (260 / preferredWidth);
double font266 = width * (266 / preferredWidth);
double font270 = width * (270 / preferredWidth);
double font280 = width * (280 / preferredWidth);
double font290 = width * (290 / preferredWidth);
double font292 = width * (292 / preferredWidth);
double font300 = width * (300 / preferredWidth);
double font314 = width * (314 / preferredWidth);
double font320 = width * (320 / preferredWidth);
double font330 = width * (330 / preferredWidth);
double font350 = width * (350 / preferredWidth);
double font360 = width * (360 / preferredWidth);
double font400 = width * (400 / preferredWidth);
double font420 = width * (420 / preferredWidth);
double font460 = width * (460 / preferredWidth);
double font480 = width * (480 / preferredWidth);
double font488 = width * (488 / preferredWidth);
double font500 = width * (500 / preferredWidth);
double font520 = width * (520 / preferredWidth);
double font540 = width * (540 / preferredWidth);
double font550 = width * (550 / preferredWidth);
double font600 = width * (600 / preferredWidth);
double font700 = width * (700 / preferredWidth);
double font800 = width * (800 / preferredWidth);

double font80pctWidth = width * 0.8;
double font90pctWidth = width * 0.9;
double font100pctWidth = width * 1.0;

double fontNeg10 = width * (-10 / preferredWidth);
