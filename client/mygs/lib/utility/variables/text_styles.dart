import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';

/// Fonts

String fontOpenSans = "Open Sans";


/// text styles

TextStyle styleTitle1 = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font16,
    fontWeight: FontWeight.w400,
    // height: 21.79,
    color: colorWhiteText);

TextStyle styleTitle2 = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font16,
    fontWeight: FontWeight.w400,
    // height: 21.79,
    color: colorBlack);

TextStyle styleTitle3 = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font14,
    fontWeight: FontWeight.w400,
    // height: 21.79,
    color: colorBlack);

TextStyle styleTitleContent = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font20,
    fontWeight: FontWeight.w400,
    // height: 27.24,
    color: colorBlack);

TextStyle styleInscriptions = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font16,
    fontWeight: FontWeight.w400,
    // height: 21.79,
    color: colorGrayTextFields);

TextStyle styleTextButton = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font18,
    fontWeight: FontWeight.w400,
    // height: font24,
    color: colorWhiteText);

TextStyle styleInscriptionsGreen = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font16,
    fontWeight: FontWeight.w400,
    // height: 21.79,
    color: colorMain);

TextStyle styleFuelPrice = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font14,
    fontWeight: FontWeight.w600,
    // height: 21.79,
    color: colorMain);

TextStyle styleFuelStandNumber = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font20,
    fontWeight: FontWeight.w700,
    // height: 21.79,
    color: colorMain);


TextStyle styleTitleMenu = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font16,
    fontWeight: FontWeight.w600,
    // height: 21.79,
    color: colorBlack);

TextStyle styleBold = TextStyle(
    fontFamily: fontOpenSans,
    fontSize: font20,
    fontWeight: FontWeight.w600,
    // height: 21.79,
    color: colorBlack);