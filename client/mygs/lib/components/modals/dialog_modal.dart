import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import '../buttons/regular_button.dart';

class DialogModal extends StatelessWidget {
  const DialogModal(
      {super.key,
      required this.action,
      required this.actionTitle,
      required this.title,
      required this.discription, required this.cont});

  final VoidCallback action;
  final String actionTitle;
  final String title;
  final String discription;
  final BuildContext cont;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      alignment: Alignment.bottomCenter,
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(font25)),
      child: Container(
        constraints: BoxConstraints(
          maxHeight: font350,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: font34, horizontal: font15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Center(
                  child: Text(
                title,
                style: styleTitleContent,
              )),
              SizedBox(
                height: font12,
              ),
              Text(
                discription,
                style: styleTextButton.copyWith(color: colorGrayTextFields),
              ),
              SizedBox(
                height: font36,
              ),
              RegularButton(
                context: actionTitle,
                onTap: (){
                  action();
                  Navigator.pop(context);
                },
                isGreen: true,
              ),
              SizedBox(
                height: font12,
              ),
              RegularButton(
                  context: AppLocalizations.of(context).translate('cancel'),
                  onTap: () {
                    Navigator.pop(cont);
                  })
            ],
          ),
        ),
      ),
    );
  }
}
