
import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:another_flushbar/flushbar.dart';

import 'package:flutter/material.dart';

import '../../utility/variables/text_styles.dart';


void infoFlashbar(
   {required BuildContext context,
  required String title,
  required String message,
  bool isInfo = false,
  bool isSuccess = false,
  bool isError = false
  }
  ) {
    Flushbar(
      duration: const Duration(seconds: 2),
      margin: EdgeInsets.all(font30),
      padding: EdgeInsets.all(font10),
      borderRadius: BorderRadius.circular(font5),

      backgroundColor:  isError? colorError: (isInfo? colorInfo:(isSuccess? colorLightGreen:colorWarning)),
                       

      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(font4, font4,),
          blurRadius: font4,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.VERTICAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      title: title,
      message: message, 
      titleText: Text(
        title,
        style: styleTextButton.copyWith(fontWeight: FontWeight.w600, fontSize: font14),
      ),
      messageText: Text(
        message,
        style: styleTextButton.copyWith(fontSize: font14),
      ),
      
    ).show(context);
  }