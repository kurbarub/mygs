import 'package:MyGS/classes/feed_back_class.dart';
import 'package:MyGS/classes/gas_station_class.dart';
import 'package:MyGS/classes/refueling_stand_class.dart';
import 'package:MyGS/components/buttons/regular_button.dart';
import 'package:MyGS/components/dividers/regular_divider.dart';
import 'package:MyGS/pages/maps/feed_back_page.dart';
import 'package:MyGS/pages/refueling/refueling_page.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../classes/fuel_class.dart';
import '../../pages/access/no_access_page.dart';
import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/pay_methods.dart';
import '../../utility/variables/services.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import 'dialog_modal.dart';
import 'info_bottom_flushbar.dart';

class GasStationSheet extends StatefulWidget {
  GasStationSheet(
      {super.key,
      required this.gs});

  final GasStation gs;

  @override
  State<GasStationSheet> createState() => _GasStationSheetState();
}

class _GasStationSheetState extends State<GasStationSheet> {
  void submitRoute() {}


  void submitRefuel() {
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
    else 
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const RefuelingPage()));
  }

  DataCollector dc = DataCollector();

  var coordinate;
  final Map<String, Marker> _markers = {};

  Future<void> _onMapCreated(GoogleMapController controller) async {
    final gasStation = widget.gs;

    setState(() {
      _markers.clear();
      final marker = Marker(
        markerId: MarkerId(gasStation.id.toString()),
        position: LatLng(gasStation.latitude!, gasStation.longtitude!),
      );
      _markers[gasStation.id.toString()] = marker;
    });
  }

  void submitFeedBack() async{
    if(!dc.isLoggedIn() || dc.isUnAthorizedVersion())
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const NoAccessPage()));
    else{
      
      FeedBack fb = await dc.getFeedBackGSIfExist(widget.gs.id!);

      Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => FeedBackPage(fb: fb, itemId: widget.gs.id!)));
    }

  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  void _modalBottomSheetMenu() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showModalBottomSheet(
          enableDrag: true,
          isDismissible: true,
          barrierColor: Colors.transparent,
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
          context: context,
          builder: (builder) {
            return _sheet();
          });
    });
  }

  void _launchMapsUrl(double lat, double lon) async {
    final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    coordinate = [widget.gs.latitude, widget.gs.longtitude];
    _modalBottomSheetMenu();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "${widget.gs.firmName} Gas Station",
          style: styleTitleContent,
        ),
        centerTitle: true,
      ),
      body: InkWell(
        onTap: () {
          _modalBottomSheetMenu();
        },
        child: _map(),
      ),
    );
  }

  Widget _map() {
    late GoogleMapController mapController;

    final LatLng _center = LatLng(coordinate[0], coordinate[1]);

    return GoogleMap(
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 15.0,
      ),
      markers: _markers.values.toSet(),
    );
  }

  Widget makeDismissible({required Widget child}) => GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => Navigator.of(context).pop(),
        child: GestureDetector(
          onTap: () {},
          child: child,
        ),
      );


    Future modalRemoveFromFavorite() {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return DialogModal(
                action: () async{
                  await dc.changeGSLike(widget.gs.id!);
                  infoFlashbar(
                    context: context,
                    title: AppLocalizations.of(context).translate('rem_to_fav'),
                    message: AppLocalizations.of(context).translate('rem_to_fav_disc'),
                    isInfo: true);
                    setState(() {
                      widget.gs.isFavorite = !widget.gs.isFavorite!;
                    });
                },
                actionTitle: "Remove",
                title: AppLocalizations.of(context)
                    .translate('remove_from_favorites'),
                discription:
                    AppLocalizations.of(context).translate('remove_fav_comment'),
                cont: context);
          });
    }

    void ch(){
      if(widget.gs.isFavorite!)
        modalRemoveFromFavorite();
      else{
        dc.changeGSLike(widget.gs.id!);
        infoFlashbar(
          context: context,
          title: AppLocalizations.of(context).translate('added_to_fav'),
          message: AppLocalizations.of(context).translate('added_to_fav_disc'),
          isInfo: true);

        setState(() {
          widget.gs.isFavorite = !widget.gs.isFavorite!;
        });
      }
    }

  Widget _sheet() {
    return makeDismissible(
      child: DraggableScrollableSheet(
        initialChildSize: 0.33,
        minChildSize: 0,
        // maxChildSize: 0.7,
        builder: (_, controller) => Container(
          decoration: BoxDecoration(
              color: colorWhiteText,
              borderRadius:
                  BorderRadius.vertical(top: Radius.circular(font25))),
          padding: EdgeInsets.all(font30),
          child: ListView(
            controller: controller,
            clipBehavior: Clip.none,
            // mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            widget.gs.firmName!,
                            style: styleTitle2.copyWith(
                                fontWeight: FontWeight.w600, fontSize: font20),
                          ),
                          SizedBox(
                            width: font12,
                          ),
                          Row(
                            children: [
                              Text(
                                widget.gs.rating.toString(),
                                style: styleTitle2,
                              ),
                              SizedBox(
                                width: font6,
                              ),
                              SvgPicture.asset('assets/icons/filled_star.svg'),
                            ],
                          )
                        ],
                      ),
                      SizedBox(
                        height: font12,
                      ),
                      Text(
                        widget.gs.getWorkingHours(),
                        style: styleTitle2,
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: font21),
                    child: InkWell(
                      onTap: (){
                          if(!dc.isLoggedIn() || dc.isUnAthorizedVersion()){
                            infoFlashbar(
                              context: context,
                              title: AppLocalizations.of(context).translate("access_denied"),
                              message: AppLocalizations.of(context).translate("access_denied_desc"),
                              isError: true);
                          }else{
                            ch();
                          }
                            
                        },
                      child: SvgPicture.asset(
                        widget.gs.isFavorite!
                            ? 'assets/icons/filled_heart.svg'
                            : 'assets/icons/unfilled_heart.svg',
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: font20,
              ),
              RegularDivider(height: font2),
              SizedBox(
                height: font20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SvgPicture.asset(
                        'assets/icons/map.svg',
                        height: font20,
                        width: font20,
                      ),
                      SizedBox(
                        width: font10,
                      ),
                      Text(
                        "${widget.gs.adress}",
                        style: styleTitle2,
                      ),
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: colorMain,
                      borderRadius: BorderRadius.circular(font15),
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: font2, horizontal: font8),
                      child: Text("${widget.gs.distance} km", style: styleTitle1),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: font28,
              ),
              Row(
                children: [
                  Expanded(
                    child: RegularButton(
                        context:
                            AppLocalizations.of(context).translate('route'),
                        onTap: () {
                          _launchMapsUrl(
                              widget.gs.latitude!, widget.gs.longtitude!);
                        }),
                  ),
                  SizedBox(
                    width: font18,
                  ),
                  Expanded(
                    child: RegularButton(
                      context: AppLocalizations.of(context).translate('refuel'),
                      onTap: (){submitRefuel();},
                      isGreen: true,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: font32,
              ),
              Text(
                AppLocalizations.of(context).translate('fuel_prices'),
                style: styleTitle2.copyWith(fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: font20,
              ),
              Wrap(
                  children: List.generate(widget.gs.fuels!.length, (index) {
                return _priceBox(widget.gs.fuels![index]);
              })),
              SizedBox(
                height: font14,
              ),
              Text(
                AppLocalizations.of(context).translate('paymen_method'),
                style: styleTitle2.copyWith(fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: font20,
              ),
              Wrap(
                  children:
                      List.generate(widget.gs.paymentMethods!.length, (index) {
                return _chip(paymentMthods[ widget.gs.paymentMethods![index]]!);
              })),
              SizedBox(
                height: font14,
              ),
              Text(
                AppLocalizations.of(context).translate('services'),
                style: styleTitle2.copyWith(fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: font20,
              ),
              Wrap(
                  children: List.generate(widget.gs.services!.length, (index) {
                return _chip(services[widget.gs.services![index]]!);
              })),
              SizedBox(
                height: font14,
              ),
              Text(
                AppLocalizations.of(context).translate('refueling_stand'),
                style: styleTitle2.copyWith(fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: font20,
              ),
              Wrap(
                  children:
                      List.generate(widget.gs.refuelingStands!.length, (index) {
                return _standBox(widget.gs.refuelingStands![index]);
              })),
              SizedBox(
                height: font40,
              ),
              RegularButton(
                context: AppLocalizations.of(context).translate('feedback'),
                onTap: () {
                  submitFeedBack();
                },
                isGreen: true,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _priceBox(FuelClass fuel) {
    return Padding(
      padding: EdgeInsets.only(bottom: font12, right: font16),
      child: Material(
        elevation: font4,
        shadowColor: colorBlack,
        borderRadius: BorderRadius.circular(font5),
        child: Container(
          width: font79,
          decoration: BoxDecoration(
              color: colorTextfieldBackround,
              borderRadius: BorderRadius.circular(font5)),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: font10, vertical: font6),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  fuel.name!,
                  style: styleTitle3,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: font4,
                ),
                Text(
                  "${fuel.price} Kc",
                  style: styleFuelPrice,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _chip(String content) {
    return Padding(
      padding: EdgeInsets.only(bottom: font12, right: font16),
      child: InkWell(
        onTap: () {},
        child: Material(
          elevation: font4,
          shadowColor: colorBlack,
          borderRadius: BorderRadius.circular(font5),
          child: ConstrainedBox(
            constraints: BoxConstraints(),
            child: DecoratedBox(
              // alignment: Alignment.center,
              // height: font22,
              decoration: BoxDecoration(
                  color: colorTextfieldBackround,
                  borderRadius: BorderRadius.circular(font5)),
              child: Padding(
                padding:
                    EdgeInsets.symmetric(vertical: font6, horizontal: font8),
                child: Text(
                  content,
                  textAlign: TextAlign.center,
                  style: styleTitleContent.copyWith(fontSize: font14),
                  softWrap: true,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _standBox(RefuelingStand stand) {
    return Padding(
      padding: EdgeInsets.only(bottom: font12, right: font16),
      child: Material(
        elevation: font4,
        shadowColor: colorBlack,
        borderRadius: BorderRadius.circular(font5),
        child: Container(
          width: font79,
          decoration: BoxDecoration(
              color: colorTextfieldBackround,
              borderRadius: BorderRadius.circular(font5)),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: font8, vertical: font6),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  stand.standId.toString(),
                  style: styleFuelStandNumber,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: font4,
                ),
                Wrap(
                    alignment: WrapAlignment.end,
                    children: List.generate(stand.fuels!.length, (index) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: font2),
                        child: Text(
                          stand.fuels![index].name!,
                          textAlign: TextAlign.center,
                          style: styleTitle3.copyWith(fontSize: font12),
                        ),
                      );
                    }))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
