import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class FeedBackTextArea extends StatefulWidget {
  const FeedBackTextArea({super.key, required this.hint, required this.controller});

  final String hint;
  final TextEditingController controller;

  @override
  State<FeedBackTextArea> createState() => _FeedBackTextAreaState();
}

class _FeedBackTextAreaState extends State<FeedBackTextArea> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          textSelectionTheme:
              TextSelectionThemeData(selectionColor: colorLightGreen)),
      child: TextFormField(
        
        maxLines: 10,
        onTap: () {
          // widget.tap();
        },
        key: widget.key,
        // obscureText: _isObscure,
        controller: widget.controller,
        style: styleInscriptions.copyWith(color: colorBlack),
        cursorWidth: font1,
        cursorColor: colorMain,
        decoration: InputDecoration(
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(font5),
              borderSide: BorderSide(width: 0, style: BorderStyle.none)),
          hoverColor: colorLightGreen,
          hintText: widget.hint,
          fillColor: colorTextfieldBackround,
          filled: true,
          // suffixIcon: _isPasswordField(),
          hintStyle: styleInscriptions,
          contentPadding:
              EdgeInsets.symmetric(horizontal: font20, vertical: font14),
        ),
      ),
    );
  }
}
