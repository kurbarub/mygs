// import 'dart:html';

import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';

import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class SingleBorderTextfield extends StatefulWidget {
  SingleBorderTextfield(
      {Key? key,
      required this.controller,
      required this.text,
      required this.tap,
      this.isReadOnly = false,
      this.isMask = false,
      required this.start})
      : super(key: key);

  TextEditingController controller;
  String text;
  VoidCallback tap;
  bool isReadOnly;
  bool isMask;
  VoidCallback start;

  @override
  State<SingleBorderTextfield> createState() => _SingleBorderTextfieldState();
}

class _SingleBorderTextfieldState extends State<SingleBorderTextfield> {
  // bool _isObscure = false;

  var maskFormatterCode = new MaskTextInputFormatter(
      mask: '###-###', filter: {"#": RegExp(r'[0-9]')});

  @override
  void initState() {
    // _isObscure = widget.isPassword;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          textSelectionTheme:
              TextSelectionThemeData(selectionColor: colorLightGreen)),
      child: TextFormField(
        onChanged: (v){
       
          if(v.length == 7){
            widget.start();
            
          }
        },
        inputFormatters: widget.isMask ? [maskFormatterCode] : [],
        readOnly: widget.isReadOnly,
        onTap: () {
          widget.tap();
        },
        key: widget.key,
        // obscureText: _isObscure,
        controller: widget.controller,
        style: styleInscriptions.copyWith(color: colorBlack, fontSize: font36),
        cursorWidth: font1,
        cursorColor: colorMain,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
        
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: font3,
              color: colorUnderlineBorder
            )
            
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: font3,
              color: colorUnderlineBorder
            )
            
          ),
          
          // border: OutlineInputBorder(
          //     // borderRadius: BorderRadius.circular(font5),
          //     borderSide: BorderSide(width: 0, style: BorderStyle.solid)),
          hoverColor: colorLightGreen,
          hintText: widget.text,
          // focusColor: colorUnderlineBorder,
          fillColor: Colors.transparent,
          
          filled: true,
          // suffixIcon: _isPasswordField(),
          hintStyle: styleInscriptions.copyWith(color: colorModalWindow, fontSize: font36),
          contentPadding:
              EdgeInsets.zero
        ),
      ),
    );
  }

  // Widget? _isPasswordField() {
  //   if (widget.isPassword) {
  //     return IconButton(
  //         icon: Icon(
  //           _isObscure ? Icons.visibility_off : Icons.visibility,
  //           color: colorBlack,
  //         ),
  //         onPressed: () {
  //           setState(() {
  //             _isObscure = !_isObscure;
  //           });
  //         });
  //   } else
  //     return null;
  // }
}
