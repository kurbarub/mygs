// import 'dart:html';

import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';

import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class AuthorizationTextField extends StatefulWidget {
  AuthorizationTextField(
      {Key? key,
      this.isPassword = false,
      required this.controller,
      required this.text,
      required this.tap,
      this.isReadOnly = false,
      this.isDateMask = false,
      this.isCvvMask = false})
      : super(key: key);

  bool isPassword;
  TextEditingController controller;
  String text;
  VoidCallback tap;
  bool isReadOnly;
  bool isDateMask;
  bool isCvvMask;

  @override
  State<AuthorizationTextField> createState() => _AuthorizationTextFieldState();
}

class _AuthorizationTextFieldState extends State<AuthorizationTextField> {
  bool _isObscure = false;
  var maskFormatterDate = new MaskTextInputFormatter(
      mask: '##/####', filter: {"#": RegExp(r'[0-9]')});
  var maskFormatterCvv =
      new MaskTextInputFormatter(mask: '###', filter: {"#": RegExp(r'[0-9]')});
  @override
  void initState() {
    _isObscure = widget.isPassword;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          textSelectionTheme:
              TextSelectionThemeData(selectionColor: colorLightGreen)),
      child: Material(
        elevation: font4,
        borderRadius: BorderRadius.circular(font5),
        shadowColor: Colors.black,
        child: TextFormField(
          inputFormatters: widget.isDateMask ? [maskFormatterDate] : 
                            widget.isCvvMask?[maskFormatterCvv]: [],
          readOnly: widget.isReadOnly,
          onTap: () {
            widget.tap();
          },
          key: widget.key,
          obscureText: _isObscure,
          controller: widget.controller,
          style: styleInscriptions.copyWith(color: colorBlack),
          cursorWidth: font1,
          cursorColor: colorMain,
          decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(font5),
                borderSide: BorderSide(width: 0, style: BorderStyle.none)),
            hoverColor: colorLightGreen,
            hintText: widget.text,
            fillColor: colorTextfieldBackround,
            filled: true,
            suffixIcon: _isPasswordField(),
            hintStyle: styleInscriptions,
            contentPadding:
                EdgeInsets.symmetric(horizontal: font20, vertical: font14),
          ),
        ),
      ),
    );
  }

  Widget? _isPasswordField() {
    if (widget.isPassword) {
      return IconButton(
          icon: Icon(
            _isObscure ? Icons.visibility_off : Icons.visibility,
            color: colorBlack,
          ),
          onPressed: () {
            setState(() {
              _isObscure = !_isObscure;
            });
          });
    } else
      return null;
  }
}
