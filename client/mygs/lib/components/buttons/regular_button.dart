import 'package:MyGS/utility/variables/text_styles.dart';
import 'package:flutter/material.dart';

import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';

class RegularButton extends StatefulWidget {
  const RegularButton({
    super.key,
    this.isGreen = false,
    required this.context,
    required this.onTap
  });

  final bool isGreen;
  final String context;
  final VoidCallback onTap;

  @override
  State<RegularButton> createState() => _RegularButtonState();
}

class _RegularButtonState extends State<RegularButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Container(
        width: double.infinity,
        height: font53,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(font5),
          color:  widget.isGreen? colorMain : colorTextfieldBackround
        ),
        alignment: Alignment.center,
        child: Text(
          widget.context,
          style: styleTextButton.copyWith(color: widget.isGreen? colorWhiteText : colorBlack)
        ),
      ),
    );
  }
}

