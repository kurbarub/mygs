import 'package:flutter/material.dart';

import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class ButtonText extends StatelessWidget {
  final String text;
  final VoidCallback onTap;
  final bool isGreen;

  const ButtonText({
    Key? key,
    required this.text,
    required this.onTap,
    this.isGreen = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // width: font130,
      child: InkWell(
        onTap: onTap,
        child: Text(
          text,
          style: isGreen?styleInscriptionsGreen: styleInscriptions,
        ),
      ),
    );
  }
}
