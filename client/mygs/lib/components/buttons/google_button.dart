import 'package:MyGS/utility/variables/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';

class GoogleButton extends StatefulWidget {
  const GoogleButton({
    super.key,
    required this.context,
    required this.onTap
  });

  final String context;
  final VoidCallback onTap;

  @override
  State<GoogleButton> createState() => _GoogleButtonState();
}

class _GoogleButtonState extends State<GoogleButton> {
  @override
  Widget build(BuildContext context) {
    return Material(
      shadowColor: colorBlack,
      elevation: font4,
      borderRadius: BorderRadius.circular(font5),
      child: InkWell(
        onTap: widget.onTap,
        child: Container(
          width: font195,
          height: font50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(font5),
            color: colorLightWhite,
           
          ),
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                widget.context,
                style: styleTitle1.copyWith(color: colorBlack)
              ),
              // SizedBox(width: font12,),
              // SvgPicture.asset('assets/icons/google_logo.svg'),
            ],
          ),
        ),
      ),
    );
  }
}