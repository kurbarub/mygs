import 'package:MyGS/services/data_collector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import '../modals/info_bottom_flushbar.dart';

class RegularTile extends StatefulWidget {
  RegularTile(
      {super.key,
      required this.onTap,
      required this.title,
      this.isSwitcher = false,
      this.switcherValue = false});

  final VoidCallback onTap;
  final String title;
  final bool isSwitcher;
  bool switcherValue;

  @override
  State<RegularTile> createState() => _RegularTileState();
}

class _RegularTileState extends State<RegularTile> {

  DataCollector dc = DataCollector();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Container(
        height: font46,
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: colorModalWindow, width: font1))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppLocalizations.of(context).translate(widget.title),
              style: styleTitle2.copyWith(color: colorGrayTextFields),
            ),
            !widget.isSwitcher
                ? SvgPicture.asset(
                    "assets/icons/right_simple_arrow.svg",
                    height: font14,
                  )
                : Transform.scale(
                    scale: 0.7,
                    child: CupertinoSwitch(
                      // dragStartBehavior: DragStartBehavior.down,
                      activeColor: colorMain,
                      value: widget.switcherValue,
                      onChanged: (value) {
                        
                        if(!dc.isLoggedIn() || dc.isUnAthorizedVersion()) {
                          infoFlashbar(
                            context: context,
                            title: AppLocalizations.of(context).translate("access_denied"),
                            message: AppLocalizations.of(context).translate("access_denied_desc"),
                            isError: true);
                        } else{
                          widget.onTap();
                          setState(() {
                            widget.switcherValue = !widget.switcherValue;
                        });
                        }
                        
                      },
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
