import 'package:MyGS/classes/gas_station_class.dart';
import 'package:MyGS/services/data_collector.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/text_styles.dart';
import '../modals/dialog_modal.dart';
import '../modals/info_bottom_flushbar.dart';

class GasStationTile extends StatefulWidget {
  const GasStationTile(
      {super.key,
      this.isGreen = false,
      required this.onTap,
      this.isFavoriteIcon = true,
      required this.gs});

  final bool isGreen;
  final VoidCallback onTap;
  final bool isFavoriteIcon;
  final GasStation gs;

  @override
  State<GasStationTile> createState() => _GasStationTileState();
}

class _GasStationTileState extends State<GasStationTile> {
  @override
  Widget build(BuildContext context) {

    DataCollector dc = DataCollector();

    bool localFav = widget.gs.isFavorite!;

    Future modalRemoveFromFavorite() {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return DialogModal(
                action: () async{
                  await dc.changeGSLike(widget.gs.id!);

                    setState(() {
                      widget.gs.isFavorite = !widget.gs.isFavorite!;
                    });
                },
                actionTitle: "Remove",
                title: AppLocalizations.of(context)
                    .translate('remove_from_favorites'),
                discription:
                    AppLocalizations.of(context).translate('remove_fav_comment'),
                cont: context);
          });
    }

    void ch(){
      if(widget.gs.isFavorite!)
        modalRemoveFromFavorite();
      else{
        dc.changeGSLike(widget.gs.id!);
        infoFlashbar(
          context: context,
          title: AppLocalizations.of(context).translate('added_to_fav'),
          message: AppLocalizations.of(context).translate('added_to_fav_disc'),
          isInfo: true);

        setState(() {
          widget.gs.isFavorite = !widget.gs.isFavorite!;
        });
      }
    }

    return InkWell(
      onTap: widget.onTap,
      child: Container(
        decoration: BoxDecoration(
            color: widget.isGreen ? colorMain : colorTextfieldBackround,
            borderRadius: BorderRadius.circular(font5)),
        child: Padding(
          padding: EdgeInsets.all(font10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "${widget.gs.adress}",
                    style: widget.isGreen
                        ? styleTitle1
                        : styleTitle1.copyWith(color: colorBlack,),
                  ),
                  Text(
                    "(${widget.gs.firmName})",
                    style: widget.isGreen
                        ? styleTitle1.copyWith(fontWeight: FontWeight.w600)
                        : styleTitle1.copyWith(
                            fontWeight: FontWeight.w600, color: colorBlack),
                  )
                ],
              ),
              SizedBox(
                height: font10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: widget.isGreen ? colorWhiteText : colorMain,
                          borderRadius: BorderRadius.circular(font15),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: font2, horizontal: font8),
                          child: Text(
                            "${widget.gs.distance} km",
                            style: widget.isGreen
                                ? styleTitle1.copyWith(color: colorBlack)
                                : styleTitle1,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: font12,
                      ),
                      Text(
                        widget.gs.getWorkingHours(),
                        style: widget.isGreen
                            ? styleTitle1
                            : styleTitle1.copyWith(color: colorBlack),
                      ),
                      SizedBox(
                        width: font18,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      InkWell(
                        onTap: (){
                          if(!dc.isLoggedIn() || dc.isUnAthorizedVersion()){
                            infoFlashbar(
                              context: context,
                              title: AppLocalizations.of(context).translate("access_denied"),
                              message: AppLocalizations.of(context).translate("access_denied_desc"),
                              isError: true);
                          }else{
                            ch();
                            
                          }
                            
                        },
                        child: SvgPicture.asset(
                            widget.gs.isFavorite! ? 'assets/icons/filled_heart.svg' : 'assets/icons/unfilled_heart.svg',
                          
                          height: font25,
                        ),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
