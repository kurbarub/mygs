import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/text_styles.dart';

class FilterDropDownTile extends StatefulWidget {
  const FilterDropDownTile(
      {super.key, required this.title, required this.content});

  final String title;
  final Widget content;
  @override
  State<FilterDropDownTile> createState() => _FilterDropDownTileState();
}

class _FilterDropDownTileState extends State<FilterDropDownTile> with SingleTickerProviderStateMixin{
  bool isOpened = false;

  late AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
      upperBound: 0.5,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: font4,
      shadowColor: colorBlack,
      borderRadius: BorderRadius.circular(font5),
      child: ConstrainedBox(
        constraints: new BoxConstraints(
          minHeight: font46,
        ),
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: colorTextfieldBackround,
            borderRadius: BorderRadius.circular(font5),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: font22, vertical: font12),
            child: Column(
              children: [
                InkWell(
                  
                  onTap: () {
                          setState(() {
                            isOpened = !isOpened;
                            if (!isOpened) {
                              _controller..reverse(from: 0.5);
                            } else {
                              _controller..forward(from: 0.0);
                            }                 
                          });
                        },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        widget.title,
                        style: styleTitle1.copyWith(color: colorBlack),
                      ),

                      RotationTransition(
                        turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
                        child: SvgPicture.asset('assets/icons/bottom_arrow.svg'),
                      )
                      
                    ],
                  ),
                ),
                // SizedBox(height: ,),
                isOpened ? widget.content: Container()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
