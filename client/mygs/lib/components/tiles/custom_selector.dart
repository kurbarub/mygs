import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:restart_app/restart_app.dart';

import '../../classes/city_singleton.dart';
import '../../classes/language_singleton.dart';
import '../../services/data_collector.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';
import '../dividers/regular_divider.dart';

class CustomListSelector extends StatefulWidget {
  CustomListSelector(
      {super.key,
      required this.selectedItem,
      required this.items,
      this.isWhiteBackground = true,
      this.isMaxWidth = false,
      this.isLanguage = false,
      this.isCity = false,
      required this.onSelected});

  final VoidCallback onSelected;
  String selectedItem;
  final List<String> items;
  final bool isWhiteBackground;
  final bool isMaxWidth;
  final bool isLanguage;
  final bool isCity;

  @override
  State<CustomListSelector> createState() => _CustomListSelectorState();
}

class _CustomListSelectorState extends State<CustomListSelector>
    with SingleTickerProviderStateMixin {
  bool isOpened = false;
  final DataCollector dc = DataCollector();
  late AnimationController _controller;

  late String selectedItem;

  @override
  void initState() {
    super.initState();

    selectedItem = widget.selectedItem;
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
      upperBound: 0.5,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  Future<void> setLanguage(String lang) async {
    await SharedPrefLanguageSingleton().setLanguage(lang);
  }

  // Future<void> setCity(String city) async {
  //   await SharedPrefCitySingleton().setCity(city);
  // }

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: font4,
      shadowColor: colorBlack,
      borderRadius: BorderRadius.circular(font5),
      child: ConstrainedBox(
        constraints: new BoxConstraints(
            minHeight: font46,
            maxWidth: widget.isMaxWidth ? double.infinity : font150),
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: colorTextfieldBackround,
            borderRadius: BorderRadius.circular(font5),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: font22, vertical: font12),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      isOpened = !isOpened;
                      if (!isOpened) {
                        _controller..reverse(from: 0.5);
                      } else {
                        _controller..forward(from: 0.0);
                      }
                    });
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        selectedItem,
                        style: styleTitle1.copyWith(color: colorBlack),
                      ),
                      RotationTransition(
                        turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
                        child:
                            SvgPicture.asset('assets/icons/bottom_arrow.svg'),
                      )
                    ],
                  ),
                ),

                // SizedBox(height: font10,),
                isOpened ? _content() : Container()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _content() {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: font10),
          child: RegularDivider(
            height: font1,
          ),
        ),
        Container(
            width: widget.isMaxWidth ? double.infinity : font150,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(widget.items.length, (index) {
                return InkWell(
                  onTap: () {
                    setState(() {
                      isOpened = false;
                      selectedItem = widget.items[index].toString();

                      widget.isLanguage
                          ? setLanguage(widget.selectedItem =
                              widget.items[index].toString())
                          : () {};

                      widget.isCity
                          ? dc.setCity(widget.selectedItem =
                              widget.items[index].toString())
                          : () {};
                    });
                    widget.onSelected();
                  },
                  child: Padding(
                    padding: EdgeInsets.only(top: font16),
                    child: Text(widget.items[index]),
                  ),
                );
              }),
            )),
      ],
    );
  }
}
