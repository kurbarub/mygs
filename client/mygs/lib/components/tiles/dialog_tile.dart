import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../utility/translation/app_localization.dart';
import '../../utility/variables/colors.dart';
import '../../utility/variables/sizes.dart';
import '../../utility/variables/text_styles.dart';

class DialogTile extends StatefulWidget {
  DialogTile(
      {super.key,
      required this.title,
      required this.icon,
      this.isShared = false,
      required this.onTapRemove,
      required this.action,
      this.isTrash = true
      });

  final String title;
  final Widget icon;
  final bool isShared;
  final VoidCallback onTapRemove;
  final Widget action;
  final bool isTrash;

  @override
  State<DialogTile> createState() => _DialogTileState();
}

class _DialogTileState extends State<DialogTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: font46,
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(color: colorModalWindow, width: font1))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              widget.icon,
              SizedBox(
                width: font12,
              ),
              Text(
                widget.title,
                style: styleTitle2.copyWith(color: colorGrayTextFields),
              ),
            ],
          ),
          Row(
            children: [
              widget.isShared
                  ? Padding(
                      padding: EdgeInsets.only(right: font12),
                      child: SvgPicture.asset(
                        "assets/icons/share.svg",
                        // height: font14,
                      ),
                    )
                  : Text(''),

              widget.isTrash?
              InkWell(
                onTap: widget.onTapRemove,
                child: Container(
                  width: font63,
                  height: font33,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(font5),
                      color: colorTranspRed),
                  child: SvgPicture.asset(
                    "assets/icons/trash.svg",
                    // height: font14,
                  ),
                ),
              ): Container(),
              widget.action
            ],
          )
        ],
      ),
    );
  }
}
