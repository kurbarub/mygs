import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class RegularDivider extends StatelessWidget {
  const RegularDivider({super.key, required this.height});

  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: height,
      decoration: BoxDecoration(color: colorBlack),
    );
  }
}
