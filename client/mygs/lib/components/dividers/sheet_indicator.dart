import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../utility/variables/colors.dart';

class SheetIndicator extends StatelessWidget {
  const SheetIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: font5,
      width: font50,
      decoration: BoxDecoration(
        color: colorGrayTextFields
      ),
    );
  }
}