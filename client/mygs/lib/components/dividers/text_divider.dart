import 'package:MyGS/utility/variables/colors.dart';
import 'package:MyGS/utility/variables/sizes.dart';
import 'package:flutter/material.dart';


import '../../utility/variables/text_styles.dart';

class TextDivider extends StatelessWidget {
  final String text;
  const TextDivider({super.key, this.text = ''});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Divider(
            color: colorGrayTextFields,
            height: font1,
          ),
        ),
      
        Padding(
          padding: EdgeInsets.symmetric(horizontal: text!=''?font10:0),
          child: Text(
            text,
            style: styleInscriptions,
          ),
        ), 
        Expanded(
          child: Divider(
            color: colorGrayTextFields,
            height: font1,
          ),
        ),
        
      ],
    );
  }
}
