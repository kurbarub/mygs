import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../../utility/variables/colors.dart';

class WidgetDivider extends StatefulWidget {
  const WidgetDivider({super.key, required this.child, required this.height});

  final Widget child;
  final double height;

  @override
  State<WidgetDivider> createState() => _WidgetDividerState();
}

class _WidgetDividerState extends State<WidgetDivider> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Divider(
            color: colorGrayTextFields,
            height: widget.height,
          ),
        ),
        widget.child,
        // Padding(
        //   padding: EdgeInsets.symmetric(horizontal: text != '' ? font10 : 0),
        //   child: Text(
        //     text,
        //     style: styleInscriptions,
        //   ),
        // ),
        Expanded(
          child: Divider(
            color: colorGrayTextFields,
            height: widget.height,
          ),
        ),
      ],
    );
  }
}
