import 'package:MyGS/pages/access/splash_page.dart';
import 'package:MyGS/utility/translation/app_language.dart';
import 'package:MyGS/utility/translation/app_localization.dart';
import 'package:MyGS/utility/variables/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';

import 'classes/language_singleton.dart';

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Permission.camera.request();
  // await Permission.microphone.request();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    systemNavigationBarColor:
        Colors.black, // color bottom grabber in newer devices
    systemNavigationBarDividerColor:
        Colors.black, // line between screen and bottom nav bar
    statusBarBrightness: Brightness.dark,
    statusBarIconBrightness: Brightness.light,
    systemNavigationBarIconBrightness: Brightness.dark,
    systemNavigationBarContrastEnforced: false,
    systemStatusBarContrastEnforced: false,
  ));
  AppLanguage appLanguage = AppLanguage();

  await SharedPrefLanguageSingleton().initialize();
  if(SharedPrefLanguageSingleton().language == '') 
    await SharedPrefLanguageSingleton().setLanguage('English');

  runApp(MyGSApp(
    appLanguage: appLanguage,
  ));
}

class MyGSApp extends StatefulWidget {
  final AppLanguage? appLanguage;
  const MyGSApp({super.key, this.appLanguage});

  @override
  State<MyGSApp> createState() => _MyGSAppState();

  
}

class _MyGSAppState extends State<MyGSApp> {

  
  @override
  Widget build(BuildContext context) {


    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: colorMain, //or set color with: Color(0xFF0000FF)
    ));
    return ChangeNotifierProvider(
        create: (_) => widget.appLanguage,
        builder: (context, child) {
          return Consumer<AppLanguage?>(builder: (context, value, child) {
            return  MaterialApp(
              locale: const Locale('en', ''),
              supportedLocales: const <Locale>[
                Locale('en', ''),
              ],
              localizationsDelegates: const [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                DefaultCupertinoLocalizations.delegate
              ],
              debugShowCheckedModeBanner: false,
              title: 'MyGS',
              home: const SplashPage(),
              theme: ThemeData(
                  
                  appBarTheme: const AppBarTheme(
                    
                    color: Colors.transparent,
                    
                    elevation: 0,
                    iconTheme: IconThemeData(
                     color: Colors.black
                    ),
             
                ),
                hoverColor: colorLightGreen,
                indicatorColor: colorLightGreen
              ),

            );
          });
        });
  }
}

