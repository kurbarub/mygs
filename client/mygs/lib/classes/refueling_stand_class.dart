import 'package:MyGS/classes/fuel_class.dart';


class RefuelingStand {
  int? id;
  int? standId;
  List<FuelClass>? fuels;
 

  RefuelingStand({
    this.id,
    this.standId,
    this.fuels
  });

  RefuelingStand.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    standId = json['standId'];
    fuels = getFuelsList(json['fuels']);
  }

  List<FuelClass> getFuelsList(List<dynamic> json) {
    return (json).map((e) => FuelClass.fromJson(e)).toList();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['standId'] = this.standId;
    data['fuels'] = this.fuels;
    return data;
  }
}