import 'dart:convert';

import 'package:MyGS/classes/adress_class.dart';
import 'package:MyGS/classes/fuel_class.dart';
import 'package:MyGS/classes/refueling_stand_class.dart';
import 'package:intl/intl.dart';
import 'ofer_cless.dart';

class GasStation {
  int? id;
  String? firmName;
  double? latitude;
  double? longtitude;
  double? rating;
  DateTime? openedFrom;
  DateTime? openedTo;
  double? distance;
  bool? isClosed;
  bool? isAroundTheClock;
  bool? isFavorite;


  String? adress;
  List<FuelClass>? fuels;
  List<int>? paymentMethods;
  List<int>? services;
  List<RefuelingStand>? refuelingStands;

  GasStation(
      {this.id,
      this.firmName,
      this.adress,
      this.fuels,
      this.latitude,
      this.longtitude,
      this.distance,
      this.isAroundTheClock,
      this.isClosed,
      this.isFavorite,
      this.openedFrom,
      this.openedTo,
      this.paymentMethods,
      this.rating,
      this.refuelingStands,
      this.services,
      });

  String getWorkingHours(){

    if(this.isAroundTheClock!) return "Around the clock";
    if(this.isClosed!) return "Closed";
    else return "Open (${DateFormat("kk:mm").format(this.openedFrom!).toString()} - ${DateFormat("kk:mm").format(this.openedTo!).toString()})";
  }

  GasStation.fromJsonFullInfo(Map<String, dynamic> json) {

    id = json['id'];
    firmName = json['firmName'];
    adress = json['address'];
    fuels = getFuelsList(json['priceList']);
    latitude = json['latitude'];
    longtitude = json['longitude'];
    distance = json['distance'];
    isAroundTheClock = json['isAroundTheClock'];
    isClosed = json['isClosed'];
    isFavorite = json['isFavorite'];
    openedFrom = DateTime.parse(json['openFrom']);
    openedTo = DateTime.parse(json['openTo']);
    paymentMethods = getIntList(json['paymentMethods']);
    rating = json['rating'];
    refuelingStands = getRefuelingStandsList(json['refuelingStands']);
    services = getIntList(json['services']);

  }
 
  GasStation.fromJsonShortInfo(Map<String, dynamic> json) {
    id = json['id'];
    firmName = json['firmName'];
    adress = json['address'];
    latitude = json['latitude'];
    longtitude = json['longitude'];
    distance = json['distance'];
    isAroundTheClock = json['isAroundTheClock'];
    isClosed = json['isClosed'];
    isFavorite = json['isFavorite'];
    openedFrom = DateTime.parse(json['openFrom']);
    openedTo = DateTime.parse(json['openTo']);
    rating = json['rating'];
  }

  List<FuelClass> getFuelsList(List<dynamic> json) {
    return (json).map((e) => FuelClass.fromJson(e)).toList();
  }

  List<RefuelingStand> getRefuelingStandsList(List<dynamic> json) {
    return (json)
        .map((e) => RefuelingStand.fromJson(e))
        .toList();
  }


  List<int> getIntList(List<dynamic> json) {

    return (json).map((e) => e as int).toList();
  }
}
