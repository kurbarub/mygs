class Adress {
  String? id;
  String? buildingNumber;
  String? city;
  String? street;
  String? zipCode;
 

  Adress({
    this.id,
    this.buildingNumber,
    this.city,
    this.street,
    this.zipCode
  });

  Adress.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    buildingNumber = json['building_number'];
    city = json['city'];
    street = json['street'];
    zipCode = json['zip_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['building_number'] = this.buildingNumber;
    data['city'] = this.city;
    data['street'] = this.street;
    data['zip_code'] = this.zipCode;
    return data;
  }
}