import 'dart:ffi';

import 'package:MyGS/classes/auto_class.dart';
import 'package:MyGS/classes/gas_station_class.dart';
import 'package:MyGS/classes/share_class.dart';

import 'card_class.dart';
import 'order_class.dart';

class UserClass {
  int? id;
  String? dob;
  String? email;
  String? name;
  String? surname;
  String? phone;
  String? password;
  String? newPassword;
  int? code;
  String? token;

  List<CardClass>? cards;
  List<AutoClass>? cars;
  List<GasStation>? favorites;
  List<OrderClass>? orders;
  List<ShareClass>? invitedUsers;
  int? gender;

  bool? receiveOfferNews;
  bool? receiveOrderInfo;

  UserClass(
      {this.cards,
      this.cars,
      this.dob,
      this.email,
      this.favorites,
      this.id,
      this.invitedUsers,
      this.gender,
      this.name,
      this.orders,
      this.phone,
      this.surname,
      this.password,
      this.code,
      this.token,
      this.newPassword,
      this.receiveOfferNews,
      this.receiveOrderInfo
      });

  Map<String, dynamic> toRegistrationPostData() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['surname'] = this.surname;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['dob'] = this.dob;
    data['password'] = this.password;
    data['gender'] = this.gender;
    return data;
  }

  Map<String, dynamic> toVerifyAccountData() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    data['code'] = this.code;
    return data;
  }

  Map<String, dynamic> toLogInData() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    return data;
  }

  
  Map<String, dynamic> toResestPassword() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['code'] = this.code;
    data['password'] = this.password;
    data['passwordAgain'] = this.newPassword;
    return data;
  }


  UserClass.fromJsonLogIn(Map<String, dynamic> json) {
    id = json['id'];
    dob =  json['dob'];
    email = json['email'];
    gender = json['gender'];
    name = json['name'];
    phone = json['phone'];
    surname = json['surname'];
    token = json['accessToken'];
    receiveOfferNews = json['receiveOrderInfo'];
    receiveOrderInfo = json['receiveOrderInfo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cards'] = this.cards;
    data['cars'] = this.cars;
    data['dob'] = this.dob;
    data['email'] = this.email;
    data['favorites'] = this.favorites;
    data['invitedUsers'] = this.invitedUsers;
    data['isMale'] = this.gender;
    data['name'] = this.name;
    data['orders'] = this.orders;
    data['phone'] = this.phone;
    data['surname'] = this.surname;
    return data;
  }
}
