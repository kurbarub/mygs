class AutoClass {
  int? id;
  String? brand;
  String? number;
  int? fuelType;

  AutoClass({this.number, this.id, this.brand, this.fuelType});

  String getFuelTypeString(){
    if(this.fuelType == 0) return "DIESEL";
    if(this.fuelType == 1) return "PETROL";
    if(this.fuelType == 2) return "LPG";
    return "CNG";
  }

  AutoClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    brand = json['brand'];
    number = json['number'];
    fuelType = json['fuelType'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['brand'] = this.brand;
    data['number'] = this.number;
    data['fuelType'] = this.fuelType;

    return data;
  }
}
