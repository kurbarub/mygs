import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefLanguageSingleton {
  static final SharedPrefLanguageSingleton _instance = SharedPrefLanguageSingleton._internal();

  factory SharedPrefLanguageSingleton() => _instance;

  SharedPrefLanguageSingleton._internal();

  late SharedPreferences _pref;

  Future<void> initialize() async {
    _pref = await SharedPreferences.getInstance();
  }

  Future<bool> setLanguage(String language) => _pref.setString('app_language', language);

  String get language => _pref.getString('app_language') ?? '';
}