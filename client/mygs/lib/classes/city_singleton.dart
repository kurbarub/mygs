
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefCitySingleton {
  static final SharedPrefCitySingleton _instance = SharedPrefCitySingleton._internal();

  factory SharedPrefCitySingleton() => _instance;

  SharedPrefCitySingleton._internal();

  late SharedPreferences _pref;

  Future<void> initialize() async {
    _pref = await SharedPreferences.getInstance();
  }

  Future<bool> setCity(String city) => _pref.setString('app_city', city);

  String get city => _pref.getString('app_city') ?? '';
}