class CardClass {
  int? id;
  String? lastFourNumber;
  int? bin;

  CardClass({
    this.id, this.lastFourNumber, this.bin
  });

  CardClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    lastFourNumber = json['lastFourDigits'];
    bin = json['bin'];

  }

  Map<String, dynamic> toJson(String cardNumber, DateTime dateExp, int cvv) {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['cardNumber'] = cardNumber;
    data['dateExp'] = dateExp.toIso8601String();
    data['cvv'] = cvv;
    return data;
  }
}
