import 'package:MyGS/classes/auto_class.dart';
import 'package:MyGS/classes/card_class.dart';
import 'package:MyGS/classes/user_class.dart';

class ShareClass {
  String? id;
  List<AutoClass>? sharedCars;
  List<CardClass>? sharedCards;
  String? invitedUser;

  ShareClass({this.id, this.invitedUser, this.sharedCards, this.sharedCars});

  ShareClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sharedCards = getCardList(json['shared_cards']);
    sharedCars = getAutoList(json['shared_cars']);
    invitedUser = json['invited_user'];
  }

  List<AutoClass> getAutoList(List<dynamic> json) {
    return (json)
        .map((e) => AutoClass.fromJson(e))
        .toList();
  }

  List<CardClass> getCardList(List<dynamic> json) {
    return (json)
        .map((e) => CardClass.fromJson(e))
        .toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['invited_user'] = this.invitedUser;
    data['shared_cards'] = this.sharedCards;
    data['shared_cars'] = this.sharedCars;
    return data;
  }
}
