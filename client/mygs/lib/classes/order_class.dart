
class OrderClass {
  int? id;
  double? totalAmount;
  double? countLitters;
  DateTime? created;
  String? fuelName;
  String? refueledCarNumber;
  String? refueledCardNumber;
  String? gasStationAddress;
  String? gasStationName;



  OrderClass({
    this.created, 
    this.fuelName, 
    this.gasStationAddress, 
    this.id, 
    this.totalAmount,
    this.countLitters,
    this.gasStationName,
    this.refueledCarNumber,
    this.refueledCardNumber
  });

  OrderClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    totalAmount = json['totalAmount'];
    countLitters = json['countLitters'];
    created = DateTime.parse(json['created']);
    fuelName = json['fuelName'];
    refueledCarNumber = json['refueledCarNumber'];
    refueledCardNumber = json['refueledCardNumber'];
    gasStationAddress = json['gasStationAddress'];
    gasStationName = json['gasStationName'];
   
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['id'] = this.id;
  //   data['total_amount'] = this.totalAmount;
  //   data['liters_count'] = this.litersCount;
  //   data['created'] = this.created;
  //   data['fuel_name'] = this.fuelName;
  //   data['auto_number'] = this.autoNumber;
  //   data['gas_station_ddress'] = this.gasStationAddress;
  //   return data;
  // }
}
