
class FeedBack {
  int? id;
  String? content;
  String? title;
  int? rating;
  // List<Message>? messages;

  FeedBack({this.id, this.content = "", this.rating = 0, this.title=""});

  FeedBack.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    content = json['content'];
    title = json['title'];
    rating = json['rating'];
    // messages = json['messages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content'] = this.content;
    data['title'] = this.title;
    data['rating'] = this.rating;
    // data['messages'] = this.messages;
    return data;
  }
}
