
class QuestionClass {
  int? id;
  String? question;
  String? answer;

  QuestionClass({this.answer, this.id, this.question});

  QuestionClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    question = json['title'];
    answer = json['answer'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.question;
    data['answer'] = this.answer;
    return data;
  }
}
