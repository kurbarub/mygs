class OferClass {
  int? id;
  String? content;
  String? imgUrl;
  String? title;
  int? gsId;
  String? gsAddress;
  String? gsName;
  
  OferClass({
    this.id,
    this.content,
    this.imgUrl,
    this.title,
    this.gsAddress,
    this.gsId,
    this.gsName
  });

  OferClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    content = json['content'];
    imgUrl = json['imgUrl'];
    title = json['title'];
    gsName = json['gsName'];
    gsAddress = json['gsAddress'];
    gsId = json['gsId'];
  }
  
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['content'] = this.content;
    data['imgUrl'] = this.imgUrl;
    data['title'] = this.title;
    data['gsName'] = this.gsName;
    data['gsAddress'] = this.gsAddress;
    data['gsId'] = this.gsId;
    return data;
  }
  
}