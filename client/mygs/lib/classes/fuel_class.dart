class FuelClass {
  int? id;
  String? name;
  double? price;
  String? fuelType;

  FuelClass({this.id, this.name, this.price, this.fuelType});


  int getFuelTypeIndex(){
    if(this.fuelType == "DIESEL") return 0;
    if(this.fuelType == "PETROL") return 1;
    if(this.fuelType == "LPG") return 2;
    return 3;
  }



  FuelClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    fuelType = json['fuelType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['fuelType'] = this.fuelType;
    return data;
  }
}
